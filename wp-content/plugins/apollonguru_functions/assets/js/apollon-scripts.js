jQuery(document).ready(function($) {
	$('body').on('click', '.my-cg-cart-remove', function(event) {
		event.preventDefault();
		var $cart = $('.tiny-cart'),
			$cartCount = $cart.find('.cg-cart-count'),
			cart_item = $(this).data('cart_item'),
			$currencySymbol = $('.woocommerce-Price-currencySymbol').eq(0);
		
		$.ajax({
			type: 'POST',
			url: cg_ajax.cg_ajax_url,
			data: {
				action: 'remove_item_from_cart',
				cart_item: cart_item
			},
			success: function( responce ) {
				if(responce.success){
					$cart.find('[data-cart_item="'+cart_item+'"]').parents('li.cart_list_product').remove();
					$cart.find('.cg-cart-count').text(responce.data.count);
					$cart
						.find('.woocommerce-Price-amount')
						.empty()
						.append($currencySymbol, responce.data.amount);
				}
			}
		});
	});
});
<?php
/*
 * Plugin Name: AG functions
 * Version: 1.0
 * Description: Apollon Guru functions.
 * Author: ApollonGuru
 * Author URI: https://apollon.guru/
 */
if ( ! defined( 'ABSPATH' ) ) exit;

include_once plugin_dir_path( __FILE__ ) . 'js_composer/js_composer.php';

add_filter( 'wpseo_canonical', function($canonical){
	$canonical = str_replace('/product-category/', '/', $canonical);
	return $canonical;
} );

add_filter( 'vbb_yotpoextra_product_cat_data', function($data){
	if($data['url']){
		$data['url'] = str_replace('/product-category/', '/', $data['url']);
	}
	return $data;
} );

add_filter( 'vbb_yotpoextra_product_cat_structured_data_json', function($structured_data_json){

	$structured_data_json = str_replace('\/product-category\/', '\/', $structured_data_json);

	return $structured_data_json;
} );

add_filter( 'woocommerce_structured_data_product', function($markup, $product){

    $wpseo = get_option( 'wpseo' );
    if(@$wpseo['company_name']){
        foreach ($markup['offers'] as &$offer) {
            $offer['seller']['name'] = $wpseo['company_name'];
        }
    }
    return $markup;
}, 10, 2 );

add_filter( 'wpseo_title', function($title){
    static $is_called = false;
    if( !$is_called && is_paged() && $title ){
        $page = get_query_var( 'paged' );
        $title = 'Page ' . $page . ' | ' . $title;
    }
    $is_called = !$is_called;
    return $title;
} );

add_filter( 'wpseo_metadesc', function($description){
    if( is_paged() && $description ){
        $page = get_query_var( 'paged' );
        $description = 'Page ' . $page . ' | ' . $description;
    }
    return $description;
} );

add_filter( 'wpseo_metakey', function($keywords){
    if( is_paged() && $keywords){
        $page = get_query_var( 'paged' );
        $keywords = 'Page ' . $page . ', ' . $keywords;
    }
    return $keywords;
} );

function ag_remove_some_widgets_blog(){

    unregister_sidebar( 'sidebar-1' );
    register_sidebar( array(
        'name' => __( 'Sidebar', 'commercegurus' ),
        'id' => 'sidebar-1',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<p class="widget-title"><span>',
        'after_title' => '</span></p>',
    ) );

    unregister_sidebar( 'first-footer' );
    register_sidebar( array(
        'name' => __( 'First Footer', 'commercegurus' ),
        'id' => 'first-footer',
        'before_widget' => '<div id="%1$s" class="col-lg-3 col-md-3 col-sm-6 col-xs-12 col-nr-3 %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<p class="widget-title">',
        'after_title' => '</p>',
    ) );

    unregister_sidebar( 'second-footer' );
    register_sidebar( array(
        'name' => __( 'Second Footer', 'commercegurus' ),
        'id' => 'second-footer',
        'before_widget' => '<div id="%1$s" class="col-lg-3 col-md-3 col-sm-6 col-xs-12 col-nr-3 %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<p class="widget-title">',
        'after_title' => '</p>',
    ) );

    unregister_sidebar( 'mobile-search' );
    register_sidebar( array(
        'name' => __( 'Mobile Search', 'commercegurus' ),
        'id' => 'mobile-search',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<p class="widget-title"><span>',
        'after_title' => '</span></p>',
    ) );

    unregister_sidebar( 'header-search' );
    register_sidebar( array(
        'name' => __( 'Header Search', 'commercegurus' ),
        'id' => 'header-search',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<p class="widget-title"><span>',
        'after_title' => '</span></p>',
    ) );

    unregister_sidebar( 'shop-sidebar' );
    register_sidebar( array(
        'name' => __( 'Shop Sidebar', 'commercegurus' ),
        'id' => 'shop-sidebar',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<p class="widget-title"><span>',
        'after_title' => '</span></p>',
    ) );
}
add_action( 'widgets_init', 'ag_remove_some_widgets_blog', 11 );

function ag_scripts() {
    wp_enqueue_script( 'apollon_scripts', plugin_dir_url( __FILE__ ) . 'assets/js/apollon-scripts.js', array( 'jquery' ), 20181227, true );
    wp_enqueue_style( 'ag-styles', plugin_dir_url( __FILE__ ) . 'assets/css/ag_styles.css', false, 20190221 );
}
add_action( 'wp_enqueue_scripts', 'ag_scripts' );

//change heading of visual composer toggle
add_filter( 'wpb_toggle_heading', 'ag_custom_toggle_heading' , 10, 2);
function ag_custom_toggle_heading($heading, $atts){
    $heading = '<p class="toggle_heading">'.$atts['title'].'</p>';
    return $heading;
}

function ag_get_sitemap( $atts ) {
    
    $default = array(
        'heading' => 'h2',
        'exclude' => ''
    );

    $atts = shortcode_atts( $default, $atts );

    $exclude = '';


    $args = array(
        'post_type' => 'post',
        'posts_per_page' => -1,
        'meta_query' => array(
            'relation' => 'OR',
            array(
                'key' => '_yoast_wpseo_meta-robots-noindex',
                'compare' => 'NOT EXISTS'
            ),
            array(
                'key' => '_yoast_wpseo_meta-robots-noindex',
                'value' => '2',
            )
        ),
    );

    if($atts['exclude'])
        $args['post__not_in'] =  explode(',',$atts['exclude']);

    $post_counts = wp_count_posts();
    $sitemap = '';
    $item_template = '<li><a href="%1$s" title="%2$s">%2$s</a></li>';
    if ( $post_counts->publish > 0 ) {

        $sitemap .= sprintf( '<%2$s>%1$s</%2$s>', 'Posts:', $default['heading'] );
        $posts = get_posts( $args );
        $sitemap .= '<ul>';
        foreach ($posts as $single_post) {
            $sitemap .= sprintf($item_template, get_permalink($single_post->ID), get_the_title($single_post->ID));
        }
        $sitemap .= '</ul>';


        $sitemap .= sprintf( '<%2$s>%1$s</%2$s>', 'Categories:', $default['heading'] );
        $cats = get_terms( 'category' );
        $sitemap .= '<ul>';
        foreach ($cats as $cat) {
            $sitemap .= sprintf($item_template, get_term_link( $cat, 'category' ), $cat->name );
        }
        $sitemap .= '</ul>';


        $args['post_type'] = 'product';
        $sitemap .= sprintf( '<%2$s>%1$s</%2$s>', 'Products:', $default['heading'] );
        $products = get_posts( $args );
        $sitemap .= '<ul>';
        foreach ($products as $single_product) {
            $sitemap .= sprintf($item_template, get_permalink($single_product->ID), get_the_title($single_product->ID));
        }
        $sitemap .= '</ul>';


        $sitemap .= sprintf( '<%2$s>%1$s</%2$s>', 'Product Categories:', $default['heading'] );
        $product_cats = get_terms( 'product_cat' );
        $sitemap .= '<ul>';
        foreach ($product_cats as $product_cat) {
            $term_link = str_replace('/product-category/', '/', get_term_link( $product_cat, 'product_cat' ) );
            $sitemap .= sprintf($item_template, $term_link, $product_cat->name );
        }
        $sitemap .= '</ul>';


        $args['post_type'] = 'page';
        $sitemap .= sprintf( '<%2$s>%1$s</%2$s>', 'Pages:', $default['heading'] );
        $pages = get_posts( $args );
        $sitemap .= '<ul>';
        foreach ($pages as $single_page) {
            $sitemap .= sprintf($item_template, get_permalink($single_page->ID), get_the_title($single_page->ID));
        }
        $sitemap .= '</ul>';
    }

    // $sitemap = wp_kses_post( $sitemap );

    return $sitemap;

}
add_shortcode( 'apollon_guru_sitemap', 'ag_get_sitemap' );

add_filter( 'woocommerce_cart_item_remove_link', function($link, $cart_item_key){
    $link = '<span data-cart_item="'.$cart_item_key.'" class="my-cg-cart-remove" title="Remove this item">x</span>';
    return $link;
}, 20, 2 );

function remove_item_from_cart() {
    $cart_item = $_POST['cart_item'];
    $remove_cart_item = false;
    $return = array('status' => 'fail');
    if($cart_item){
        $remove_cart_item = WC()->cart->remove_cart_item($cart_item);
        $data['amount'] = WC()->cart->cart_contents_total + WC()->cart->tax_total;
        $data['count'] = WC()->cart->get_cart_contents_count();
        wp_send_json_success($data);
    } else {
        wp_send_json_error();
    }
    exit();
    return;
}

add_action('wp_ajax_remove_item_from_cart', 'remove_item_from_cart');
add_action('wp_ajax_nopriv_remove_item_from_cart', 'remove_item_from_cart');

add_filter( 'woocommerce_loop_add_to_cart_link', function($link, $product, $args){
    if($product->supports( 'ajax_add_to_cart' )){
        $link = sprintf( '<span data-quantity="%s" class="%s" %s>%s</span>',
            esc_attr( isset( $args['quantity'] ) ? $args['quantity'] : 1 ),
            esc_attr( isset( $args['class'] ) ? $args['class'] : 'button' ),
            isset( $args['attributes'] ) ? wc_implode_html_attributes( $args['attributes'] ) : '',
            esc_html( $product->add_to_cart_text() )
        );
    }
    return $link;
}, 10, 3 );

add_action('wp', 'ag_canonical_request');
function ag_canonical_request()
{
    if(is_ajax() || is_admin())
    {
        return;
    }

    global $page, $post, $wp_query, $woocommerce;

    $uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2);

    $path = substr($uri_parts[0], -1) == '/' ? $uri_parts[0] : $uri_parts[0] . '/';

    $path = preg_replace('/\/{2,}/', '/', $path);

    if($path != strtolower($path)){
        $wp_query->set_404();
        status_header( 404 );
        get_template_part( 404 );
        exit();
    }
    
    // post, page, attachment, preview
    if ( is_ajax() || is_admin() || !is_singular() || is_preview() || is_checkout() || is_checkout_pay_page() || get_query_var( 'amp' ) )
    {
        return;
    }
    $permalink = get_permalink();
    // We don't have access to the number of sub pages here.
    // So we have to hack.
    $max_pages = substr_count(
        $post->post_content, '<!--nextpage-->') + 1;
    if ( 1 < $page and $page <= $max_pages )
    {
        /*
         * Handle different permalink settings, eg:
         * /%year%/%postname%.html or
         * /%year%/%postname%/
         */
        $rev_perma_struct = strrev(get_option('permalink_structure'));
        if ( '/' != $rev_perma_struct[0] )
        {
            $permalink .= "/$page";
        }
        else
        {
            $permalink .= "$page/";
        }
    }
    /*$host_uri       = 'http'
                    . ( empty ( $_SERVER['HTTPS'] ) ? '' : 's' )
                    . '://' . $_SERVER['HTTP_HOST'];
    
    // $host_uri = str_replace('www.', '', $host_uri);

    $canonical_path = str_replace($host_uri, '', $permalink);*/

    $canonical_path = parse_url($permalink, PHP_URL_PATH);

    if ( ! empty ( $_GET ) )
    {
        global $wp;
        // Array
        $allowed = $wp->public_query_vars;
        $out_arr = array();
        foreach ( $_GET as $k => $v )
        {
            if ( in_array($k, $allowed ) )
            {
                $out_arr[] = $k . ( empty ( $v ) ? '' : "=$v" );
            }
        }
        if ( ! empty ( $out_arr ) )
        {
            $canonical_path .= '?' . implode('&', $out_arr);
        }
    }

    if ( $canonical_path == $path )
    {
        return;
    }

    // check is this page with woocommerce endpoint
    if(function_exists('is_wc_endpoint_url') && is_wc_endpoint_url()){
       return;
    }

    // Debug current result:
    #print '<pre>' . var_export($canonical_path, TRUE) . '</pre>';
    // Change it or return 'false' to stop the redirect.
    if ( FALSE != $canonical_path )
    {
        $wp_query->set_404();
        status_header( 404 );
        get_template_part( 404 );
        exit();
    }
    return;
}

/**
 * делаем вид, что на сайте только один автор и все статьи его
 */
class AG_Default_Author
{
    static $author_slug = 'admin',
            $author_data = null;
    
    function __construct()
    {
        // выводим на странице автора все записи, а не только те, где он автор
        add_action( 'pre_get_posts', array( $this, 'show_all_posts_on_author_page') );

        // редиректим со страниц других авторов на 404
        add_action( 'template_redirect', array( $this, 'redirect_not_other_authors' ) );

        // меняем заголовок Н1 архива автора
        add_filter( 'gettext', array( $this, 'fix_author_archive_h1' ), 10, 3 );

        /**
         * возвращаем значение переменной запроса author_name обратно в admin
         * (в частности надо для работы <link rel="next" (или prev))
         */
        add_action( 'wp', array( $this, 'set_author_name' ) );
    }

    function show_all_posts_on_author_page($query)
    {
        
        if ( !is_admin() && $query->is_main_query() && $query->is_author) {
        
            $query->set('author_name', false);

        }

    }

    function redirect_not_other_authors()
    {
        global $wp_query;
        
        if( $wp_query->is_author && $wp_query->query['author_name'] != AG_Default_Author::$author_slug ){
        
            $wp_query->set_404();
        
        }
    }

    function fix_author_archive_h1( $translation, $text, $domain )
    {
        if($translation == 'Author: %s'){

            $author_data = AG_Default_Author::get_author();
            
            $translation = '<span class="vcard">' . $author_data->data->display_name . '</span>';
        }

        return $translation;
    }

    static function get_author()
    {
        if( !AG_Default_Author::$author_data ){
            
            AG_Default_Author::$author_data = get_user_by('slug', AG_Default_Author::$author_slug );
        
        }
        
        return AG_Default_Author::$author_data;
    }

    function set_author_name()
    {
        global $wp_query;

        if ( !is_admin() && $wp_query->is_main_query() && $wp_query->is_author) {
        
            $wp_query->set('author_name', AG_Default_Author::$author_slug);

        }
    }
}

new AG_Default_Author();

if ( !function_exists( 'cg_posted_on' ) ) :

    /**
     * Prints HTML with meta information for the current post-date/time and author.
     */
    function cg_posted_on() {

        if(!is_single())
            return;

        if( !class_exists('AG_Default_Author') )
            return;

        $author = AG_Default_Author::get_author();

        $author_id = $author->ID;

        $time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';

        $time_string = sprintf( $time_string, esc_attr( get_the_date( 'c' ) ), esc_html( get_the_date() ), esc_attr( get_the_modified_date( 'c' ) ), esc_html( get_the_modified_date() )
        );

        printf( 
            __( '<span class="posted-on">Posted</span><span class="byline"> by %1$s</span>', 'commercegurus' ), 
            sprintf( '<span class="author"><a class="url fn n" href="%1$s">%2$s</a></span>', 
                esc_url( get_author_posts_url( $author_id ) ), 
                esc_html( get_the_author_meta('display_name', $author_id) )
                )
        );
    }

endif;

// add_filter('woocommerce_get_price_html', 'ag_custom_price', 99, 2 );
function ag_custom_price($price, $product)
{
    if($seo_snippets_price = (float)get_post_meta( get_the_id(), 'seo_snippets_price', true ))
    {
        $currency_symbol = get_woocommerce_currency_symbol();
        $price .= '<span class="amount"> (' . $currency_symbol . $seo_snippets_price . ' per pill)</span>';
    }
    return $price;
}

add_filter('after_setup_theme', 'remove_redundant_shortlink');
function remove_redundant_shortlink() {
    // remove HTML meta tag
    // <link rel='shortlink' href='http://example.com/?p=25' />
    remove_action('wp_head', 'wp_shortlink_wp_head', 10);

    // remove HTTP header
    // Link: <https://example.com/?p=25>; rel=shortlink
    remove_action( 'template_redirect', 'wp_shortlink_header', 11);

    add_filter( 'wpseo_next_rel_link', '__return_false' );
    add_filter( 'wpseo_prev_rel_link', '__return_false' );
}

add_action( 'after_setup_theme', function(){
    add_action('do_feed', 'ag_disable_feed', 1);
    add_action('do_feed_rdf', 'ag_disable_feed', 1);
    add_action('do_feed_rss', 'ag_disable_feed', 1);
    add_action('do_feed_rss2', 'ag_disable_feed', 1);
    add_action('do_feed_atom', 'ag_disable_feed', 1);
    add_action('do_feed_rss2_comments', 'ag_disable_feed', 1);
    add_action('do_feed_atom_comments', 'ag_disable_feed', 1);
} );

function ag_disable_feed() {
    wp_redirect(get_option('siteurl'),301);
    exit;
}

remove_action( 'wp_head', 'feed_links_extra', 3 );
remove_action( 'wp_head', 'feed_links', 2 );

// remove 'product-category' from structured data BreadcrumbList
add_filter( 'wpseo_json_ld_output', 'ag_bread_crumbs_sd_link_fix', 10, 2 );
function ag_bread_crumbs_sd_link_fix($data, $context)  
{
    if('breadcrumb' == $context && array_key_exists('itemListElement', $data) && is_array($data['itemListElement']) ){
        foreach ($data['itemListElement'] as $key => $list_item) {
            $data['itemListElement'][$key]['item']['@id'] = str_replace('/product-category/', '/', $list_item['item']['@id']);
        }
    }
    return $data;
}

/*add_action( 'woocommerce_product_options_general_product_data', 'ag_create_product_custom_field', 11 );
function ag_create_product_custom_field() {
    $args_field_name = array(
        'id' => 'seo_structured_data_name',
        'label' => __( 'SEO Structured Data Name' ),
        'class' => 'ag-custom-field',
    );
    woocommerce_wp_text_input( $args_field_name );

    $args_field_brand = array(
        'id' => 'seo_structured_data_brand',
        'label' => __( 'SEO Structured Data Brand' ),
        'class' => 'ag-custom-field',
    );
    woocommerce_wp_text_input( $args_field_brand );

    $args_field_identifier = array(
        'id' => 'seo_structured_data_prod_identifier',
        'label' => __( 'SEO Structured Data Identifier' ),
        'class' => 'ag-custom-field',
    );
    woocommerce_wp_text_input( $args_field_identifier );
}
*/
/*add_action( 'woocommerce_process_product_meta', 'ag_save_product_custom_field' );
function ag_save_product_custom_field( $post_id ) {
    $product = wc_get_product( $post_id );
    
    $sd_name = isset( $_POST['seo_structured_data_name'] ) ? $_POST['seo_structured_data_name'] : '';
    $product->update_meta_data( 'seo_structured_data_name', sanitize_text_field( $sd_name ) );

    $sd_brand = isset( $_POST['seo_structured_data_brand'] ) ? $_POST['seo_structured_data_brand'] : '';
    $product->update_meta_data( 'seo_structured_data_brand', sanitize_text_field( $sd_brand ) );

    $sd_prod_identifier = isset( $_POST['seo_structured_data_prod_identifier'] ) ? $_POST['seo_structured_data_prod_identifier'] : '';
    $product->update_meta_data( 'seo_structured_data_prod_identifier', sanitize_text_field( $sd_prod_identifier ) );
    
    $product->save();
}*/

add_filter( 'woocommerce_structured_data_product', 'ag_fix_woocommerce_structured_data_product', 10, 2 );
function ag_fix_woocommerce_structured_data_product($markup, $product)
{

    if( !array_key_exists('sku', $markup) || !$markup['sku']){
        
        $markup['sku'] = $product->get_id();

    }

    if($sd_name = $product->get_meta( 'seo_structured_data_name' )){
        $markup['name'] = $sd_name;
    }

    if($sd_brand = $product->get_meta( 'seo_structured_data_brand' )){
        $markup['brand'] = array(
            "@type" => "Thing",
            "name" => $sd_brand
        );
    }

    if($sd_prod_identifier = $product->get_meta( 'seo_structured_data_prod_identifier' )){
        $markup['mpn'] = $sd_prod_identifier;
    }

    unset($markup['@id']);

    return $markup;
}

// set offers count and seller name from Yoast SEO plugin
add_filter( 'woocommerce_structured_data_product_offer', 'ag_fix_woocommerce_structured_data_product_offer', 10, 2 );
function ag_fix_woocommerce_structured_data_product_offer($markup_offer, $product)
{
    if( !array_key_exists('offerCount', $markup_offer) && $product->product_type == 'variable'){

        $avaialable_variations = $product->get_available_variations();

        $markup_offer['offerCount'] = count($avaialable_variations);
    }

    // get seller name from Yoast SEO plugin
    $wpseo = get_option( 'wpseo' );
    
    if(@$wpseo['company_name']){
        
        $markup_offer['seller']['name'] = $wpseo['company_name'];

    }

    if(!array_key_exists('priceValidUntil', $markup_offer) || !$markup_offer['priceValidUntil']){
        $markup_offer['priceValidUntil'] = ((int) date("Y") + 1) . '-12-31'; 
    }

    return $markup_offer;
}

/*-------------------------------------------------------------------------------------------------*/
add_filter( 'woocommerce_product_data_tabs', 'ag_create_product_custom_field_tab' , 99 , 1 );
function ag_create_product_custom_field_tab( $product_data_tabs ) {
    $product_data_tabs['apollonguru-tab'] = array(
        'label' => __( 'Seo Structured Data' ),
        'target' => 'ag_seo_structured_data',
    );
    return $product_data_tabs;
}

add_action( 'woocommerce_product_data_panels', 'ag_create_product_custom_field' );
function ag_create_product_custom_field() {
    global $woocommerce, $post;
    ?>
    <!-- id below must match target registered in above ag_create_product_custom_field_tab function -->
    <div id="ag_seo_structured_data" class="panel woocommerce_options_panel">
        <?php
        $args_field_name = array(
            'id' => 'seo_structured_data_name',
            'label' => __( 'SEO Structured Data Name' ),
            'class' => 'ag-custom-field',
        );
        woocommerce_wp_text_input( $args_field_name );

        $args_field_brand = array(
            'id' => 'seo_structured_data_brand',
            'label' => __( 'SEO Structured Data Brand' ),
            'class' => 'ag-custom-field',
        );
        woocommerce_wp_text_input( $args_field_brand );

        $args_field_identifier = array(
            'id' => 'seo_structured_data_prod_identifier',
            'label' => __( 'SEO Structured Data Identifier' ),
            'class' => 'ag-custom-field',
        );
        woocommerce_wp_text_input( $args_field_identifier );
        
        /*woocommerce_wp_checkbox( array( 
            'id'            => '_my_custom_field', 
            'wrapper_class' => 'show_if_simple', 
            'label'         => __( 'My Custom Field Label' ),
            'description'   => __( 'My Custom Field Description' ),
            'default'       => '0',
            'desc_tip'      => false,
        ) );*/
        ?>
    </div>
    <?php
}

add_action( 'woocommerce_process_product_meta', 'ag_save_product_custom_field' );
function ag_save_product_custom_field( $post_id ) {
    $product = wc_get_product( $post_id );
    
    $sd_name = isset( $_POST['seo_structured_data_name'] ) ? $_POST['seo_structured_data_name'] : '';
    $product->update_meta_data( 'seo_structured_data_name', sanitize_text_field( $sd_name ) );

    $sd_brand = isset( $_POST['seo_structured_data_brand'] ) ? $_POST['seo_structured_data_brand'] : '';
    $product->update_meta_data( 'seo_structured_data_brand', sanitize_text_field( $sd_brand ) );

    $sd_prod_identifier = isset( $_POST['seo_structured_data_prod_identifier'] ) ? $_POST['seo_structured_data_prod_identifier'] : '';
    $product->update_meta_data( 'seo_structured_data_prod_identifier', sanitize_text_field( $sd_prod_identifier ) );

    /*$woo_checkbox = isset( $_POST['_my_custom_field'] ) ? 'yes' : 'no';
    $product->update_meta_data( '_my_custom_field', sanitize_text_field( $woo_checkbox ) );*/
    
    $product->save();
}
/*------------------------------------------------------------------------------*/
/*
add_filter( 'ampforwp_modify_rel_canonical', function($amp_url){
    echo '<pre style="display:none;">$amp_url = '.htmlspecialchars(print_r($amp_url, true)).'</pre>';
    return '$amp_url';
},10);*/

// require_once plugin_dir_path( __FILE__ ) . 'apollon_product_filter.php';

add_shortcode( 'ag_dcma', 'ag_dcma_protected' );
function ag_dcma_protected($atts)
{
    $html = '';
    if($atts['id']){
        $id = $atts['id'];
        $permalink = do_shortcode( '[the_permalink]' );
        if(!get_query_var( 'amp' )){
            $html = '<a href="https://www.dmca.com/Protection/Status.aspx?ID='.$id.'&refurl='.$permalink.'" title="DMCA.com Protection Status" class="dmca-badge"> <img src ="https://images.dmca.com/Badges/_dmca_premi_badge_4.png?ID='.$id.'&refurl='.$permalink.'"  alt="DMCA.com Protection Status" /></a>  <script src="https://images.dmca.com/Badges/DMCABadgeHelper.min.js"> </script>';
        }
    }
    return $html;
}

function ag_add_img_title( $attr, $attachment = null ) {
 
    $img_title = trim( strip_tags( $attachment->post_title ) );
 
    $attr['title'] = $img_title;
    $attr['alt'] = $img_title;
 
    return $attr;
}
add_filter( 'wp_get_attachment_image_attributes','ag_add_img_title', 10, 2 );

add_filter( "get_the_author_display_name", function($value){
    
    $userdata = get_userdata( 1 );
    
    return $userdata->display_name;
});

add_action( 'woocommerce_before_shop_loop_item', function(){
    remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open' );
    remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5 );
}, 4 );
<?php
add_action( 'wp', function(){
    if ( ! function_exists( 'vc_set_shortcodes_templates_dir' ) ) return;

    $plugin_dir = plugin_dir_path( __FILE__ ) . 'vc_templates';

    vc_set_shortcodes_templates_dir( $plugin_dir );
    
} );
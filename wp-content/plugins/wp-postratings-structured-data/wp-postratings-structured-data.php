<?php
/**
 * Plugin Name:       WP-PostRatings Structured data fix
 * Description:       Fix Structured Data AgregateRating for wp-postratings
 * Version:           1.0.0
 * Author:            apollon.guru
 * Author URI:		  https://apollon.guru/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 */

// If this file is called directly, abort.

if ( ! defined( 'ABSPATH' ) ) {
    die;
}

// WordPress Structured Data AgregateRating Fix
add_filter('wp_postratings_google_structured_data', 'wp_pr_sd_fix');
function wp_pr_sd_fix($google_structured_data){

    global $post;

    if( get_query_var('amp') || !is_singular('post') ){
        return '';
    }

	// $post_types_exclude = array('shopannouncements', 'product');
	//
    // if( in_array($post->post_type, $post_types_exclude) )
    //     return '';

    if( is_singular() && is_main_query() ) {

        $ratings_max = (int) get_option( 'postratings_max' );
        $post_ratings_data = get_post_custom();
        $post_ratings_average = is_array( $post_ratings_data ) && array_key_exists( 'ratings_average', $post_ratings_data ) ? (float) $post_ratings_data['ratings_average'][0] : 0;
        $post_ratings_users = is_array( $post_ratings_data ) && array_key_exists( 'ratings_users', $post_ratings_data ) ? (int) $post_ratings_data['ratings_users'][0] : 0;

        $structured_data = array(
            '@context'          => 'http://schema.org',
            '@type'             => 'Article',
            'headline'          => $post->post_title,
            'datePublished'     => get_the_date( 'c', $post ),
            'dateModified'      => get_post_modified_time('c', $post),
            'mainEntityOfPage'  => get_permalink($post),
        );


        $autor_nickname = get_author_name($post->post_author);
		if( has_post_thumbnail( $post->ID ) ){
            $image = '"'.get_the_post_thumbnail_url($post->ID).'"';
            $structured_data['image'] = get_the_post_thumbnail_url($post->ID);
        } else {
            $image = '{"@id": "#logo"}';
            $structured_data['image'] = array("@id" => "#logo");
        }
        $url = get_permalink($post);

        $wpseo = get_option( 'wpseo' );

        $organization_arr = array(
            '@type' => 'Organization',
            'url'   => get_bloginfo( 'url' ),
        );

        // $organization  = "{";
        // $organization .= "\"@type\": \"Organization\"";
        // $organization .= ",\"url\": \"".get_bloginfo( 'url' )."\"";
        
        if(@$wpseo['company_name']){
            // $organization .= ",\"name\": \"".$wpseo['company_name']."\"";
            $organization_arr['name'] = $wpseo['company_name'];
        } else {
            // $organization .= ",\"name\": \"".get_bloginfo( 'name' )."\"";
            $organization_arr['name'] = get_bloginfo( 'name' );
        }
       
        if(@$wpseo['company_logo']){
            // $organization .= ",\"logo\":{";
            // $organization .= "\"@type\": \"ImageObject\"";
            // $organization .= ",\"url\": \"".$wpseo['company_logo']."\"";
            // $organization .= "}";
            $organization_arr['logo'] = array(
                '@type' => 'ImageObject',
                'url'   => $wpseo['company_logo']
            );
        }
        // $organization .= "}";

        // $ratings_meta  = "<script type=\"application/ld+json\" class=\"wp-postratings-structured-data\">";
        // $ratings_meta .= "{";
        // $ratings_meta .=    "\"@context\": \"http://schema.org\"";
        // $ratings_meta .=    ",\"@type\": \"Article\"";
        // $ratings_meta .=    ",\"author\": \"$autor_nickname\"";
		// $ratings_meta .=    ",\"author\": {\"@id\": \"#organization\"}";
        // $ratings_meta .=    ",\"author\": " . $organization;
        // $ratings_meta .=    ",\"headline\": \"$post->post_title\"";
        // $ratings_meta .=    ",\"datePublished\": \"".get_the_date( 'c', $post )."\"";
        // $ratings_meta .=    ",\"dateModified\": \"".get_post_modified_time('c', $post)."\"";
        // $ratings_meta .=    ",\"image\": $image";

        // if($yoast_description = get_post_meta($post->ID, '_yoast_wpseo_metadesc', true)){

        //     $yoast_description = esc_attr( $yoast_description );

        //     $ratings_meta .=    ",\"description\": \"$yoast_description\"";
            
        // }


        // setup_postdata( $post );
        // $wp_content = do_shortcode(get_the_content());
        // $wp_content = htmlspecialchars($wp_content);
        // wp_reset_postdata( $post );

        // $ratings_meta .=    ",\"description\": \"$wp_content\"";

        // $structured_data['description'] = $wp_content;
        
        // $ratings_meta .=    ",\"publisher\": {\"@id\": \"#organization\"}";
        // $ratings_meta .=    ",\"publisher\": " . $organization;
        $structured_data['publisher'] = $organization_arr;
        // $structured_data['author'] = $organization_arr;
        $structured_data['author'] = array('@type' => 'Person', 'name' => $autor_nickname);
        // $ratings_meta .=    ",\"mainEntityOfPage\": \"$url\"";

        if( $post_ratings_average > 0 ) {
            // $ratings_meta .= ",\"aggregateRating\": {";
            // $ratings_meta .=     "\"@type\": \"AggregateRating\"";
            // $ratings_meta .=     ",\"ratingValue\": \"$post_ratings_average\"";
            // $ratings_meta .=     ",\"ratingCount\": \"$post_ratings_users\"";
            // $ratings_meta .=     ",\"bestRating\": \"$ratings_max\"";
            // $ratings_meta .=     ",\"worstRating\": \"1\"";
            // $ratings_meta .= " }";

            $structured_data['aggregateRating'] = array(
                '@type'         => 'AggregateRating',
                'ratingValue'   => $post_ratings_average,
                'ratingCount'   => $post_ratings_users,
                'bestRating'    => $ratings_max,
                'worstRating'   => 1
            );
        }

        // $ratings_meta .= "}";
        // $ratings_meta .= "</script>";

        // $google_structured_data =  $ratings_meta;

        $google_structured_data = "<script type=\"application/ld+json\" class=\"wp-postratings-structured-data\">";
        $google_structured_data .= wp_json_encode( $structured_data );
        $google_structured_data .= "</script>";
    }

    return $google_structured_data;

}

add_filter('wpseo_json_ld_output', 'wp_pr_sd_organization_logo_fix', 10, 2);
function wp_pr_sd_organization_logo_fix($data, $context){

    if('website' == $context)
        return;
    if('company' == $context){
        unset($data['@id']);
        $data['url'] = get_bloginfo( 'url' );
        /*$logo_url = $data['logo'];
        $data['logo'] = array(
            "@id" => "#logo",
            "@type" => "ImageObject",
            "url" => $logo_url
        );*/
    }
    return $data;
}

add_filter( 'wp_postratings_schema_itemtype', '__return_false' );

add_action( 'wp_head', 'vbb_wpseo_json_ld');
function vbb_wpseo_json_ld()
{
    global $wp_filter;
    if(!@$wp_filter["wpseo_json_ld"]){
        $vbb_wpseo_json_ld = new WPSEO_JSON_LD();
        $vbb_wpseo_json_ld->json_ld();
    }
}

add_filter('the_content', 'vbb_add_rating_to_content');
function vbb_add_rating_to_content($content) {
    static $posts_ids_printed_ratings = array();

    if(!function_exists('the_ratings')) return $content;

    if(get_query_var( 'amp' )) return $content;

    global $post;

    if('post' != $post->post_type) return $content;

    if(in_array($post->ID, $posts_ids_printed_ratings ))  return $content;

    $posts_ids_printed_ratings[] = $post->ID;

    $content = the_ratings('div', 0, false) . $content;

    return $content;
}
<?php

if(!function_exists('vbb_taxonomy_slug_rewrite')){

	add_filter('generate_rewrite_rules', 'vbb_taxonomy_slug_rewrite', 9999);

	function vbb_taxonomy_slug_rewrite($wp_rewrite)
	{
		global $wp_query;

		$rewrite = array(
			'product-category\/(.+?)\/amp/?$' => 'index.php?amp=1&product_cat=$matches[1]',
			'product-category\/(.+?)\/amp\/page/?([0-9]{1,})/?$' => 'index.php?amp=1&product_cat=$matches[1]&paged=$matches[2]'
		);
		$wc_rewrite_options = get_option( 'ns_rewrite' );

		if($wc_rewrite_options['redirect_enabled']){

			$prod_cats = get_terms( array(
				'taxonomy'		=> 'product_cat',
				'hide_empty'	=> false,
				'fields'		=> 'id=>slug',
			) );

			foreach ($prod_cats as $prod_cat) {
				$rewrite[$prod_cat . '/amp/?$'] = 'index.php?product_cat='.$prod_cat.'&amp=1';
				$rewrite[$prod_cat . '/amp/page/?([0-9]{1,})/?$'] = 'index.php?product_cat='.$prod_cat.'&amp=1&paged=$matches[1]';
			}

			$categories = get_terms( array(
				'taxonomy'		=> 'category',
				'hide_empty'	=> false,
				'fields'		=> 'id=>slug',
			) );

			foreach ($categories as $category) {
				$rewrite[$category . '/amp/?$'] = 'index.php?category_name='.$category.'&amp=1';
				$rewrite[$category . '/amp/page/?([0-9]{1,})/?$'] = 'index.php?category_name='.$category.'&amp=1&paged=$matches[1]';
			}

		}

		$wp_rewrite->rules = $rewrite + $wp_rewrite->rules;

		return $wp_rewrite;
	}

}

add_action( 'wp', function(){
	if(get_query_var( 'amp' )){
		remove_action('amp_post_template_css','amp_framework_logo_styles',11);
		// remove_action('ampforwp_body_beginning','ampforwp_body_beginning_html_output',11);

		// add_action('ampforwp_body_beginning','vb_ampforwp_body_beginning_html_output',11);

		add_shortcode( '4k_icon', 'vbb_amp_4k_icon_fix' );
		// add_shortcode( 'gtm_amp', 'gtm_amp_vb');

		// remove_shortcode('cg_promo');`
		// add_shortcode( 'cg_promo ', '__return_false' );
	}
} );

add_action( 'amp_post_template_footer', function(){
	if(is_plugin_active('structured-data-for-wp/structured-data-for-wp.php')) return;
	
	remove_filter( 'amp_post_template_footer', 'amp_post_template_add_schemaorg_metadata' );
	
	if(is_singular( 'product' )){
		add_action( 'amp_post_template_footer', 'vbb_amp_product_template_add_schemaorg_metadata' );
	} elseif(is_singular( 'post' )){
		add_action( 'amp_post_template_footer', 'vbb_amp_post_template_add_schemaorg_metadata' );
	}
}, 5 );

function vbb_amp_product_template_add_schemaorg_metadata($amp_template){
	global $product, $post;
	$shop_name = get_bloginfo( 'name' );
	$shop_url  = home_url();
	$currency  = get_woocommerce_currency();

	$metadata = $amp_template->get( 'metadata' );

	if ( empty( $metadata ) ) {
		return;
	}

	add_shortcode( '4k_icon', '__return_false' );

	$metadata['@type'] = 'Product';
	$metadata['name'] = $metadata['headline'];
	$metadata['@id'] = get_permalink( $product->get_id() );
	$metadata['description'] = wc_clean(wpautop( do_shortcode( $product->get_short_description() ? $product->get_short_description() : $product->get_description() ) ));
	$metadata['sku'] = $product->get_sku();

	add_shortcode( '4k_icon', 'vbb_amp_4k_icon_fix' );

	if(array_key_exists('image', $metadata) && array_key_exists('url', $metadata['image']) && $metadata['image']['url']){
		$metadata['image'] = $metadata['image']['url'];
	}

	if ( '' !== $product->get_price() ) {
		if ( $product->is_type( 'variable' ) ) {
			$prices  = $product->get_variation_prices();
			$lowest  = reset( $prices['price'] );
			$highest = end( $prices['price'] );

			if ( $lowest === $highest ) {
				$markup_offer = array(
					'@type' => 'Offer',
					'price' => wc_format_decimal( $lowest, wc_get_price_decimals() ),
				);
			} else {
				$markup_offer = array(
					'@type'     => 'AggregateOffer',
					'lowPrice'  => wc_format_decimal( $lowest, wc_get_price_decimals() ),
					'highPrice' => wc_format_decimal( $highest, wc_get_price_decimals() ),
				);
			}
		} else {
			$markup_offer = array(
				'@type' => 'Offer',
				'price' => wc_format_decimal( $product->get_price(), wc_get_price_decimals() ),
			);
		}

		$markup_offer += array(
			'priceCurrency' => $currency,
			'availability'  => 'https://schema.org/' . ( $product->is_in_stock() ? 'InStock' : 'OutOfStock' ),
			'url'           => $metadata['@id'],
			'seller'        => array(
				'@type' => 'Organization',
				'name'  => $shop_name,
				'url'   => $shop_url,
			),
		);

		$metadata['offers'] = array( apply_filters( 'woocommerce_structured_data_product_offer', $markup_offer, $product ) );
	}

	unset($metadata['headline']);
	unset($metadata['datePublished']);
	unset($metadata['dateModified']);
	unset($metadata['author']);
	unset($metadata['publisher']);
	unset($metadata['mainEntityOfPage']);

	$metadata = apply_filters( 'woocommerce_structured_data_product', $metadata, $product );
	?>
	<script type="application/ld+json"><?php echo wp_json_encode( $metadata ); ?></script>
	<?php
}

function vbb_amp_post_template_add_schemaorg_metadata($amp_template){
	$metadata = $amp_template->get( 'metadata' );

	// setup_postdata( $post );
	// $wp_content = do_shortcode(get_the_content());
	// $wp_content = htmlspecialchars($wp_content);
	// wp_reset_postdata( $post );

	// $metadata['description'] = $wp_content;

    $wpseo = get_option( 'wpseo' );
    $organization = array(
    	'@type'	=> 'Organization',
    	'url'	=> get_bloginfo( 'url' )
    );

    if(@$wpseo['company_name']){
        $organization['name'] = $wpseo['company_name'];
    } else {
        $organization['name'] = get_bloginfo( 'name' );
    }

    if(@$wpseo['company_logo']){
        $organization['logo'] = array(
        	'@type'	=> 'ImageObject',
        	'url'	=> $wpseo['company_logo']
        );
    }

	// $metadata['author'] = array('@id' => '#organization');
	// $metadata['publisher'] = array('@id' => '#organization');
	// $metadata['author'] = $organization;
	$metadata['publisher'] = $organization;
	if(array_key_exists('image', $metadata) && array_key_exists('url', $metadata['image']) && $metadata['image']['url']){
		$metadata['image'] = $metadata['image']['url'];
	}

	/*$ratings_max = (int) get_option( 'postratings_max' );
	$post_ratings_data = get_post_custom();
	$post_ratings_average = is_array( $post_ratings_data ) && array_key_exists( 'ratings_average', $post_ratings_data ) ? (float) $post_ratings_data['ratings_average'][0] : 0;
	$post_ratings_users = is_array( $post_ratings_data ) && array_key_exists( 'ratings_users', $post_ratings_data ) ? (int) $post_ratings_data['ratings_users'][0] : 0;*/

	// $autor_nickname = get_the_author_meta( 'nickname',  $post->post_author);

	/*if( $post_ratings_average > 0 ) {
		$metadata['aggregateRating'] = array();
		$metadata['aggregateRating']['@type'] = 'AggregateRating';
		$metadata['aggregateRating']['ratingValue'] = $post_ratings_average;
		$metadata['aggregateRating']['ratingCount'] = $post_ratings_users;
		$metadata['aggregateRating']['bestRating'] = $ratings_max;
		$metadata['aggregateRating']['worstRating'] = 1;
	}*/

	// if( has_post_thumbnail( $post->ID ) ){
	// 	$image = '"'.get_the_post_thumbnail_url($post->ID).'"';
	// } else {
	// 	$image = '{"@id": "#logo"}';
	// }
	?>
	<script type="application/ld+json"><?php echo wp_json_encode( $metadata ); ?></script>
	<?php

}

function vbb_amp_4k_icon_fix($atts){
	if($atts['icon'] != 'fa-check-sign'){
		return;
	}
	return '<i class="tick">&#x2714;</i>';
}

add_action( 'ampforwp_before_post_content', 'vbb_add_buy_btn');
add_action( 'ampforwp_after_post_content', 'vbb_add_buy_btn', 5);
function vbb_add_buy_btn($args)
{
	global $product;
	if(is_singular('product')) : ?>
		<?php woocommerce_template_loop_price(); ?>
		<div class="amp-add-to-cart-link">
			<?php if($product->is_type( 'simple' ) && $product->is_in_stock()) : ?>
				<form action="/cart/" method="get" target="_top">
					<input type="hidden" name="add-to-cart" value="<?php echo $product->get_id();?>">
					<input type="number" name="quantity" value="1">
					<input type="submit" value="Add to Cart">
				</form>
			<?php else: ?>
				<?php woocommerce_template_loop_add_to_cart(); ?>
			<?php endif; ?>
		</div>
	<?php endif;
}

add_action( 'amp_post_template_head', 'vbb_amp_tabs_script' );
function vbb_amp_tabs_script($arg){
	if(is_singular( 'product' )){
		echo '<script async custom-element="amp-selector" src="https://cdn.ampproject.org/v0/amp-selector-0.1.js"></script>';
	}
}

add_action( 'amp_post_template_css', 'vbb_amp_tabs_styles' );
function vbb_amp_tabs_styles($arg){
	$styles_dir = AMP_VBB_PLUGIN_PATH . 'elements/styles/';
	include $styles_dir . 'style-vbb.php';
	if(is_singular( 'product' )) {
		include $styles_dir . 'style-vbb-single.php';
	}
}

function vbb_amp_print_reviews($product_id){
	$reviews = vbb_amp_get_yp_custom_reviews(array('product_id' => $product_id));

	if(!$reviews){return false;}

	$html = '<div><p>Reviews</p>';

	foreach ($reviews as $review) {

		$html .= '<div>';

		$html .= '<div class="yp-r-name">' . $review->name . '</div>';

		$html .= '<div class="yp-r-title">' . $review->title . '</div>';

		$html .= '<div class="yp-r-content">' . $review->content . '</div>';

		$html .= '<div class="yp-r-product_name">' . $review->product_name . '</div>';

		$html .= '</div>';
	}

	$html .= '</div>';

	echo $html;
}

function vbb_amp_get_yp_custom_reviews($args){
	$defaults = array(
		'product_id' => 0,
		'page_num' => 1,
		'limit' => 30,

	);

	$args = wp_parse_args( $args, $defaults );

	if(!$args['product_id']){
		return false;
	}

	global $wpdb;

	$yotpo_custom_table = $wpdb->prefix . 'yotpo_custom';

	$offset = ($args['page_num'] - 1) * $args['limit'];

	$sql = $wpdb->prepare(
		"SELECT * FROM `$yotpo_custom_table` WHERE `sku` = %d ORDER BY `created_at` DESC  LIMIT %d, %d",
		array($args['product_id'], $offset, $args['limit']) );

	$product_yotpo_reviews = $wpdb->get_results( $sql );

	return $product_yotpo_reviews;
}

function vbb_add_cart_icon()
{
	global $woocommerce; ?>
	<div class="tiny-cart">
		<a class="cart_dropdown_link cart-parent" href="<?php echo esc_url( WC()->cart->get_cart_url() ); ?>" title="<?php _e( 'View your shopping cart', 'commercegurus' ); ?>">
			<div class="cg-header-cart-icon-wrap">
				<div class="icon cg-icon-shopping-1"></div>
				<span class="cg-cart-count"><?php echo WC()->cart->cart_contents_count; ?></span>
			</div>
			<span class='cart_subtotal'><?php echo $woocommerce->cart->get_cart_subtotal(); ?></span>
		</a>
	</div>
<?php
}

add_filter( 'get_the_archive_title', 'vbb_amp_change_category_title');
function vbb_amp_change_category_title ($title) {

	if(get_query_var( 'amp' ) && is_product_category())
		$title = single_cat_title( '', false );

	return $title;

};

function vbb_ampforwp_framework_get_post_navigation(){
	global $redux_builder_amp;
	if($redux_builder_amp['enable-single-next-prev']) { ?>
		<div id="pagination">
					<div class="next">
					<?php $next_post = get_next_post();
						if (!empty( $next_post )) {
						$next_text = $next_post->post_title; ?>
						<a href="<?php echo ampforwp_url_controller( get_permalink( $next_post->ID ) ); ?>"><?php echo apply_filters('ampforwp_next_link',$next_text ); ?> &rarr;</a> <?php
						} ?>
					</div>

					<div class="prev">
					<?php $prev_post = get_previous_post();
					 if (!empty( $prev_post )) {
						 $prev_text = $prev_post->post_title;
						  ?>
						<a href="<?php echo ampforwp_url_controller( get_permalink( $prev_post->ID ) ); ?>"> &larr; <?php echo apply_filters('ampforwp_prev_link',$prev_text ); ?></a> <?php
					 } ?>
					</div>

					<div class="clearfix"></div>
		</div>
	<?php }
}

add_action( 'vbb_amp_after_loop', 'vbb_amp_product_cat_description' );
function vbb_amp_product_cat_description()
{
	$t_id = get_queried_object()->term_id;
	$term_meta = get_option( "taxonomy_$t_id" );

	if ( is_array( $term_meta ) ) {
		$term_meta = $term_meta['custom_term_meta'];
		if ( is_tax( array('product_cat', 'product_tag') ) && get_query_var( 'paged' ) == 0 ) {
			$description = wpautop( do_shortcode( $term_meta ) );
			if ( $description ) {

				$sanitizer = new AMPFORWP_Content( $description, array(),
				apply_filters( 'ampforwp_content_sanitizers',
					array(
						'AMP_Style_Sanitizer' 		=> array(),
						'AMP_Blacklist_Sanitizer' 	=> array(),
						'AMP_Img_Sanitizer' 		=> array(),
						'AMP_Video_Sanitizer' 		=> array(),
						'AMP_Audio_Sanitizer' 		=> array(),
						'AMP_Iframe_Sanitizer' 		=> array(
							'add_placeholder' 		=> true,
						)
					) ) );
				$arch_desc 		= $sanitizer->get_amp_content();
				echo $arch_desc;
			}
		}
	}
}

add_action( 'amp_meta', function(){
	$wpseo_json_ld = new WPSEO_JSON_LD();
	$wpseo_json_ld->json_ld();
} );

add_filter('ampforwp_query_args', 'vbb_amp_fix_query_args');

function vbb_amp_fix_query_args($args)
{

	if($args['post_type'] == 'product'){
		$args['posts_per_page'] = 12;
		// unset($args['post__not_in']);
	}
	unset($args['author']);
	return $args;
}

function vbb_amp_loop_image( $data=array() ) {
	global $ampLoopData, $counterOffset, $redux_builder_amp, $product;
	if (ampforwp_has_post_thumbnail()  ) {

		$tag 				= 'div';
		$tag_class 			= '';
		$layout_responsive 	= '';
		$imageClass 		= '';
		$imageSize 			= 'thumbnail';

		if ( isset($data['tag']) && $data['tag'] != "" ) {
			$tag = $data['tag'];
		}

		if ( isset($data['responsive']) && $data['responsive'] != "" ) {
			$layout_responsive = 'layout=responsive';
			}

		if ( isset($data['tag_class']) && $data['tag_class'] != "" ) {
			$tag_class = $data['tag_class'];
		}
		if ( isset($data['image_class']) && $data['image_class'] != "" ) {
			$imageClass = $data['image_class'];
		}
		if ( isset($data['image_size']) && $data['image_size'] != "" ) {
			$imageSize = $data['image_size'];
		}
		$thumb_url = ampforwp_get_post_thumbnail('url', $imageSize);
		$thumb_width = ampforwp_get_post_thumbnail('width', $imageSize);
		$thumb_height = ampforwp_get_post_thumbnail('height', $imageSize);


		if ( isset($data['image_crop']) && $data['image_crop'] != "" ) {
			$width = $data['image_crop_width'];
			if ( empty($width) ) {
				$width = $thumb_width;
			}
			$height = $data['image_crop_height'];
			if ( empty($height) ) {
				$height = $thumb_height;
			}
			if ( isset($redux_builder_amp['ampforwp-retina-images']) && true == $redux_builder_amp['ampforwp-retina-images'] ) {
				$width = $width * 2;
				$height = $height * 2;
			}
			$thumb_url_array = ampforwp_aq_resize( $thumb_url, $width, $height, true, false, true ); //resize & crop the image
			$thumb_url = $thumb_url_array[0];
			$thumb_width = $thumb_url_array[1];
			$thumb_height = $thumb_url_array[2];
		}
		$in_stock_sign = '';
		if($product && $product->is_on_sale()){
			$in_stock_sign = '<div class="onsale-wrap"><div class="onsale-inner"><span class="onsale">'.__( 'Sale!', 'woocommerce' ).'</span></div></div>';
		}
		if ( $thumb_url ) {
			echo '<'.$tag.' class="loop-img '.$tag_class.'">';
			echo $in_stock_sign;
			echo '<a href="'.amp_loop_permalink(true).'">';
			echo '<amp-img src="'. $thumb_url .'" width="'.$thumb_width.'" height="'.$thumb_height.'" '. $layout_responsive .' class="'.$imageClass.'"></amp-img>';
			echo '</a>';
			echo '</'.$tag.'>';
		}
	 }
}

function vbb_ampforwp_framework_get_featured_image(){
	global $loadComponent, $product;
	if(!isset($loadComponent['AMP-featured-image']) || $loadComponent['AMP-featured-image']!=true){
		return;
	}
	do_action('ampforwp_before_featured_image_hook');
	global $post, $redux_builder_amp;
	$post_id 		= $post->ID;
	$featured_image = "";
	$amp_html 		= "";
	$caption 		= "";
	if( ampforwp_is_front_page() ){
		$post_id = ampforwp_get_frontpage_id();
	}
	if( true == ampforwp_has_post_thumbnail() )	{
		if (has_post_thumbnail( $post_id ) ){
			$thumb_id = get_post_thumbnail_id($post_id);
			$image = wp_get_attachment_image_src( $thumb_id, 'woocommerce_thumbnail' );
			$caption = get_the_post_thumbnail_caption( $post_id ); 
			$thumb_alt = get_post_meta( $thumb_id, '_wp_attachment_image_alt', true);
			if($thumb_alt){
				$alt = $thumb_alt;
			}
			else{
				$alt = get_the_title( $post_id );
			}
			$alt = esc_attr($alt);
			if( $image ){
				$amp_html = "<amp-img src='$image[0]' width='$image[1]' height='$image[2]' layout=responsive alt='$alt' style='width:$image[1]px; height:$image[2]px;'></amp-img>";
			}
		}
		if ( ampforwp_is_custom_field_featured_image() ) {
			$amp_img_src 	= ampforwp_cf_featured_image_src();
			$amp_img_width 	= ampforwp_cf_featured_image_src('width');
			$amp_img_height = ampforwp_cf_featured_image_src('height');
			if( $amp_img_src ){	
				$amp_html = "<amp-img src='$amp_img_src' width=$amp_img_width height=$amp_img_height layout=responsive ></amp-img>";
			}
		}
		if( true == $redux_builder_amp['ampforwp-featured-image-from-content'] && ampforwp_get_featured_image_from_content() ){
			$amp_html = ampforwp_get_featured_image_from_content();
			$amp_html = preg_replace('#sizes="(.*)"#', "layout='responsive'", $amp_html);
		}
		if( $amp_html ){ 
			if($product && $product->is_on_sale()){
				$amp_html = '<div class="onsale-wrap"><div class="onsale-inner"><span class="onsale">'.__( 'Sale!', 'woocommerce' ).'</span></div></div>' . $amp_html;
			} ?>
			<figure class="amp-featured-image"> <?php  
				echo $amp_html;
				 if ( $caption ) : ?>
					<p class="wp-caption-text">
						<?php echo wp_kses_data( $caption ); ?>
					</p>
				<?php endif; ?>
			</figure>
		<?php do_action('ampforwp_after_featured_image_hook');
		}
	}
}

add_action( 'amp_meta', 'vbb_amp_add_custom_meta' );

function vbb_amp_add_custom_meta($template) {
	global $cg_options;
	if ( isset( $cg_options['cg_favicon']['url'] ) ) {
		$cg_options['cg_favicon']['url'] = $protocol . str_replace( array( 'http:', 'https:' ), '', $cg_options['cg_favicon']['url'] );
		$cg_favicon = $cg_options['cg_favicon']['url'];
	} ?>
	<link rel="shortcut icon" href="<?php
		if ( $cg_favicon ) {
			echo $cg_favicon;
		} else {
			echo get_template_directory_uri() . '/favicon.png'; 
		} ?>"/>
<?php 
}

add_filter( 'pre_get_posts', 'vbb_amp_add_products_to_search' );

function vbb_amp_add_products_to_search( $query ) {
	
	if ( $query->is_search ) {
	$query->set( 'post_type', array( 'post', 'page', 'product' ) );
	}
	
	return $query;
	
}

add_filter( 'ampforwp_modify_rel_canonical', function($amp_url){
	if(is_shop()) $amp_url = false;
	return $amp_url;
} );

add_action( 'amp_meta', function(){
	if( is_cart() )
		echo '<meta name="robots" content="noindex, nofollow"/>';
} );

add_action( 'amp_meta', function(){
	if ( is_tax() ) {
		$queried_object = get_queried_object();
		$taxonomy = $queried_object->taxonomy;
		$term_id = $queried_object->term_id;
		$meta   = get_option( 'wpseo_taxonomy_meta' );
		$yoast_wpseo_metakeywords = $meta[$taxonomy][$term_id]['wpseo_metakey'];
	} elseif (is_single() || is_page()) {
		global $post;
		$yoast_wpseo_metakeywords = get_post_meta( $post->ID, '_yoast_wpseo_metakeywords', true );
	}

	if($yoast_wpseo_metakeywords){
		echo '<meta name="keywords" content="' . $yoast_wpseo_metakeywords . '">';
	}

	if( is_cart() )
		echo '<meta name="robots" content="noindex, nofollow"/>';
} );

// убираем некоторы дубли типа /page/2/ 
add_action('wp', 
	function (){
		global $wp_query;
		// preg_match('/^\/amp\/page\/\d+\/?$/i', $_SERVER['REQUEST_URI']);

		$redirect = (ampforwp_is_front_page() && get_query_var( 'paged' )) || (is_front_page() && get_query_var( 'page' ));
		
		if($redirect){
			$wp_query->set_404();
			status_header( 404 );
			get_template_part( 404 );
			exit();
		}
		return false;
	}
);

// Title
function vbb_amp_title(){
	global $redux_builder_amp, $post;
	$ID = '';
	if( ampforwp_is_front_page() && ampforwp_get_frontpage_id() ){
		if( $redux_builder_amp['ampforwp-title-on-front-page'] ) {
			$ID = ampforwp_get_frontpage_id();
		}
	}
	elseif ( ampforwp_polylang_front_page() ) {
		$ID = pll_get_post(get_option('page_on_front'));
	}
	else
		$ID = $post->ID;
	if( $ID!=null ){
		do_action('ampforwp_above_the_title');
		$ampforwp_title = get_the_title($ID);
		$ampforwp_title =  apply_filters('ampforwp_filter_single_title', $ampforwp_title);
		$h_tag = 'h2';
		$posts_without_h1 = array(142138,141765,141935,141810,141702,141907,141997,141703,141751,141696,141995,142165);
		if(in_array($ID, $posts_without_h1)){
			$h_tag = 'h1';
		}
		 ?>
			<?php 
			echo '<'.$h_tag.' class="amp-post-title">';
			echo wp_kses_data( $ampforwp_title );
			echo '</'.$h_tag.'>';
			?>
		<?php do_action('ampforwp_below_the_title'); ?>
<?php
	}
}

function amp_vbb_archive_title(){
	global $redux_builder_amp;
	if( is_author() ){
		$curauth = (get_query_var('author_name')) ? get_user_by('slug', get_query_var('author_name')) : get_userdata(get_query_var('author'));
		if( 0 && true == ampforwp_gravatar_checker($curauth->user_email) ){
			$curauth_url = get_avatar_url( $curauth->user_email, array('size'=>180) );
			if($curauth_url){ ?>
				<div class="amp-wp-content author-img">
					<amp-img src="<?php echo esc_url($curauth_url); ?>" width="90" height="90" layout="responsive"></amp-img>
				</div>
			<?php }
		}
	}
	if ( is_archive() ) {
		$description = $sanitizer = $arch_desc = '';
		add_filter('the_author', function(){return "Daniel Santos";});
	    the_archive_title( '<h1 class="amp-archive-title">', '</h1>' );
	    $description 	= get_the_archive_description();
		$sanitizer = new AMPFORWP_Content( $description, array(),
			apply_filters( 'ampforwp_content_sanitizers',
				array(
					'AMP_Style_Sanitizer' 		=> array(),
					'AMP_Blacklist_Sanitizer' 	=> array(),
					'AMP_Img_Sanitizer' 		=> array(),
					'AMP_Video_Sanitizer' 		=> array(),
					'AMP_Audio_Sanitizer' 		=> array(),
					'AMP_Iframe_Sanitizer' 		=> array(
						'add_placeholder' 		=> true,
					)
				) ) );
		$arch_desc 		= $sanitizer->get_amp_content();
			if( $arch_desc ) {
				if ( get_query_var( 'paged' ) ) {
		        $paged = get_query_var('paged');
		    } elseif ( get_query_var( 'page' ) ) {
		        $paged = get_query_var('page');
		    } else {
		        $paged = 1;
		    }
				if($paged <= '1') {?>
					<div class="amp-archive-desc">
						<?php echo $arch_desc ; ?>
				    </div> <?php
				}
			}
	}
	if( is_category() && 1 == $redux_builder_amp['ampforwp-sub-categories-support'] ){
		$parent_cat_id 	= '';
	    $cat_childs		= array();
	    $parent_cat_id 	= get_queried_object_id();
	 	$cat_childs 	= get_terms( array(
	  						'taxonomy' => get_queried_object()->taxonomy,
	  						'parent'   => $parent_cat_id )
						);
		if( !empty( $cat_childs ) ){
			echo "<div class='amp-sub-archives'><ul>";
			foreach ($cat_childs as $cat_child ) {
				 echo '<li><a href="' . get_term_link( $cat_child ) . '">' . $cat_child->name . '</a></li>';
			}
			echo "</ul></div>";
		}
	}
	if(is_search()){
		$label = 'You searched for:';
		if(function_exists('ampforwp_translation')){
			$label = ampforwp_translation( $redux_builder_amp['amp-translator-search-text'], 'You searched for:');
		}
		echo '<h3 class="amp-loop-label">'.$label . '  ' . get_search_query().'</h3>';
	}
}

add_action( 'template_redirect', function(){
	global $wp_query;
	if( isset($wp_query->query_vars['amp']) && (int)$wp_query->query_vars['amp'] !== 1){
		$wp_query->set_404();
		status_header( 404 );
		get_template_part( 404 );
		exit();
	}
	remove_filter( 'nav_menu_link_attributes', 'ampforwp_nav_menu_link_attributes' );
});

add_filter( 'ampforwp_url_purifier', 'ag_ampforwp_amphtml_archivepage_fix' );
function ag_ampforwp_amphtml_archivepage_fix($url)
{
	$get_permalink_structure = get_option('permalink_structure');
	
	$paged = get_query_var('paged');
	
	if(!empty( $get_permalink_structure ) && is_archive() && get_query_var('paged') == 0){
		
		$endpoint = AMPFORWP_AMP_QUERY_VAR;
		
		if ( !preg_match('/\/amp\/?(\?.*)?$/i', $url, $result) ){
			$url = user_trailingslashit( trailingslashit($url) . $endpoint );
		}
	}
	
	return $url;
}

add_action( 'amp_post_template_head',  'remove_amp_next_prev_link', 9);

function remove_amp_next_prev_link(){
	remove_action( 'amp_post_template_head', 'ampforwp_rel_next_prev' );
}
.ampTabContainer {
    display: flex;
    flex-wrap: wrap;
}

.tabButton[selected] {
    outline: none;
    background: #ca1a1d;
    color: #FFF;
}

.tabButton {
    list-style: none;
    flex-grow: 1;
    text-align: center;
    cursor: pointer;
    margin-bottom: 20px;
    font-size: 16px;
    line-height: 40px;
    position:relative;
}

.tabButton[selected]:after {
    content: '';
    width: 0;
    height: 0;
    border-top: 8px solid transparent;
    border-right: 8px solid transparent;
    border-left: 8px solid transparent;
    position: absolute;
    z-index: 5;
    left: 50%;
    margin-left: -8px;
    bottom: -8px;
    border-top-color: #ca1a1d;
}

.tabContent {
    display: none;
    width: 100%;
    order: 1; /* must be greater than the order of the tab buttons to flex to the next line */
}

.tabButton[selected]+.tabContent {
    display: block;
}

/* For example below (not required) */
.itemCustom {
    border: 1px solid #000;
    height: 280px;
    width: 380px;
    margin: 10px;
    text-align: center;
    padding-top: 140px;
}
.product-tabs {
    padding: 50px 0;
}

amp-selector [option][selected]{outline: none;}
.tick{margin-right: 10px;}

.amp-related-posts ul li{width: 100%;}

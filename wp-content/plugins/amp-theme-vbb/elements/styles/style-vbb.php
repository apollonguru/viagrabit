@font-face {
  font-family: 'icomoon';
  src:  url('<?php echo esc_url(AMP_VBB_PLUGIN_URL .'fonts/icomoon.eot'); ?>');
  src:  url('<?php echo esc_url(AMP_VBB_PLUGIN_URL .'fonts/icomoon.eot'); ?>') format('embedded-opentype'),
    url('<?php echo esc_url(AMP_VBB_PLUGIN_URL .'fonts/icomoon.ttf'); ?>') format('truetype'),
    url('<?php echo esc_url(AMP_VBB_PLUGIN_URL .'fonts/icomoon.woff'); ?>') format('woff'),
    url('<?php echo esc_url(AMP_VBB_PLUGIN_URL .'fonts/icomoon.svg'); ?>') format('svg');
  font-weight: normal;
  font-style: normal;
}


[class^="icon-"], [class*=" icon-"]{ font-family: 'icomoon'; speak: none; font-style: normal; font-weight: normal; font-variant: normal; text-transform: none; line-height: 1;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
}

#sidebar[aria-hidden="false"]+#designthree {
    animation: none;
    -webkit-transform: none;
    transform: none;
    overflow: visible;
}
@font-face {
    font-family: "commercegurus";
    src: url('/wp-content/themes/adrenalin/css/fonts/commercegurus.eot?#iefix') format('embedded-opentype'),
        url('/wp-content/themes/adrenalin/css/fonts/commercegurus.woff') format('woff'),
        url('/wp-content/themes/adrenalin/css/fonts/commercegurus.ttf')  format('truetype'),
        url('/wp-content/themes/adrenalin/css/fonts/commercegurus.svg#commercegurus') format('svg');
}
.amp-logo{width: 77px;}
.amp-logo amp-img{
    width:40px;
    height:40px;
}
#footer p{color: #999;}
.tiny-cart{
    margin-right: 10px;
}
.tiny-cart a{
    color: #111;
    font-size: 13px;
}
.cg-header-cart-icon-wrap {
    position: absolute;
}
.icon.cg-icon-shopping-1 {
    color: #ca1a1d;
    font-size: 24px;
    display: inline-block;
    position: relative;
    top: 2px;
}
.cg-icon-shopping-1:before {
    content: "e";
    font-family: "commercegurus";
    speak: none;
    line-height: 1;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
}
.cg-cart-count {
    background: #eee;
    display: block;
    position: absolute;
    bottom: 8px;
    left: 14px;
    text-align: center;
    -moz-border-radius: 10px;
    -webkit-border-radius: 10px;
    -khtml-border-radius: 10px;
    border-radius: 50%;
    font-size: 11px;
    font-weight: 400;
    line-height: 21px;
    color: #222;
    padding: 0 7px;
}
.cart_subtotal {
    margin-left: 34px;
}
.cart_subtotal .amount {
    font: normal 400 16px "Roboto Condensed";
    color: #333;
    padding-left: 8px;
}
.ap_m_21 .ico-mod .ico-pic,
.ap_m_24 .ico-mod .ico-pic,
.ap_m_33 .ico-mod .ico-pic,
.ap_m_35 .ico-mod .ico-pic,
.ap_m_39 .ico-mod .ico-pic,
.ap_m_71 .ico-mod .ico-pic{
    font-size: 100px;
    border-radius: 70px;
}
.ap_r_13 .ap_m_75 .testi-cont,
.ap_r_13 .ap_m_75 .testi-auth{width: auto;}
.ap_r_13 .testi-mod{
    margin-right:2%;
    width:48%;
}
.ap_r_13 .testi-mod:last-child{margin-right:0;}
.price{
    color: #ca1a1d;
    font-size: 20px;
}
a.add_to_cart_button,
.amp-add-to-cart-link input[type="submit"]{
    width: auto;
    line-height: 40px;
    color:#FFF;
    background: #DF440B;
    display: inline-block;
    padding: 0px 30px;
    margin: 20px 0;
    transition: all 200ms ease-out;
    font-size: 16px;
    border: none;
    text-transform: uppercase;
    cursor: pointer;
}
.amp-add-to-cart-link:after{
    content:' ';
    clear: both;
    display: block;
}
.amp-add-to-cart-link input[name="quantity"]{
    line-height: 36px;
    padding: 0;
    text-align: center;
    width: 40px;
}
.price del{
    font-size: 17px;
    color: #b3abab;
}
.price ins{
    text-decoration:none;
}

.breadcrumb {
    width: 100%;
    margin-top: 10px;
}

.breadcrumb ul,.category-single ul {
    padding: 0;
    margin: 0
}

.breadcrumb ul li {
    display: inline
}

.breadcrumb ul li a,.breadcrumb ul li span {
    font-size: 12px
}

.breadcrumb ul li a::after {
    content: "►";
    display: inline-block;
    font-size: 8px;
    padding: 0 6px 0 7px;
    vertical-align: middle;
    opacity: 0.5;
    position: relative;
    top: -1px
}

.breadcrumb ul li:hover a::after {
    color: #c3c3c3
}

.breadcrumb ul li:last-child a::after {
    display: none
}

#pagination .prev{
    float:left;
    max-width:50%;
}

#pagination .next{
    float:right;
    max-width:50%;
}

<?php // main page ?>
.amp-frontpage .ap_r_4.amppb-fluid .col,
.amp-frontpage .ap_r_13.amppb-fluid .col{max-width:100%;}
.amp-frontpage .ap_r_4 .blu-mod{margin:0;}
.amp-frontpage a.btn-txt{
    box-sizing: border-box;
    width: auto;
}
<?php // end main page ?>


.cg-strip-wrap,
.cg-msg-wrap{display:flex;}
.cg-strip-wrap .row,
.cg-strip-wrap .row .cg-pos,
.cg-msg-wrap .row{margin: auto;}
.cg-msg-wrap a{color:#FFF; text-decoration:underline;}
.cg-msg-wrap{
    background-color: #ca1a1d;
    height: 120px;
}


.amp-featured-image{
    position: relative;
    margin-top:40px;
}

.wistia_responsive_padding.amp-wp-inline-4a7d4a8f8d3d079d9271a4696fce350d{padding:0}

td,th{border: 1px solid #000;}

.loop-img{position: relative;}

.onsale-wrap {
    position: absolute;
    margin: 0px 15px;
    top: -6px;
    left: 7px;
    border: 1px solid #222;
    border-radius: 99px;
    padding: 2px;
    /* background: #fff; */
    /* width: 48px; */
    z-index: 2;
    /* box-sizing: border-box; */
}

.onsale-inner {
    width: 42px;
    height: 42px;
    border-radius: 99px;
    background-color: #222;
    display: table;
    text-align: center;
    /* text-align: center; */
}

.single-post .onsale-inner{
    width: 62px;
    height: 62px;
}

span.onsale {
    display: table-cell;
    vertical-align: middle;
    /* text-align: center; */
    font-size: 12px !important;
    color: #fff !important;
    font-weight: 400 !important;
    text-transform: uppercase;
}

.single-post span.onsale{font-size: 16px;}

.cg-strip-wrap .cg-pos{
    color: white;
    max-width: 100% !important;
}

.text-align-center{text-align: center;}

.wpb_button_a span, a.btn-txt{
    font-size: 15px;
    border-radius: 0px;
    color: #fff!important;
    background: #ca1a1d;
    display: inline-block;
    padding: 10px 20px 10px 20px;
    font-weight: 400;
    box-sizing: initial;
    border: 2px solid #dd6d6f;
    text-transform: uppercase;
    transition: all 0.2s linear !important;
}

.wpb_button_a span:hover, a.btn-txt:hover{border-color: #fff;}

.company-name{
    line-height: 36px;
    color: #d72226;
    font-size: 22px;
}
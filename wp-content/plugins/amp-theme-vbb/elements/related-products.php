<?php
global $post,  $redux_builder_amp;
$show_image = (isset($argsdata['show_image']) ? $argsdata['show_image'] : true);
$string_number_of_related_posts = $redux_builder_amp['ampforwp-number-of-related-posts'];
$int_number_of_related_posts = round(abs(floatval($string_number_of_related_posts)));

$args = array(
	'post_type' => 'product',
	'post__not_in' => array($post->ID),
	'posts_per_page'=> $int_number_of_related_posts,
	'ignore_sticky_posts'=>1,
	'has_password' => false ,
	'post_status'=> 'publish',
	'orderby' => 'rand'
);
$categories = get_the_terms($post->ID, 'product_cat');

if ($categories) {

	$category_ids = array();

	foreach($categories as $individual_category){
		$category_ids[] = $individual_category->term_id;
	}

	$args['tax_query'] = array(
			array(
				'taxonomy' => 'product_cat',
				'terms'    => $category_ids,
			),
	);
}

$my_query = new WP_Query($args);
	if( $my_query->have_posts() ) { ?>
		<div class="amp-related-posts container">
			<h2 class="amp-related-posts-title">Related Products</h2>
			<ul class="clearfix">
				<?php
				while( $my_query->have_posts() ) {
					$my_query->the_post();?>
					<li class="<?php if ( has_post_thumbnail() ) { echo'has_thumbnail'; } else { echo 'no_thumbnail'; } ?>">
						<div class="amp-related-post-wrap">
							<?php
							$related_post_permalink = ampforwp_url_controller( get_permalink() );
							// global $_wp_additional_image_sizes;
							// echo '<pre>$_wp_additional_image_sizes = '.htmlspecialchars(print_r($_wp_additional_image_sizes, true)).'</pre>';
							if ( $show_image ) {
								if ( isset($argsdata['image_size']) && '' != $argsdata['image_size'] ) {
									ampforwp_get_relatedpost_image($argsdata['image_size']);
								}
								else {
									ampforwp_get_relatedpost_image('woocommerce_thumbnail');
								}
							}
							$related_post_permalink = ampforwp_url_controller( get_permalink() );?>
							<div class="related_link">
						        <a href="<?php echo esc_url( $related_post_permalink ); ?>"><?php the_title(); ?></a>

						        <?php /*
						        $show_excerpt = (isset($argsdata['show_excerpt'])? $argsdata['show_excerpt'] : true);
						        if($show_excerpt){
						             if(has_excerpt()){
											$content = get_the_excerpt();
										}else{
											$content = get_the_content();
										}
								?><p><?php
								echo wp_trim_words( strip_shortcodes( $content ) , '15' );
								?></p><?php
							}*/
							?>
							</div>
						</div>
					</li><?php
				}

			} ?>
			</ul>
		</div>
<?php wp_reset_postdata(); ?>
<?php do_action('ampforwp_below_related_post_hook');

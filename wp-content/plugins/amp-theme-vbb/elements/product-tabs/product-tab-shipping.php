<?php if ( $cg_options['returns_tab_title'] && $cg_options['returns_tab_content'] ) {
    ?>
    <div role="tab" class="tabButton" option="c"><?php echo $cg_options['returns_tab_title'] ?></div>
    <div role="tabpanel" class="tabContent">
        <?php $returns_tab_content = do_shortcode( $cg_options['returns_tab_content'] );
        if ( $returns_tab_content ) {
            $sanitizer_classes = array
            (
                'AMP_Form_Sanitizer' => array(),
                'AMP_Comments_Sanitizer' => array(),
                'AMP_Video_Sanitizer' => array(),
                'AMP_Audio_Sanitizer' => array(),
                'AMP_Playbuzz_Sanitizer' => array(),
                'AMPFORWP_Instagram_Embed_Sanitizer' => array(),
                'AMPforWP_Img_Sanitizer' => array(),
                'AMPforWP_Iframe_Sanitizer' => array(),
                'AMP_Style_Sanitizer' => array(),
                'AMP_Tag_And_Attribute_Sanitizer' => array(),
            );
            list( $sanitized_returns_tab_content, $scripts, $styles ) = \AMPforWP\AMPVendor\AMP_Content_Sanitizer::sanitize( $returns_tab_content, $sanitizer_classes, array('content_max_width'=>100) );
            echo $sanitized_returns_tab_content;
        } ?>
    </div>
<?php } ?>

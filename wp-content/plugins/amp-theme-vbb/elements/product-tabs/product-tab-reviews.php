<div role="tab" class="tabButton" option="b">Reviews</div>
<div role="tabpanel" class="tabContent">
    <a href="<?php the_permalink(); ?>">Write a review</a>
    <?php vbb_amp_print_reviews($product->get_id()); ?>
</div>

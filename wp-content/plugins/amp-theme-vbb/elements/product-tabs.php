<div class="amp-wp-content product-tabs">
    <?php global $product, $redux_builder_amp, $cg_options; ?>
    <amp-selector role="tablist" layout="container"class="ampTabContainer">

        <?php $tabs_dir = AMP_VBB_PLUGIN_PATH . 'elements/product-tabs/'; ?>

        <?php include($tabs_dir . 'product-tab-description.php'); ?>

        <?php include($tabs_dir . 'product-tab-additional-information.php'); ?>

        <?php include($tabs_dir . 'product-tab-reviews.php'); ?>

        <?php include($tabs_dir . 'product-tab-shipping.php'); ?>

    </amp-selector>
</div>

<!--Post Content here-->
<div class="amp-wp-content the_content">
	<?php do_action('ampforwp_before_post_content',$this); //Post before Content here
    global $product;

    $content = wpautop( do_shortcode( $product->post->post_excerpt ) );
    if ( $content ) {
        $sanitizer_classes = array
        (
            'AMP_Form_Sanitizer' => array(),
            'AMP_Comments_Sanitizer' => array(),
            'AMP_Video_Sanitizer' => array(),
            'AMP_Audio_Sanitizer' => array(),
            'AMP_Playbuzz_Sanitizer' => array(),
            'AMPFORWP_Instagram_Embed_Sanitizer' => array(),
            'AMPforWP_Img_Sanitizer' => array(),
            'AMPforWP_Iframe_Sanitizer' => array(),
            'AMP_Style_Sanitizer' => array(),
            'AMP_Tag_And_Attribute_Sanitizer' => array(),
        );
        list( $sanitized_content, $scripts, $styles ) = \AMPforWP\AMPVendor\AMP_Content_Sanitizer::sanitize( $content, $sanitizer_classes, array('content_max_width'=>100) );
        echo $sanitized_content;
    }
	do_action('ampforwp_after_post_content',$this) ; //Post After Content here?>
</div>

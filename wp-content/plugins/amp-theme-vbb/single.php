<?php amp_header(); ?>
<?php include AMP_VBB_PLUGIN_PATH . 'elements/bread-crumbs.php'; ?>
<?php amp_featured_image();?>
<?php vbb_amp_title(); ?>
<?php ampforwp_framework_get_author_box( array('author_prefix' => 'Posted on by ') ); ?>
<?php amp_content(); ?>
<?php amp_post_pagination();?>
<?php 
global $redux_builder_amp;
$redux_builder_amp['amp-translator-categories-text'] = 'This entry was posted in';
amp_categories_list();?>
<?php amp_tags_list();?>
<?php amp_comments();?>
<?php vbb_ampforwp_framework_get_post_navigation();?>
<?php //amp_related_posts(); ?>
<?php amp_footer()?>

<?php amp_header(); ?>

<?php include AMP_VBB_PLUGIN_PATH . 'elements/bread-crumbs.php'; ?>

<?php vbb_ampforwp_framework_get_featured_image();?>
<?php amp_title(); ?>
<?php //amp_content(); ?>

<?php include AMP_VBB_PLUGIN_PATH . 'elements/product-content.php'; ?>

<?php include AMP_VBB_PLUGIN_PATH . 'elements/product-tabs.php'; ?>

<?php amp_post_pagination();?>
<?php //amp_author_box(); ?>
<?php //amp_social(array('twitter'));?>
<?php //amp_categories_list();?>
<?php //amp_tags_list();?>
<?php //amp_comments();?>
<?php //amp_post_navigation();?>
<?php //amp_related_posts(); ?>
<?php include AMP_VBB_PLUGIN_PATH . 'elements/related-products.php'; ?>
<?php amp_footer()?>

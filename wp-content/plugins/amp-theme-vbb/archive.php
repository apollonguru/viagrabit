
<?php amp_header(); ?>

<?php include AMP_VBB_PLUGIN_PATH . 'elements/bread-crumbs.php'; ?>

<?php amp_vbb_archive_title(); ?>
<?php amp_loop_template(); ?>

<?php /*

<amp-analytics>
  <script type="application/json">
    {
      "requests": {
        "event": "https://amp-publisher-samples-staging.herokuapp.com/amp-analytics/ping?user=[[.]]&account=ampbyexample&event=${eventId}"
      },
      "triggers": {
        "trackAnchorClicks": {
          "on": "click",
          "selector": ".add_to_cart_button",
          "request": "event",
          "vars": {
            "eventId": "clickOnSpecialAnchor"
          }
        }
      }
    }
  </script>
</amp-analytics>

*/ ?>

<?php do_action( 'vbb_amp_after_loop' ); ?>

<?php amp_footer()?>

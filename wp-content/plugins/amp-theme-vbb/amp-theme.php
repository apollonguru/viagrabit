<?php
/*
Plugin Name: AMP Theme VBB
Description: AMP Theme for VBB (AMP Framework)
Version: 1.0
Author: apollon.guru
Author URI: https://apollon.guru/
License: GPL2
AMP: AMP VBB Theme
*/

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) exit;

define('AMP_VBB_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );

define('AMP_VBB_PLUGIN_URL', plugin_dir_url( __FILE__ ) );

function amp_vbb_plugin_activate() {
	$frontpage_id = get_option( 'page_on_front' );
	// delete_post_meta( $frontpage_id, 'amp-page-builder');
	// delete_post_meta( $frontpage_id, 'ampforwp_page_builder_enable');
	if ($frontpage_id) {
		$amp_page_builder = get_post_meta( $frontpage_id, 'amp-page-builder', true );
		if(!$amp_page_builder){
			$frontpage_amp_content = file_get_contents(AMP_VBB_PLUGIN_PATH . '/elements/main-page-amp-content.json');
			// echo '<pre>$frontpage_amp_content = '.htmlspecialchars(print_r($frontpage_amp_content, true)).'</pre>';
			$frontpage_amp_content = str_replace("'", "", $frontpage_amp_content);
    		$frontpage_amp_content = wp_slash($frontpage_amp_content);
			if($frontpage_amp_content){
				update_post_meta( $frontpage_id, 'amp-page-builder', $frontpage_amp_content );
				update_post_meta( $frontpage_id, 'ampforwp_page_builder_enable', 'yes' );
			}
		}
	}
	// die();
}
register_activation_hook( __FILE__, 'amp_vbb_plugin_activate' );

$redux_builder_amp = get_option( 'redux_builder_amp' );

if( $redux_builder_amp && 
	array_key_exists('amp-design-selector', $redux_builder_amp) && 
	$redux_builder_amp['amp-design-selector'] == 'amp-theme-vbb' 
	)
{

	include AMP_VBB_PLUGIN_PATH . 'functions-custom.php';
	
}
<?php while(amp_loop('start')): ?>
<div class="loop-post">
    <?php global $post; ?>
    <?php vbb_amp_loop_image(array('image_size' => 'medium')); ?>
    <?php amp_loop_category(); ?>
    <?php amp_loop_title(); ?>
    <?php if('product' == $post->post_type){
        woocommerce_template_loop_price();
        echo '<br/><a href="/cart/?add-to-cart='.$post->ID.'" class="button product_type_simple add_to_cart_button">Add to cart</a>';
    } else {
        amp_loop_excerpt();
        amp_loop_date();
    } ?>
</div>
<?php endwhile; amp_loop('end');  ?>
<?php amp_pagination(); ?>

jQuery(document).ready(function($){

    $('#app_key_send').click(function(){

        var val_key = $("input#app_key").val();

        if(val_key != ""){
            $.ajax({
                url: ajaxurl,
                method: 'POST',
                dataType: 'json',
                data: {
                    action: 'ap_api_reg_key',
                    val_key: val_key
                },

                success: function(response){
                    alert('Saved');

                },
                error: function(a,b){
                    alert('Error');
                }
            })
        }

    });


    $('#app_secretkey_send').click(function(){

        var val_key = $("#secretkey").val();

        if(val_key != ""){
            $.ajax({
                url: ajaxurl,
                method: 'POST',
                dataType: 'json',
                data: {
                    action: 'ap_api_reg_secretkey',
                    yp_secretkey: val_key
                },

                success: function(response){
                    alert('Saved');

                },
                error: function(a,b){
                    alert('Error');
                }
            })
        }

    });

    $('#add_update_time').click(function(){

        var cronTime = $("#cron-times_value").val();

        if(cronTime != ""){
            $.ajax({
                url: ajaxurl,
                method: 'POST',
                dataType: 'json',
                data: {
                    action: 'ap_api_reg_time_cron',
                    cron_time: cronTime
                },

                success: function(response){

                    $('#block_yp_cron_time').find('strong').text(cronTime);

                    alert('Saved');

                },
                error: function(a,b){
                    alert('Error');
                }
            })
        }

    });
    $('select#cron-times_value').change(function(){


        $time_val = $(this).val();
        $(this).attr('data-time-cron',$time_val);
        console.log($time_val);

    });
});

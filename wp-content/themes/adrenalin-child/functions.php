<?php
use GeoIp2\Exception\AddressNotFoundException;
use GeoIp2\WebService\Client;

require_once dirname( __FILE__ ) . '/includes/cron_schedules.php';

define( 'THEME_JS_DIR', get_stylesheet_directory_uri() . '/js' );

function debug_log( $message )
{
    error_log( $message . "\n", 3, '/tmp/wp-debug.log' );
}

if ( isset( $_GET['cc'] ) ) {
    add_filter( 'wp_headers', 'set_custom_cache_headers' );
}

function set_custom_cache_headers( $headers )
{
    $headers['Cache-Control'] = 'max-age=0, no-cache, no-store, must-revalidate';
    $headers['Pragma'] = 'no-cache';
    $headers['Expires'] = 'Wed, 11 Jan 1984 05:00:00 GMT';

    return $headers;
}

function validate_tracking_number( $str )
{
    $str = strtolower( $str );
    return ( strrpos( $str, 'em', -strlen( $str ) ) !== false ||
        strrpos( $str, 'ea', -strlen( $str ) ) !== false ||
        strrpos( $str, 'rm', -strlen( $str ) ) !== false ||
        strrpos( $str, 'lt', -strlen( $str ) ) !== false ||
        strrpos( $str, 'rr', -strlen( $str ) ) !== false ||
        strrpos( $str, 'rn', -strlen( $str ) ) !== false ||
        strrpos( $str, 'ls', -strlen( $str ) ) !== false );
}

add_action( 'fue_before_variable_replacements', 'fue_register_variable_replacements', 10, 4 );

function fue_register_variable_replacements( $var, $email_data, $fue_email, $queue_item )
{
    $variables = array(
        'fashionpay_payment_link' => '',
        'order_total_amount' => '',
        'payment_url' => '',
        'crypto_amount' => '',
        'payment_address' => ''
    );

    if ( !empty( $email_data['order_id'] ) ) {
        $order = wc_get_order( $email_data['order_id'] );
        $meta = get_post_meta( $order->get_id() );

        $payment_url = $order->get_checkout_payment_url(true);
        $variables['payment_url'] = '<a href="' . $payment_url . '">here ></a>';
        $variables['crypto_amount'] = CW_Formatting::fbits($meta['crypto_amount'][0]);
        $variables['payment_address'] = $meta['payment_address'][0];
        $variables['order_total_amount'] = $order->get_total();
        $variables['fashionpay_payment_link'] = "<a href='https://www.myshopplatform.com/sslfastpayment?MerNo=12277&Amount={$order->order_total}&ordernumber={$order->get_order_number()}&Currency=1&language=en&tradeUrl=boglassstore.com'>Ref {$order->get_order_number()}</a>";
    }

    $var->register( $variables );
}

function sv_twilio_sms_variable_replacement( $message, $order )
{
    $notes = $order->get_customer_order_notes();
    usort( $notes, function ( $a, $b ) {
        return strcmp( $a->comment_date_gmt, $b->comment_date_gmt );
    } );
    $notes = array_reverse( $notes );

    if ( count( $notes ) > 0 ) {
        $message = str_replace( '%note%', $notes[0]->comment_content, $message );
    } else {
        $message = str_replace( '%note%', '', $message );
    }
    $message = str_replace( '%shipping_first_name%', $order->get_shipping_first_name(), $message );
    return $message;
}

add_filter( 'wc_twilio_sms_customer_sms_before_variable_replace', 'sv_twilio_sms_variable_replacement', 10, 2 );

function getXLS( $xls )
{
    $objPHPExcel = PHPExcel_IOFactory::load( $xls );
    $objPHPExcel->setActiveSheetIndex( 0 );
    $aSheet = $objPHPExcel->getActiveSheet();


    $array = array();

    foreach ( $aSheet->getRowIterator() as $row ) {
        $cellIterator = $row->getCellIterator();

        $item = array();
        foreach ( $cellIterator as $cell ) {
            array_push( $item, $cell->getCalculatedValue() );
        }

        array_push( $array, $item );
    }

    return $array;
}

add_filter( 'addtoany_sharing_disabled', function () {
    if ( 'shopannouncements' == get_post_type() ) {
        return true;
    }
} );

function redirect_by_order_number() {
    if ( ( isset( $_GET['post'] ) && isset( $_GET['action'] ) ) &&  $_GET['action'] == 'edit' ) {
        $order_id = wc_seq_order_number_pro()->find_order_by_order_number( $_GET['post'] );
        if ( $order_id > 0  && $order_id != (int) $_GET['post'] ) {
            wp_redirect("/wp-admin/post.php?post={$order_id}&action=edit");
            exit();
        }
    }
}
add_action( 'admin_init', 'redirect_by_order_number' );

add_filter( 'wpseo_add_x_redirect', false );

function get_client_ip()
{
    static $ip = null;
    if ( $ip !== null ) {
        return $ip;
    }
    if ( isset( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {
        $arr = explode( ',', $_SERVER['HTTP_X_FORWARDED_FOR'] );
        $pos = array_search( 'unknown', $arr );
        if ( false !== $pos ) {
            unset( $arr[$pos] );
        }
        $ip = trim( $arr[0] );
    } elseif ( isset( $_SERVER['HTTP_CLIENT_IP'] ) ) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif ( isset( $_SERVER['REMOTE_ADDR'] ) ) {
        $ip = $_SERVER['REMOTE_ADDR'];
    }

    return $ip;
}

add_action( 'template_redirect', 'apply_customer_data', 33 );

function apply_customer_data() {
    if ( !is_ajax() ) {
        if ( is_page( wc_get_page_id( 'checkout' ) ) ) {
            add_filter( 'woocommerce_form_field_args', function ( $args ) {

                if ( !isset( $_REQUEST['cem'] ) || empty( $_REQUEST['cem'] ) ) {
                    return $args;
                }

                $query_args = array(
                    'post_type' => 'shop_order',
                    'post_status' => 'publish',
                    'posts_per_page' => 1, // or -1 for all
                    'fields' => 'ids',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'shop_order_status',
                            'field' => 'slug',
                            'terms' => array('processing', 'completed')
                        ),
                    ),
                    'meta_query' => array(
                        array(
                            'key' => '_billing_email',
                            'value' => $_REQUEST['cem'],
                        ),
                    ),
                );
                $orders = get_posts( $query_args );
                $order = new WC_Order( $orders[0] );

                WC()->customer->set_postcode( $order->get_billing_postcode() );
                WC()->customer->set_state( $order->get_billing_state() );
                WC()->customer->set_shipping_postcode( $order->get_shipping_postcode() );

                switch ( $args['id'] ) {
                    case 'billing_first_name':
                        $args['default'] = $order->get_billing_first_name();
                        break;
                    case 'billing_last_name':
                        $args['default'] = $order->get_billing_last_name();
                        break;
                    case 'billing_email':
                        $args['default'] = $order->get_billing_email();
                        break;
                    case 'billing_phone':
                        $args['default'] = $order->get_billing_phone();
                        break;
                    case 'billing_address_1':
                        $args['default'] = $order->get_billing_address_1();
                        break;
                    case 'billing_address_2':
                        $args['default'] = $order->get_billing_address_2();
                        break;
                    case 'billing_state':
                        $args['default'] = $order->get_billing_state();
                        break;
                    case 'billing_country':
                        $args['default'] = $order->get_billing_country();
                        break;
                    case 'billing_city':
                        $args['default'] = $order->get_billing_city();
                        break;
                }

                if ( $order->get_shipping_address_1() || $order->get_shipping_address_2() ) {
                    switch ( $args['id'] ) {
                        case 'shipping_first_name':
                            $args['default'] = $order->get_shipping_first_name();
                            break;
                        case 'shipping_last_name':
                            $args['default'] = $order->get_shipping_last_name();
                            break;
                        case 'shipping_address_1':
                            $args['default'] = $order->get_shipping_address_1();
                            break;
                        case 'shipping_address_2':
                            $args['default'] = $order->get_shipping_address_2();
                            break;
                        case 'shipping_state':
                            $args['default'] = $order->get_shipping_state();
                            break;
                        case 'shipping_country':
                            $args['default'] = $order->get_shipping_country();
                            break;
                        case 'shipping_city':
                            $args['default'] = $order->get_shipping_city();
                            break;
                        case 'shipping_postcode':
                            $args['default'] = $order->get_shipping_postcode();
                            break;
                    }
                }

                return $args;
            }, 10, 3 );
        }
    }
}

add_filter( 'woocommerce_default_catalog_orderby', function ( $default ) {
    if ( in_array($_SERVER['REQUEST_URI'], array('/buy-cheap-modalert/', '/generic-stendra-avanafil-online/') ) ) {
        return 'popularity';
    } else {
        return $default;
    }
} );

// Downcase URL hook
add_action( 'wp_loaded', function () {
    if ( !is_admin() ) {
        if (isset($_REQUEST['sfn_trk']) || (isset($_GET['key'])) || (isset($_REQUEST['key']) && isset($_REQUEST['login']))) {
            return;
        }
        if ( stripos( $_SERVER['REQUEST_URI'], '/wc-api' ) !== false ||
            ( stripos( $_SERVER['REQUEST_URI'], '/wp-admin' ) !== false ) ) {
            return;
        }
        if (  preg_match( '/[A-Z]/', $_SERVER['REQUEST_URI'] ) > 0 ) {
            $url = 'https://' . $_SERVER['HTTP_HOST'] . strtolower( $_SERVER['REQUEST_URI'] );
            wp_redirect( $url, 301 );
            exit();
        }
    }
} );

add_filter('paginate_links', function( $link ) {
    if (preg_match('/page\/1\/$/', $link)) {
        $link = str_replace('page/1/', '', $link);
    }
    return $link;
}, 10);

global $products;

$products = array(
    'https://viabestbuys.com' => array(
        array(6, 66786, "Tadalafil 20mg from Sunrise", 10),
        array(1, 67132, "Sildenafil Citrate 100 mg from Sunrise", 10),
        array(4438, 67133, "Vardegra 20 mg", 10)
    ),
    'https://buyedtab.com' => array(
        array(6, 66786, "Tadalafil 20mg from Sunrise", 10),
        array(1, 67132, "Sildenafil Citrate 100 mg from Sunrise", 10),
        array(4438, 141735, "Vardegra 20 mg", 10),
    ),
    'https://sildenafilviagra.com' => array(
        array(6, 66786, "Tadalafil 20mg from Sunrise", 10),
        array(1, 67132, "Sildenafil Citrate 100 mg from Sunrise", 10),
        array(4438, 141726, "Vardegra 20 mg", 10),
    ),
    'https://cialisbit.com' => array(
        array(6, 66786, "Tadalafil 20mg from Sunrise", 10),
        array(1, 67132, "Sildenafil Citrate 100 mg from Sunrise", 10),
        array(4438, 141718, "Vardegra 20 mg", 10),
    ),
    'https://modafinilxl.com' => array(
        array(4111, 141407, 'Artivigil 150mg Armodafinil', 10),
        array(3, 66786, 'Modalert 200mg Tab Modafinil 200mg', 10),
    ),
    'https://prepgeneric.com' => array(
        array(72, 66786, 'Tenvir - EM', 30),
        array(4433, 172355, 'Ricovir EM', 30),
        array(4440, 171998, 'Tavin - EM', 30),
        array(4432, 171999, 'Tenof - EM', 30),
    ),
);

function get_products_for_site( $siteurl ) {
    global $products;
    if ( $siteurl == 'http://localhost:8100' ) {
        $siteurl = 'https://viabestbuys.com';
    }
    return $products[ $siteurl ];
}

function get_xls_site_products( $siteurl ) {
    global $products;
    if ( $siteurl == 'http://localhost:8100' ) {
        $siteurl = 'https://viabestbuys.com';
    }
    $site_products = $products[ $siteurl ];

    if ( $siteurl === 'https://modafinilxl.com') {
        $modafinixl_products = array(
            array(4, 157117, 'Modvigil 200', 10),
            array(25, 141528, 'Waklert 150', 10),
            array(4436, 171282, 'MODAFIL MD 200 TAB', 10),
        );
        $site_products = array_merge($site_products, $modafinixl_products);
    } else if ( $siteurl !== 'https://prepgeneric.com' ) {
        $other_products = array(
            array(4218, 0, 'Malegra 200 mg', 10),
            array(7, 0, 'Tadarise 40mg Tab', 10),
            array(145, 0, 'Sildigra Soft 100mg tab', 10),
        );
        $site_products = array_merge($site_products, $other_products);
    }

    return $site_products;
}

add_filter('post_class', 'fc_remove_hentry', 20);
function fc_remove_hentry($classes) {
    if (($key = array_search('hentry', $classes)) !== false) {
        unset( $classes[$key] );
    }
    return $classes;
}

//Remove All Meta Generators
function remove_meta_generators($html) {
    $patterns = array(
        '/<meta name(.*)=(.*)"generator"(.*)>/i',
        '/<link.*type="(application\/rss\+xml|application\/json\+oembed|text\/xml\+oembed)".*>/i',
        "/<link rel='dns-prefetch' href='\/\/s.w.org'.*>/i",
        "/<link rel='(pingback|profile|wlwmanifest|EditURI|shortlink|https\:\/\/api.w.org\/)'.*>/i",
        '/<link rel="(pingback|profile|wlwmanifest|EditURI|shortlink|https\:\/\/api.w.org\/)".*>/i',
    );
    foreach ($patterns as $pattern) {
        $html = preg_replace($pattern, '', $html);
    }
    return $html;
}
function clean_meta_generators($html) {
    ob_start('remove_meta_generators');
}
add_action('get_header', 'clean_meta_generators', 100);
add_action('before_google_tag_manager', function(){ ob_end_flush(); }, 100);


define('SNT_KEY', '_snt_exported');
define('SNT_PROCESSED', '_snt_processed');
define('XLS_PROCESSED', '_xls_processed');

function get_aftership_site_url() {
    $aftership_sites = array(
        'https://viabestbuys.com'      => 'http://viabestbuy.aftership.com/',
        'https://viabestbuy.com'       => 'http://viabestbuy.aftership.com/',
        'https://viagrabestbuy.com'    => 'http://viabestbuy.aftership.com/',
        'https://modafinilxl.com'      => 'http://modafinilxl.aftership.com/',
        'https://buyedtab.com'        =>  'http://buyedtab.aftership.com/',
        'https://cialisbit.com'        => 'http://cialisbit.aftership.com/',
        'https://sildenafilviagra.com' => 'http://sviagra.aftership.com/',
        'https://prepgeneric.com'      => 'http://prepgeneric.aftership.com',
    );
    return $aftership_sites[get_site_url()];
}

function get_support_site_url() {
    $hostname = parse_url(get_site_url(), PHP_URL_HOST);
    return "https://support.{$hostname}";
}

// Loading order is required
require_once dirname( __FILE__ ) . '/includes/theme.php';
require_once dirname( __FILE__ ) . '/includes/orders_xls.php';
require_once dirname( __FILE__ ) . '/includes/orders_china_xls.php';
require_once dirname( __FILE__ ) . '/includes/nmi_orders_export.php';
require_once dirname( __FILE__ ) . '/includes/update_tracking.php';
require_once dirname( __FILE__ ) . '/includes/snt.php';
require_once dirname( __FILE__ ) . '/includes/norton_shopping.php';
require_once dirname( __FILE__ ) . '/includes/shortcodes.php';
require_once dirname( __FILE__ ) . '/includes/checkout_hooks.php';
require_once dirname( __FILE__ ) . '/includes/woocommerce_hooks.php';
require_once dirname( __FILE__ ) . '/includes/woocommerce_custom_api.php';
//require_once dirname( __FILE__ ) . '/includes/payofix_woocommerce.php';
require_once dirname( __FILE__ ) . '/includes/theme_overload.php';
require_once dirname( __FILE__ ) . '/includes/seo.php';
require_once dirname( __FILE__ ) . '/includes/fue.php';
require_once dirname( __FILE__ ) . '/includes/default_author.php';

if ( get_option('siteurl') == 'https://viabestbuys.com' || get_option('siteurl') == 'http://localhost:8100') {
    require_once dirname( __FILE__ ) . '/includes/pharmacy_reviews.php';
}

if ( defined( 'WP_CLI' ) && WP_CLI ) {
    require dirname( __FILE__ ) . '/includes/yotpo_reviews.php';
    require dirname( __FILE__ ) . '/includes/convert_xls.php';
    require dirname( __FILE__ ) . '/includes/check_mailbox.php';
    require dirname( __FILE__ ) . '/includes/cli_commands.php';
}

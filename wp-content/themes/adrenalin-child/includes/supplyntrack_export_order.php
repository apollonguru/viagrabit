<?php

function track_log($message) {
	error_log(date("j, n, Y") . ' ' . $message . "\n", 3, '/tmp/tracking-debug.log');
}

function get_shipping_address_for_order( $order, $quantity, $shipping_type ) {
	if ( $order->get_shipping_address_1() || $order->get_shipping_address_2() ) {
		$first_name = $order->get_shipping_first_name();
		$last_name = $order->get_shipping_last_name();
		$city = $order->get_shipping_city();
		$address = trim( $order->get_shipping_address_1() . ' ' . $order->get_shipping_address_2() );
		$postcode = $order->get_shipping_postcode();
		$state = $order->get_shipping_state();
		$country = trim( preg_replace( '/\(US\)|\(UK\)/', '', WC()->countries->countries[ $order->get_shipping_country() ] ) );
		$phone = $order->get_billing_phone();
	} else {
		$first_name = $order->get_billing_first_name();
		$last_name = $order->get_billing_last_name();
		$city = $order->get_billing_city();
		$address = trim( $order->get_billing_address_1() . ' ' . $order->get_billing_address_2() );
		$postcode = $order->get_billing_postcode();
		$state = $order->get_billing_state();
		$country = trim( preg_replace( '/\(US\)|\(UK\)/', '', WC()->countries->countries[ $order->get_billing_country() ] ) );
		$phone = $order->get_billing_phone();
	}

	return '<FirstName>' . $first_name . '</FirstName>
			<LastName>' . $last_name . '</LastName>
			<City>' . $city . '</City>
			<Street>' . $address . '</Street>
			<Zip>' . $postcode . '</Zip>
			<Apartment></Apartment>
			<Quantity>' . $quantity . '</Quantity>
			<State>' . $state . '</State>
			<Country>' . $country . '</Country>
			<ShippingType>' . $shipping_type . '</ShippingType>
			<Comments></Comments>
			<PhnNum>' . $phone . '</PhnNum>';
}

function send_orders_to_snt() {
    $lockFile = '/tmp/snt.lock';
    $hasFile = file_exists( $lockFile );
    $lockFp = fopen( $lockFile, 'w' );

    if ( !flock( $lockFp, LOCK_EX | LOCK_NB ) ) {
        die( 'Sorry, one more script is running.' );
    }

    if ( $hasFile ) {
        echo 'The previous running has been completed with an error.';
    }

    register_shutdown_function( function () use ( $lockFp, $lockFile ) {
        flock( $lockFp, LOCK_UN );
        unlink( $lockFile );
    } );

    $supplyntrack = include( 'items_supplyntrack.php' );

    global $products;
    $siteurl = get_option( 'siteurl' );

	if ( $siteurl == 'http://localhost:8100' ) {
		$supplyntrack = $supplyntrack['https://viabestbuy.com'];
		$default_products = $products['https://viabestbuy.com'];
	} else {
		$default_products = $products[$siteurl];
		$supplyntrack = $supplyntrack[$siteurl];
	}

	$token = "YZbZ2ZbKDpA0DmITg59c";
	$client = new SoapClient("http://webservice.supplyntrack.net/SNTWebservice.asmx?WSDL", array('trace' => 1));

	$default_product_qty = 10;

    $args = array(
        'limit' => -1,
        'status' => 'processing',
        'snt_exported' => 'no',
    );

    $orders = wc_get_orders( $args );

    foreach ( $orders as $order ) {
        $orders_xml = '<?xml version="1.0" encoding="utf-8" ?>
				     <Orders xmlns="http://www.supplyntrack.net">';
        $default_included_products = array();

        $order_number = $order->get_order_number();

        if ( $order->get_customer_note() != '' ) {
            track_log('Order #' . $order_number . ' not exported to SupplynTrack: have comment from client' );
            continue;
        }

        $default_items_ids = array_map( function ( $item ) {
            return $item[0];
        }, $default_products );

        $default_products_ids = array_map( function ( $item ) {
            return $item[1];
        }, $default_products );

        $shipping_type = 'Express';

        foreach ( $order->get_items() as $item ) {
            $product_id = $item->get_product_id();

            if ( !array_key_exists( $product_id, $supplyntrack ) ) {
                continue;
            }

            $item_info = $supplyntrack[$product_id];
            $item_id = $item_info[2];
            $quantity = $item_info[1] * (int)$item->get_quantity();

            // Skip default products
            if ( in_array( $product_id, $default_products_ids ) ) {
                continue;
            }

            // Check for item_id in default products and add ten pills to
            if ( in_array( $item_id, $default_items_ids ) ) {
                if ( !in_array( $item_id, array_keys( $default_included_products ) ) ) {
                    $quantity = $quantity + $default_product_qty;
                    $default_included_products[$item_id] = $quantity;
                } else {
                    $default_included_products[$item_id] += $quantity;
                }
                continue;
            }

            $orders_xml .= '
                <Order UserOrderId="' . $order_number . '" Token="' . $token . '" ImportType="1">
                    <ShippingId>VBB</ShippingId>
                    <ItemId>' . $item_id . '</ItemId>'
                        . get_shipping_address_for_order( $order, $quantity, $shipping_type ) .
                        '</Order>';
        }
        
        foreach ( $default_products as $product ) {
            if ( in_array( $product[0], array_keys( $default_included_products ) ) ) {
                $quantity = $default_included_products[$product[0]];
            } else {
                $quantity = 10;
            }

            $orders_xml .= '
            <Order UserOrderId="' . $order_number . '" Token="' . $token . '" ImportType="1">
                <ShippingId>VBB</ShippingId>
                <ItemId>' . $product[0] . '</ItemId>'
                        . get_shipping_address_for_order( $order, $quantity, $shipping_type ) .
                        '</Order>';
        }
        $orders_xml .= '</Orders>';
        track_log( $orders_xml );

        $params = array(
            "OrdersXML" => $orders_xml
        );

        $validation_params = array(
            'xml' => $orders_xml
        );
        $res = $client->ValidateXML( $validation_params )->ValidateXMLResult;

        if ( $res === 'True' ) {
            $result = $client->SaveOrder_XML($params)->SaveOrder_XMLResult->any;

            if ( $result == '<success xmlns=""><message>Your Order has been imported successfully.</message></success>' ) {
                update_post_meta( $order->get_id(), SNT_KEY, 'yes' );
                track_log( $result );
            }
        } else {
            track_log( 'Order #' . $order_number . 'not exported to SupplynTrack: ' . $res );
        }
    }
}

function mark_order_for_snt($order_id) {
	$export_mark = get_post_meta( $order_id, SNT_KEY, true );
	if ( $export_mark == 'yes' ) {
	    return;
    } else {
        update_post_meta( $order_id, SNT_KEY, 'no' );
    }
}

$siteurl_pos = strpos( get_option( 'siteurl' ), 'https' );

if (  $siteurl_pos !== false || !current_user_can( 'manage_woocommerce' ) ) {
	add_action( 'woocommerce_order_status_processing', 'mark_order_for_snt' );
}

if ( defined( 'WP_CLI' ) ) {
    WP_CLI::add_command( 'send-orders-to-snt', 'send_orders_to_snt', array('shortdesc' => 'Update file with emails'));
}

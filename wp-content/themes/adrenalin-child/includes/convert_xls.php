<?php

function convert_xls( $args )
{
    try {
        $objPHPExcel = PHPExcel_IOFactory::load( $args[0] );
        $sheet = $objPHPExcel->setActiveSheetIndex( $args[1] );
        echo $sheet->getTitle() . PHP_EOL;
    } catch (Exception $e) {
        echo $e->getMessage();
    }

    $array = array();
    foreach ( $sheet->getRowIterator() as $row ) {
        $cellIterator = $row->getCellIterator();

        $item = array();
        foreach ( $cellIterator as $cell ) {
            array_push( $item, $cell->getValue() );
        }

        array_push( $array, $item );
    }

    foreach ($array as $item) {
        echo "\n" . (int) $item[1] . " => array(
                ". (int) $item[1] . ",
                ". (int) $item[2] . ",
                ". (int) $item[3] . ",
                '{$item[4]}',
            ),";
    }

}

WP_CLI::add_command( 'convert-xls', 'convert_xls',
    array('shortdesc' => 'Convert xls to php array',)
);

function get_unique() {
    $supplyntrack = include( 'items_supplyntrack.php' );
    $products = $supplyntrack['https://buyedtab.com'];
    $result = array_unique($products);
    var_export($result);
}

WP_CLI::add_command( 'get-unique', 'get_unique',
    array('shortdesc' => '',)
);

function export_completed_orders() {
    $args  = array(
        'post_type'      => 'shop_order',
        'post_status'    => 'publish',
        'posts_per_page' => - 1, // or -1 for all
        'fields' => 'ids',
        'tax_query' => array(
            array(
                'taxonomy' => 'shop_order_status',
                'field'    => 'slug',
                'terms'    => array( 'completed' )
            ),
        ),
    );

    $orders = get_posts( $args );
    $completed_orders = array();
    foreach ($orders as $order) {
        $order = new WC_Order($order);
        $data = array($order->get_formatted_billing_full_name(), $order->billing_email);
        array_push( $completed_orders, $data );
    }

    $fp = fopen( 'clients.csv', 'w' );

    foreach ( $completed_orders as $data ) {
        fputcsv( $fp, array( $data[0], $data[1] ), ';' );
    }
    fclose($fp);
}

WP_CLI::add_command( 'export-completed-orders', 'export_completed_orders',
    array('shortdesc' => 'Export completed orders',)
);

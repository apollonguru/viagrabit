<?php

use Defuse\Crypto\Key;
use Defuse\Crypto\Crypto;

function get_emails_for_orders($args)
{
    @set_time_limit(0);
    @ini_set('memory_limit', '2048M');

    $xlsPath = $args[0];

    $worksheet = PHPExcel_IOFactory::load($xlsPath);
    $worksheet->setActiveSheetIndex(0);
    $sheet = $worksheet->getActiveSheet();

    $column = 'A';
    $lastRow = $sheet->getHighestRow();
    // Second row because first is title
    for ($row = 0; $row <= $lastRow; $row++) {
        $cell = $sheet->getCell($column . $row);
        $order_id = wc_seq_order_number_pro()->find_order_by_order_number($cell->getValue());
        $meta = get_post_meta($order_id);

        if ($meta['_billing_email'][0] != '') {
            $worksheet->getActiveSheet()
                ->setCellValue('B' . $row, $meta['_billing_email'][0]);
        }
    }

    $sheetWriter = PHPExcel_IOFactory::createWriter($worksheet, 'Excel2007');
    $sheetWriter->setPreCalculateFormulas(false);
    $sheetWriter->save($xlsPath);
}

function get_tracking_number()
{
    if (get_option('siteurl') == 'http://localhost:8100') {
        return;
    }
    $supplyntrack = include('items_supplyntrack.php');
    $token = "YZbZ2ZbKDpA0DmITg59c";
    $client = new SoapClient("http://webservice.supplyntrack.net/SNTWebservice.asmx?WSDL");

    $args = array('status' => 'processing', 'limit' => -1, 'return' => 'objects');
    $orders = wc_get_orders($args);

    foreach ($orders as $order) {
        $order_number = $order->get_order_number();
        $products = $supplyntrack[get_option('siteurl')];

        foreach ($order->get_items() as $item) {
            $product_id = $item->get_product_id();
            if (!array_key_exists($product_id, $products)) {
                continue;
            }
            $item_info = $products[$product_id];
            $result = $client->RetrieveTrackingNumber(
                array(
                    "Token" => $token,
                    "OrderId" => $order_number,
                    "ItemId" => $item_info[2],
                    "ShippingId" => "VBB"
                )
            )->RetrieveTrackingNumberResult;

            $tracking_number = $result;

            debug_log("Tracking Number from service" . var_dump($result, true));

            if (validate_tracking_number($tracking_number)) {
                debug_log("Processing new tracking number: " . $tracking_number . " Order: " . $order_number);
                $country = $order->get_billing_country();

                $args = array(
                    'post_id' => $order->get_id(),
                    'status' => 'approve',
                    'type' => 'order_note'
                );

                remove_filter('comments_clauses', array('WC_Comments', 'exclude_order_comments'), 10);
                $notes = get_comments($args);
                add_filter('comments_clauses', array('WC_Comments', 'exclude_order_comments'), 10, 1);

                $notes_content_list = array();

                foreach ($notes as $note) {
                    if (validate_tracking_number($note->comment_content)) {
                        array_push($notes_content_list, $note->comment_content);
                    }
                }

                if (get_post_meta($order->get_id(), '_aftership_tracking_number', true) === '') {
                    // Detect carrier by country
                    if (strpos($country, 'US') !== false) {
                        update_post_meta($order->get_id(), '_aftership_tracking_provider', 'usps');
                    } elseif (strpos($country, 'ES') !== false) {
                        update_post_meta($order->get_id(), '_aftership_tracking_provider', 'spain-correos-es');
                    } else {
                        update_post_meta($order->get_id(), '_aftership_tracking_provider', 'india-post-int');
                    }

                    // Detect carrier by tracking number
                    if (strpos($tracking_number, 'EA') !== false) {
                        update_post_meta($order->get_id(), '_aftership_tracking_provider', 'usps');
                    } elseif (strpos($tracking_number, 'RM') !== false) {
                        update_post_meta($order->get_id(), '_aftership_tracking_provider', 'india-post-int');
                    } elseif (strpos($tracking_number, 'LT') !== false) {
                        update_post_meta($order->get_id(), '_aftership_tracking_provider', 'singapore-post');
                    } elseif (strpos($tracking_number, 'RR') !== false) {
                        update_post_meta($order->get_id(), '_aftership_tracking_provider', 'poczta-polska');
                    } elseif (strpos($tracking_number, 'RN') !== false) {
                        update_post_meta($order->get_id(), '_aftership_tracking_provider', 'royal-mail');
                    }

                    update_post_meta($order->get_id(), '_aftership_tracking_number', $tracking_number);
                    $order->update_status('completed');
                }

                if (array_search($tracking_number, $notes_content_list) === false) {
                    $order->add_order_note($tracking_number, 1);
                }
            } else {
                debug_log("No tracking number for order: " . $order_number . " " . $order->get_billing_country());
            }
        }
    }

}

function uncompleted_orders($args)
{
    @set_time_limit(0);
    @ini_set('memory_limit', '-1');
    $siteurl = get_option('siteurl');
    list($filepath, $postfix) = $args;
    $args = array('limit' => -1, 'return' => 'ids');
    $orders = wc_get_orders($args);

    $order_numbers = array();
    $uncompleted = array();

    if (($handle = fopen($filepath, "r")) !== FALSE) {
        while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {
            if (substr($data[2], -1) == $postfix) {
                $orders_numbers[] = $data[2];
            }
        }
        fclose($handle);
        foreach ($orders as $order_id) {
            $order = new WC_Order($order_id);
            if (in_array($order->get_order_number(), $order_numbers) && !in_array($order->get_status(), array('processing', 'completed'))) {
                echo "{$order->get_order_number()} {$order->get_status()}";
                $uncompleted[] = $order->get_order_number();
            }
        }
    }
    $uncompleted_count = 0;
    foreach ($uncompleted as $order) {
        $uncompleted_count++;
    }
    echo "{$siteurl}: orders not completed {$uncompleted_count}\n";
}

function all_orders_to_csv($args)
{
    @set_time_limit(0);
    @ini_set('memory_limit', '-1');
    $xlsPath = $args[0];
    $queryargs = array('limit' => -1, 'return' => 'ids', 'type' => 'shop_order');
    $orders = wc_get_orders($queryargs);


    $worksheet = new PHPExcel();

    $row = 0;
    foreach ($orders as $order_id) {
        $order = wc_get_order($order_id);

        $worksheet->setActiveSheetIndex(0)
            ->setCellValue('A' . $row, $order->get_billing_email())
            ->setCellValue('B' . $row, $order->get_status())
            ->setCellValue('C' . $row, $order->get_order_number())
            ->setCellValue('D' . $row, $order->get_billing_first_name());
        $row++;
    }
    $sheetWriter = PHPExcel_IOFactory::createWriter($worksheet, 'CSV');
    $sheetWriter->setExcelCompatibility(true);
    $sheetWriter->save($xlsPath);
}


class PayofixCard
{
    public function loadEncryptionKeyFromConfig()
    {
        return Key::loadFromAsciiSafeString(ENC_KEY);
    }

    public function load_card($order_id)
    {
        $key = $this->loadEncryptionKeyFromConfig();
        $ciphertext = get_post_meta($order_id, '_card_data', true);
        try {
            return json_decode(Crypto::decrypt($ciphertext, $key));
        } catch (\Defuse\Crypto\Exception\WrongKeyOrModifiedCiphertextException $ex) {
            return null;
        }
    }
}

function get_payofix_card_numbers()
{
    $orders = wc_get_orders(array('limit' => -1, 'payment_method' => 'payofix'));
    $card = new PayofixCard();
    $data = array();
    foreach ($orders as $order) {
        $card_data = $card->load_card($order->get_id());
        if (!is_null($card_data)) {
            $data[$card_data->card_number] = $order->get_billing_email();
        }
    }
    array_unique($data);
    foreach ($data as $k => $v) {
        WP_CLI::log(substr($k, 0, 6) . '-' . substr($k, -4) . '  ' . $v);
    }
}

function get_emails_for_orders_blacklist() {
    @set_time_limit(0);
    @ini_set('memory_limit', '2048M');
    $order_numbers = array(
        '164905V',
        '165113V',
        '165224V',
        '165325V',
        '165423V',
        '165855V',
        '165856V',
        '166047V',
        '166173V',
        '166795V',
        '166820V',
        '167066V',
        '167324V',
        '167657V',
        '167942V',
        '168584V',
        '169191V',
        '169543V',
        '169784V',
        '169974V',
        '170039V',
        '170363V',
        '170598V',
        '170706V',
        '170795V',
        '170831V',
        '170880V',
        '171092V',
        '171209V',
        '171248V',
        '171347V',
        '171672V',
        '172266V',
        '172602V',
        '172767V',
        '172821V',
        '173282V',
        '173558V',
        '173713V',
        '173918V',
        '174117V',
        '174140V',
        '174229V',
        '175063V',
        '175389V',
        '175417V',
        '175503V',
        '175688V',
        '175856V',
        '176548V',
        '176620V',
        '176768V',
        '176835V',
        '176968V',
        '177064V',
        '177512V',
        '177876V',
        '178466V',
        '178527V',
        '178751V',
        '179080V',
        '179420V',
        '179603V',
        '179799V',
        '179896V',
        '180237V',
        '180481V',
        '180506V',
        '180522V',
        '180541V',
        '180703V',
        '180737V',
        '180785V',
        '180791V',
        '180983V',
        '181040V',
        '181444V',
        '181536V',
        '181599V',
        '181714V',
        '181788V',
        '181845V',
        '181863V',
        '181916V',
        '182156V',
        '182436V',
        '182447V',
        '182645V',
        '182717V',
        '182755V',
        '182786V',
        '182894V',
        '182923V',
        '182963V',
        '183039V',
        '183339V',
        '183570V',
        '183903V',
        '183919V',
        '184137V',
        '184399V',
        '184414V',
        '184661V',
        '184816V',
        '184945V',
        '185044V',
        '185051V',
        '185239V',
        '185399V',
        '185485V',
        '185561V',
        '185710V',
        '185721V',
        '185939V',
        '186127V',
        '186502V',
        '186639V',
        '186699V',
        '187219V',
        '187372V',
        '187543V',
        '188021V',
        '188174V',
        '188719V',
        '188967V',
        '189131V',
        '189326V',
        '189645V',
        '189825V',
        '189910V',
        '190093V',
        '190482V',
        '190698V',
        '190882V',
        '191109V',
        '191217V',
        '191268V',
        '191473V',
        '191484V',
        '191535V',
        '191798V',
        '192056V',
        '192063V',
        '192239V',
        '192824V',
        '192932V',
        '192955V',
        '193000V',
        '193006V',
        '193213V',
        '193305V',
        '193709V',
        '194251V',
        '194453V',
        '194474V',
        '194513V',
        '194589V',
        '194590V',
        '194720V',
        '194883V',
        '195321V',
        '195382V',
        '195473V',
        '195930V',
        '196133V',
        '196438V',
        '196827V',
        '196874V',
        '197111V',
        '197124V',
        '197127V',
        '197233V',
        '197395V',
        '197546V',
        '197611V',
        '197643V',
        '197712V',
        '197860V',
        '197874V',
        '197966V',
        '197994V',
        '197997V',
        '198133V',
        '198240V',
        '198308V',
        '198443V',
        '198491V',
        '198506V',
        '198587V',
        '199230V',
        '199412V',
        '199531V',
        '199775V',
        '199816V',
        '200458V',
        '200565V',
        '201745V',
        '201854V',
        '202083V',
        '202096V',
        '202155V',
        '202189V',
        '202302V',
        '202354V',
        '202445V',
        '202503V',
        '202788V',
        '203122V',
        '203342V',
        '203550V',
        '203721V',
        '204248V',
        '205619V',
        '205676V',
        '206375V',
        '206530V',
        '207406V',
        '207516V',
        '207518V',
        '209196V',
        '209239V',
        '209621V',
        '211253V',
        '211370V',
        '211693V',
        '211783V',
        '211831V',
        '212157V',
        '212932V',
        '213331V',
        '213444V',
        '213494V',
        '213510V',
        '213631V',
        '213704V',
        '213818V',
        '213950V',
        '214409V',
        '214617V',
        '214716V',
        '214909V',
        '215129V',
        '215462V',
        '215676V',
        '215694V',
        '215906V',
        '216354V',
        '216527V',
        '216532V',
        '216594V',
        '217772V',
        '217794V',
        '217842V',
        '217844V',
        '217864V',
        '217865V',
        '218202V',
        '218254V',
        '218548V',
        '219069V',
        '219185V',
        '219595V',
        '219758V',
        '220248V',
        '220724V',
        '220833V',
        '220855V',
        '221030V',
        '221461V',
        '222382V',
        '222467V',
        '222695V',
        '222780V',
        '222978V',
        '223027V',
        '223052V',
        '223168V',
        '223280V',
        '223428V',
        '223547V',
        '223635V',
        '223668V',
        '223901V',
        '224049V',
        '224210V',
        '224424V',
        '224478V',
        '224702V',
        '224726V',
        '224875V'
    );

    foreach ($order_numbers as $n) {
        $order_id = wc_seq_order_number_pro()->find_order_by_order_number($n);
        $meta = get_post_meta($order_id);

        if ($meta['_billing_email'][0] != '') {
            echo $meta['_billing_email'][0] . "\n";
        }
    }
}

function get_email_for_transaction_id() {
    $transaction_ids = include('transactions.php');
    foreach ($transaction_ids as $tid) {
        $orders = wc_get_orders(array('transaction_id' => $tid));
        foreach ($orders as $order) {
            $meta = get_post_meta($order->get_id());

            if ($meta['_billing_email'][0] != '') {
                echo $tid . ' ' . $meta['_billing_email'][0] . "\n";
            }
        }
    }
}

function handle_transaction_id_query_var( $query, $query_vars ) {
    if ( ! empty( $query_vars['transaction_id'] ) ) {
        $query['meta_query'][] = array(
            'key' => '_transaction_id',
            'value' => esc_attr( $query_vars['transaction_id'] ),
        );
    }

    return $query;
}
add_filter( 'woocommerce_order_data_store_cpt_get_orders_query', 'handle_transaction_id_query_var', 10, 2 );

if (defined('WP_CLI')) {
    WP_CLI::add_command('get-emails-for-orders', 'get_emails_for_orders',
        array('shortdesc' => 'Update file with emails',)
    );
    WP_CLI::add_command('get-tracking-number', 'get_tracking_number',
        array('shortdesc' => 'Get tracking numbers from SuplyNTrack service',)
    );
    WP_CLI::add_command('uncompleted-orders', 'uncompleted_orders',
        array('shortdesc' => 'Get uncompleted orders',)
    );
    WP_CLI::add_command('get-all-orders', 'all_orders_to_csv',
        array('shortdesc' => 'Get all orders in xls',)
    );
    WP_CLI::add_command('get-payofix-card-numbers', 'get_payofix_card_numbers',
        array('shortdesc' => '',)
    );
    WP_CLI::add_command('get-blacklist-emails', 'get_emails_for_orders_blacklist',
        array('shortdesc' => '',)
    );
    WP_CLI::add_command('get-emails-for-transactions', 'get_email_for_transaction_id',
        array('shortdesc' => '',)
    );
}

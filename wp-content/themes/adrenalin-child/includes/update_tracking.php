<?php

add_action( 'admin_menu', 'register_update_tracking_woocommerce_menu' );
function register_update_tracking_woocommerce_menu() {
    $hookname = add_submenu_page(
        'woocommerce',
        __('Update tracking', 'woocommerce'),
        __('Update tracking', 'woocommerce'),
        'manage_woocommerce',
        'wc-update-tracking',
        'update_tracking_do_page'
    );
    add_action( $hookname, 'update_tracking_upload_import_file' );
}

function update_tracking_do_page() {
    ?>
    <h1>Upload tracking xls file</h1>
    <form method="POST" enctype="multipart/form-data">
        <?php wp_nonce_field( 'update_tracking', 'update_tracking_nonce' ); ?>
        <input type="file" name="file"/>
        <?php submit_button( _x( 'Upload', 'button', 'upload-tracking' ) ); ?>
    </form>
    <?php
}

function update_tracking_upload_import_file() {
    if ( !empty( $_POST ) && !empty( $_FILES['file'] ) ) {

        $uploaded_file = $_FILES['file'];

        // Check and move file to uploads dir, get file data
        // Will show die with WP errors if necessary (file too large, quota exceeded, etc.)
        $overrides = array('test_form' => false, 'test_type' => false);
        $file_data = wp_handle_upload( $uploaded_file, $overrides );
        if ( isset( $file_data['error'] ) ) {
            wp_die(
                $file_data['error'],
                '',
                array('back_link' => true)
            );
        } else {
            echo "
            <div id='progress'></div>
            <script>
                jQuery(document).ready(function($) {
                    var done = false;
                    var data = {'action': 'process_trackings'};
                    // We can also pass the url value separately from ajaxurl for front end AJAX implementations
                    $('#progress').text('Processing trackings: 50');
                    var request = function () {
                        if ( done != true ) {
                            jQuery.post(ajaxurl, data, function(response) {
                               if ( response === 'done' ) {
                                   done = true;
                               }
                               $('#progress').text('Processing trackings: ' + response);
                               request() 
                            });
                        }
                    }
                    request();
                });
            </script>";
        }
        store_data_to_redis( $file_data['file'] );
    }
}

function store_data_to_redis( $file ) {
    // File exists?
    if ( !file_exists( $file ) ) {
        wp_die(
            __( 'Import file could not be found. Please try again.', 'upload_tracking' ),
            '',
            array('back_link' => true)
        );
    }
    $xlsData = getXLS( $file );
    $order_id_key = array_search( 'Order ID', $xlsData[0] );
    $tracking_number_key = array_search( 'TrackingNumber', $xlsData[0] );
    $country_key = array_search( 'Country', $xlsData[0] );

    $redis = new Redis();
    $connected = $redis->connect( REDIS_MASTER_HOST, 6379 );

    if ( $connected ) {
        foreach ( $xlsData as $row ) {
            $order_number = $row[$order_id_key];
            // Check for first row and skip it
            if ( $order_number === 'Order ID' ) {
                continue;
            }
            $tracking_number = $row[$tracking_number_key];
            if ( $tracking_number === NULL ) {
                continue;
            }
            $country = $row[$country_key];
            if ( strpos( $order_number, '-' ) !== false ) {
                $values = explode( '-', $order_number );
                $order_number = $values[0];
            }

            $order_suffix = $order_number[strlen( $order_number ) - 1];
            if ( in_array( $order_suffix, ['V', 'N', 'M', 'C', 'B'] )  ) {
                $order_id = wc_seq_order_number_pro()->find_order_by_order_number( $order_number );
            } else {
                $order_id = $order_number;
            }
            $order_key = "tr:ordr:$order_suffix:$order_id";
            $redis->sAdd("orders_tracking", $order_key);
            $redis->rPush($order_key, $order_id, $tracking_number, $country);
        }
        unlink( $file );
    }
}

add_action( 'wp_ajax_process_trackings', 'process_trackings' );
function process_trackings() {
    global $wpdb;
    $redis = new Redis();
    $connected = $redis->connect( REDIS_MASTER_HOST, 6379 );
    if ( !$connected ) { return; }
    $counter = 50;
    $tracking_keys = $redis->sPop("orders_tracking", 50);

    if (!empty($tracking_keys)) {
        foreach ( $tracking_keys as $key ) {
            list($order_id, $tracking_number, $country) = $redis->lRange($key, 0 ,-1);

            $order = new WC_Order( $order_id );

            $args = array(
                'post_id' => $order_id,
                'status' => 'approve',
                'type' => 'order_note'
            );

            remove_filter( 'comments_clauses', array('WC_Comments', 'exclude_order_comments'), 10, 1 );
            $notes = get_comments( $args );
            add_filter( 'comments_clauses', array('WC_Comments', 'exclude_order_comments'), 10, 1 );

            $notes_content_list = array();

            foreach ( $notes as $note ) {
                if ( strrpos( $note->comment_content, 'EM', -strlen( $note->comment_content ) ) !== false ||
                    strrpos( $note->comment_content, 'RM', -strlen( $note->comment_content ) ) !== false ) {
                    array_push( $notes_content_list, $note->comment_content );
                }
            }

            if ( get_post_meta( $order->get_id(), '_aftership_tracking_number', true ) === '' ) {
                if ( strpos( $country, 'United States' ) !== false ) {
                    update_post_meta( $order_id, '_aftership_tracking_provider', 'usps' );
                } elseif ( strpos( $country, 'Spain' ) !== false ) {
                    update_post_meta( $order_id, '_aftership_tracking_provider', 'spain-correos-es' );
                } elseif ( strpos( strtolower($tracking_number), 'sg' ) !== false ) {
                    update_post_meta( $order_id, '_aftership_tracking_provider', 'singapore-post' );
                } else {
                    update_post_meta( $order_id, '_aftership_tracking_provider', 'india-post-int' );
                }
                update_post_meta( $order_id, '_aftership_tracking_number', $tracking_number );
                $order->update_status( 'completed' );
            }

            if ( array_search( $tracking_number, $notes_content_list ) === false ) {
                $order->add_order_note( $tracking_number, 1 );
            }
            $redis->del($key);
        }
        $counter += 50;
        echo $counter;
    } else {
        echo 'done';
    }
    wp_die();
}

<?php

class NortonShopping {

	public static function init()
	{
		$self = new self();
		add_action('wp_loaded', array($self, 'on_loaded'));
	}

	public function on_loaded() {
		add_action( 'wp_footer', array( $this, 'norton_shopping' ) );
		add_action( 'woocommerce_thankyou', array( $this, 'wc_thankyou_page_norton' ) );
		add_action( 'woocommerce_after_add_to_cart_button', array( $this, 'wc_after_checkout_button' ));
		add_action( 'woocommerce_proceed_to_checkout', array( $this, 'wc_after_checkout_button' ));
	}

	protected function get_store_number() {
		$store_numbers = array(
			'https://viabestbuys.com' => 953336505,
			'https://modafinilxl.com' => 956018152,
			'https://buyedtab.com' => 956018318,
			'https://cialisbit.com' => 956018484,
			'https://sildenafilviagra.com' => 956028859,
			'https://prepgeneric.com' => 964909610,
		);
		return $store_numbers[get_option('siteurl')];
	}

	public function norton_shopping() {
		echo <<<EOD
<!-- BEGIN: _GUARANTEE Seal -->
<span id="_GUARANTEE_SealSpan" ></span>
<img style="display:none;" src="//nsg.symantec.com/Web/Seal/AltSealInfo.aspx?S=Large&T=M&Elem=ImgTagSeal&HASH=fur%2B8MOLjoy7I5yNHTj7HMRUOcsGKvfkg%2B%2B2HOgzF900MVNiaeI%2FFIF2ceuJgQGeoZXzwXgxnnF5hpfCqE4zYw%3D%3D&CBF=AB&AB=1&DP=fn=;src=ImgTagSeal;grt=true" />
<script type="text/javascript" src="//nsg.symantec.com/Web/Seal/gjs.aspx?SN={$this->get_store_number()}"></script>
<script type="text/javascript" >
if( window._GUARANTEE && _GUARANTEE.Loaded ) {
  _GUARANTEE.Hash = "fur%2B8MOLjoy7I5yNHTj7HMRUOcsGKvfkg%2B%2B2HOgzF900MVNiaeI%2FFIF2ceuJgQGeoZXzwXgxnnF5hpfCqE4zYw%3D%3D";
  _GUARANTEE.WriteSeal( "_GUARANTEE_SealSpan", "GuaranteedSeal" );
}
</script>
<!-- END: _GUARANTEE Seal -->
EOD;
	}

	public function wc_thankyou_page_norton( $order_id ) {
		$order = wc_get_order( $order_id );
		echo <<<EOD
	<!-- BEGIN: NSG_GUARANTEE -->
<span id="_GUARANTEE_GuaranteeSpan"></span>
<script type="text/javascript" src="//nsg.symantec.com/Web/Seal/gjs.aspx?SN={$this->get_store_number()}"></script>
<script type="text/javascript">
if(window._GUARANTEE && _GUARANTEE.Loaded) {
  _GUARANTEE.Hash = "fur%2B8MOLjoy7I5yNHTj7HMRUOcsGKvfkg%2B%2B2HOgzF900MVNiaeI%2FFIF2ceuJgQGeoZXzwXgxnnF5hpfCqE4zYw%3D%3D";
  _GUARANTEE.Guarantee.order = "{$order->get_order_number()}";
  _GUARANTEE.Guarantee.subtotal = "{$order->get_total()}";
  _GUARANTEE.Guarantee.currency  = 'USD';
  _GUARANTEE.WriteGuarantee("JavaScript", "_GUARANTEE_GuaranteeSpan");
}
</script>
<!-- END: NSG_GUARANTEE -->
EOD;
	}

	public function wc_after_checkout_button() {
		echo '<br><br><span id="buySAFE_Kicker" name="buySAFE_Kicker" type="Kicker Guaranteed Border 200x70"></span>';
	}

	public function wc_after_proceed_to_checkout_button() {
		echo "<span id='_GUARANTEE_Kicker' name='_GUARANTEE_Kicker' type='Kicker Custom 2'></span>";
	}
}

NortonShopping::init();

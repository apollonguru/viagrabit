<?php

function track_log($message) {
	error_log(date("j, n, Y") . ' ' . $message . "\n", 3, '/tmp/tracking-debug.log');
	if ( defined( 'WP_CLI' ) ) {
        WP_CLI::log($message);
    }
}

class BaseShippingAddress {
    protected $first_name;
    protected $last_name;
    protected $city;
    protected $address;
    protected $postcode;
    protected $state;
    protected $country;
    protected $phone;
    protected $order;

    public function __construct( $order )
    {
        $this->order = $order;

        if ( $order->get_shipping_address_1() || $order->get_shipping_address_2() ) {
            $this->first_name = $order->get_shipping_first_name();
            $this->last_name = $order->get_shipping_last_name();
            $this->city = $order->get_shipping_city();
            $this->address = trim( $order->get_shipping_address_1() . ' ' . $order->get_shipping_address_2() );
            $this->postcode = $order->get_shipping_postcode();
            $this->state = $order->get_shipping_state();
            $this->country = trim( preg_replace( '/\(US\)|\(UK\)/', '', WC()->countries->countries[$order->get_shipping_country()] ) );
            $this->phone = $order->get_billing_phone();
        } else {
            $this->first_name = $order->get_billing_first_name();
            $this->last_name = $order->get_billing_last_name();
            $this->city = $order->get_billing_city();
            $this->address = trim( $order->get_billing_address_1() . ' ' . $order->get_billing_address_2() );
            $this->postcode = $order->get_billing_postcode();
            $this->state = $order->get_billing_state();
            $this->country = trim( preg_replace( '/\(US\)|\(UK\)/', '', WC()->countries->countries[$order->get_billing_country()] ) );
            $this->phone = $order->get_billing_phone();
        }
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @return mixed
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }
}

class SNTShippingAddress extends BaseShippingAddress {
    function __construct( $order )
    {
        parent::__construct($order);

        if ( $order->get_shipping_address_1() || $order->get_shipping_address_2() ) {
            $this->last_name = $order->get_shipping_last_name() . ' ' . $order->get_shipping_company();
        } else {
            $this->last_name = $order->get_billing_last_name() . ' ' . $order->get_billing_company();
        }
    }
}

function handle_custom_query_vars( $query, $query_vars ) {
    if ( ! empty( $query_vars['snt_exported'] ) ) {
        $query['meta_query'][] = array(
            'key' => SNT_KEY,
            'value' => esc_attr( $query_vars['snt_exported'] ),
        );
    }

    if ( ! empty( $query_vars['xls_processed'] ) ) {
        $query['meta_query'][] = array(
            'key' => XLS_PROCESSED,
            'value' => esc_attr( $query_vars['xls_processed'] ),
        );
    }

    if ( ! empty( $query_vars['snt_processed'] ) ) {
        $query['meta_query'][] = array(
            'key' => SNT_PROCESSED,
            'value' => esc_attr( $query_vars['snt_processed'] ),
        );
    }

    return $query;
}
add_filter( 'woocommerce_order_data_store_cpt_get_orders_query', 'handle_custom_query_vars', 10, 2 );

function generate_xls_for_order_cli( $args ) {
    $order = new WC_Order($args[0]);
    $siteurl = get_site_url();
    generate_xls_for_order( $siteurl, get_xls_site_products( $siteurl ), $order);
}

function sending_order_xls_to_admin( $order, $xls_path) {
    $order_number = $order->get_order_number();
    if ( get_post_meta( $order->get_id(), SNT_KEY, true ) === 'yes' ) {
        $title = 'Auto Order #' . $order_number . ' XLS';
        $subject = $title;
        $header = $title;
        WP_CLI::log($title);
    } else {
        $title = 'New Order #' . $order_number . ' XLS';
        $subject = $title;
        $header = $title;
        WP_CLI::log($title);
    }

    wp_mail(
        'vbestbuy99@gmail.com',
        $subject,
        $header,
        '',
        array($xls_path)
    );
    update_post_meta( $order->get_id(), XLS_PROCESSED, 'yes' );
    unlink( $xls_path );
}

function generate_and_send_xls_for_cli( $args ) {
    try {
        $order_id = wc_seq_order_number_pro()->find_order_by_order_number( $args[0] );
        $order = new WC_Order( $order_id );
        $siteurl = get_site_url();
        $xls_path = generate_xls_for_order( $siteurl, get_xls_site_products( $siteurl ), $order);
        sending_order_xls_to_admin( $order, $xls_path);
    } catch (Exception $e) {
        WP_CLI::log($e->getMessage());
    }
}

/**
 * Generate xls for given order
 * @param $siteurl
 * @param $site_products
 * @param $order
 * @return string xls path to
 * @throws PHPExcel_Exception
 * @throws PHPExcel_Reader_Exception
 * @throws PHPExcel_Writer_Exception
 */
function generate_xls_for_order( $siteurl, $site_products, $order) {
    $order_number = $order->get_order_number();
    WP_CLI::log('Processing order ' . $order_number);
    $worksheet = new PHPExcel();
    $worksheet->setActiveSheetIndex( 0 );
    $worksheet->getActiveSheet()->getStyle( 'A1:R1' )->getFont()->setBold( true );
    $worksheet->getActiveSheet()->getStyle( 'A1:R1' )->getFont()->setColor( new PHPExcel_Style_Color( PHPExcel_Style_Color::COLOR_WHITE ) );
    $worksheet->getActiveSheet()->getStyle( 'A1:R1' )->applyFromArray(
        array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => '90713a')
            )
        )
    );

    $xlsPath = WP_CONTENT_DIR . '/uploads/' . 'order_' . $order_number . '.xls';

    if ( $siteurl == 'https://modafinilxl.com' ) {
        $shipping_method = 'Singapore-Express';
    } else {
        $shipping_method = 'Express';
    }

    $worksheet->getActiveSheet()
        ->setCellValue( 'A1', 'Order ID' )
        ->setCellValue( 'B1', 'Shipment ID' )
        ->setCellValue( 'C1', 'First Name' )
        ->setCellValue( 'D1', 'Last Name' )
        ->setCellValue( 'E1', 'Apartment' )
        ->setCellValue( 'F1', 'Street' )
        ->setCellValue( 'G1', 'City' )
        ->setCellValue( 'H1', 'Zip' )
        ->setCellValue( 'I1', 'State' )
        ->setCellValue( 'J1', 'Country' )
        ->setCellValueExplicit( 'K1', 'PhoneNum', PHPExcel_Cell_DataType::TYPE_STRING )
        ->setCellValue( 'L1', 'ItemId' )
        ->setCellValue( 'M1', 'itemName' )
        ->setCellValue( 'N1', 'Order Date' )
        ->setCellValue( 'O1', 'Shipping Type' )
        ->setCellValue( 'P1', 'Quantity' )
        ->setCellValue( 'Q1', 'Status' )
        ->setCellValue( 'R1', 'TrackingNumber' );
    $row = 1;
    $counter = 0;

    $shipping_address = new SNTShippingAddress( $order );

    foreach ( $site_products as $product ) {
        $row++;
        $quantity = ($siteurl != 'https://prepgeneric.com' ? 10 : 30);
        $worksheet->getActiveSheet()
            ->setCellValue( 'A' . $row, $order_number )
            ->setCellValue( 'B' . $row, 'VBB' )
            ->setCellValue( 'C' . $row, $shipping_address->getFirstName() )
            ->setCellValue( 'D' . $row, $shipping_address->getLastName() )
            ->setCellValue( 'E' . $row, $shipping_address->getAddress() )
            ->setCellValue( 'F' . $row, '' )
            ->setCellValue( 'G' . $row, $shipping_address->getCity() )
            ->setCellValue( 'H' . $row, $shipping_address->getPostcode() )
            ->setCellValue( 'I' . $row, $shipping_address->getState() )
            ->setCellValue( 'J' . $row, $shipping_address->getCountry() )
            ->setCellValueExplicit( 'K' . $row, $shipping_address->getPhone(), PHPExcel_Cell_DataType::TYPE_STRING)
            ->setCellValue( 'L' . $row, $product[0] )
            ->setCellValue( 'M' . $row, $product[2] )
            ->setCellValue( 'N' . $row, '' )
            ->setCellValue( 'O' . $row, $shipping_method )
            ->setCellValue( 'P' . $row, $quantity )
            ->setCellValue( 'Q' . $row, '' )
            ->setCellValue( 'R' . $row, '' );
        $worksheet->getActiveSheet()->getStyle( 'K' . $row )->getNumberFormat()
            ->setFormatCode( PHPExcel_Style_NumberFormat::FORMAT_TEXT );
        $counter++;
    }

    $worksheet->getDefaultStyle()
        ->getAlignment()
        ->setVertical( PHPExcel_Style_Alignment::VERTICAL_CENTER );

    for ( $col = 'A'; $col !== 'S'; $col++ ) {
        $worksheet->getActiveSheet()
            ->getColumnDimension( $col )
            ->setAutoSize( true );
    }

    foreach ( $worksheet->getActiveSheet()->getRowIterator() as $row ) {
        $worksheet->getActiveSheet()->getRowDimension( $row->getRowIndex() )->setRowHeight( 15 );
        if ( $row->getRowIndex() != 1 ) {

        }
    }

    $highestRow = $worksheet->setActiveSheetIndex( 0 )->getHighestRow();
    $worksheet->getActiveSheet()->getStyle( 'L2:L' . $highestRow )->applyFromArray(
        array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'ffff00')
            )
        )
    );

    $sheetWriter = PHPExcel_IOFactory::createWriter( $worksheet, 'Excel5' );
    $sheetWriter->save( $xlsPath );

    return $xlsPath;
}

function send_xls_order_to_admin()
{
    try {
        WP_CLI::log('Start xls sending');
        $lockFile = '/tmp/send_xls.lock';
        $hasFile = file_exists( $lockFile );
        $lockFp = fopen( $lockFile, 'w' );

        if ( !flock( $lockFp, LOCK_EX | LOCK_NB ) ) {
            die( 'Sorry, one more script is running.' );
        }

        if ( $hasFile ) {
            WP_CLI::log('The previous running has been completed with an error.');
        }

        register_shutdown_function( function () use ( $lockFp, $lockFile ) {
            flock( $lockFp, LOCK_UN );
            unlink( $lockFile );
        } );

        $siteurl = get_site_url();

        $site_products = get_xls_site_products( $siteurl );

        $args = array(
            'limit' => -1,
            'status' => 'processing',
            'snt_processed' => 'yes',
            'xls_processed' => 'no',
        );

        $orders = wc_get_orders( $args );

        foreach ($orders as $order) {
            $xls_path = generate_xls_for_order( $siteurl, $site_products, $order );
            sending_order_xls_to_admin($order, $xls_path);
        }
    } catch (Exception $e) {
       WP_CLI::log($e->getMessage());
    }
}

function get_snt_items_for_site($siteurl) {
    $supplyntrack = include( 'items_supplyntrack.php' );
    if ( $siteurl == 'http://localhost:8100' ) {
        $siteurl = 'https://viabestbuys.com';
    }
    return $supplyntrack[$siteurl];
}

function get_shipping_address_for_order( $order, $quantity, $shipping_type ) {
    $shipping_address = new BaseShippingAddress( $order );

	return '<FirstName>' . $shipping_address->getFirstName() . '</FirstName>
			<LastName>' . $shipping_address->getLastName() . '</LastName>
			<City>' . $shipping_address->getCity() . '</City>
			<Street>' . $shipping_address->getAddress() . '</Street>
			<Zip>' . $shipping_address->getPostcode() . '</Zip>
			<Apartment></Apartment>
			<Quantity>' . $quantity . '</Quantity>
			<State>' . $shipping_address->getState() . '</State>
			<Country>' . $shipping_address->getCountry() . '</Country>
			<ShippingType>' . $shipping_type . '</ShippingType>
			<Comments></Comments>
			<PhnNum>' . $shipping_address->getPhone() . '</PhnNum>';
}

function send_orders_to_snt() {
    $lockFile = '/tmp/snt.lock';
    $hasFile = file_exists( $lockFile );
    $lockFp = fopen( $lockFile, 'w' );

    if ( !flock( $lockFp, LOCK_EX | LOCK_NB ) ) {
        die( 'Sorry, one more script is running.' );
    }

    if ( $hasFile ) {
        echo 'The previous running has been completed with an error.';
    }

    register_shutdown_function( function () use ( $lockFp, $lockFile ) {
        flock( $lockFp, LOCK_UN );
        unlink( $lockFile );
    } );

    $siteurl = get_option( 'siteurl' );
    $default_products = get_products_for_site($siteurl);
    $supplyntrack = get_snt_items_for_site($siteurl);
	$default_product_qty = 10;
    $skip_default_product = false;

	if ( $siteurl === 'https://prepgeneric.com' ) {
	    $skip_default_product = true;
    }

	$token = "YZbZ2ZbKDpA0DmITg59c";
	$client = new SoapClient("http://webservice.supplyntrack.net/SNTWebservice.asmx?WSDL", array('trace' => 1));

    $args = array(
        'limit' => -1,
        'status' => 'processing',
        'snt_processed' => 'no',
    );

    $orders = wc_get_orders( $args );
    foreach ( $orders as $order ) {
        update_post_meta( $order->get_id(), SNT_PROCESSED, 'yes' );
        update_post_meta($order->get_id(), XLS_PROCESSED, 'no');

        $orders_xml = '<?xml version="1.0" encoding="utf-8" ?>
				     <Orders xmlns="http://www.supplyntrack.net">';
        $default_included_products = array();

        $default_items_ids = array_map( function ( $item ) {
            return $item[0];
        }, $default_products );

        $default_products_ids = array_map( function ( $item ) {
            return $item[1];
        }, $default_products );

        $order_number = $order->get_order_number();

        // Detect customer note
        $customer_note = $order->get_customer_note();
        if ( $customer_note != '' && !is_no_signature($customer_note)) {
            track_log('Order #' . $order_number . ' not exported to SupplynTrack: have comment from client' );
            continue;
        }

        $shipping_type = determine_shipping_type( $siteurl, $order, $supplyntrack, $default_products, $default_product_qty );

        $skip_export = false;

        foreach ( $order->get_items() as $item ) {
            $product_id = $item->get_product_id();

            if ( !array_key_exists( $product_id, $supplyntrack ) ) {
                track_log("Detected combo pack in #{$order_number}");
                $skip_export = true;
            }

            $item_info = $supplyntrack[$product_id];

            if ( $siteurl == 'https://viabestbuys.com' && $item_info[2] == 4 ) {
                track_log("Detected Modafinil in #{$order_number} on https://viabestbuys.com");
                $skip_export = true;
            }

            // Temporary fix
            if ($siteurl == 'https://modafinilxl.com' && $item_info[2] == 24) {
                $item_id = 3;
            } else {
                $item_id = $item_info[2];
            }

            $quantity = $item_info[1] * (int)$item->get_quantity();

            // Skip default products
            if ( in_array( $product_id, $default_products_ids ) && !$skip_default_product ) {
                continue;
            }

            // Check for item_id in default products and add ten pills to
            if ( in_array( $item_id, $default_items_ids ) && !$skip_default_product ) {
                if ( !in_array( $item_id, array_keys( $default_included_products ) ) ) {
                    $quantity = $quantity + $default_product_qty;
                    $default_included_products[$item_id] = $quantity;
                } else {
                    $default_included_products[$item_id] += $quantity;
                }
                continue;
            }

            $orders_xml .= '
                <Order UserOrderId="' . $order_number . '" Token="' . $token . '" ImportType="1">
                    <ShippingId>VBB</ShippingId>
                    <ItemId>' . $item_id . '</ItemId>'
                        . get_shipping_address_for_order( $order, $quantity, $shipping_type ) .
                '</Order>';
        }

        if ( $skip_export == true ) {
            continue;
        }

        // Fill order items
        if (!$skip_default_product) {
            foreach ( $default_products as $product ) {
                if ( in_array( $product[0], array_keys( $default_included_products ) ) ) {
                    $quantity = $default_included_products[$product[0]];
                } else {
                    $quantity = 10;
                }

                $orders_xml .= '
                <Order UserOrderId="' . $order_number . '" Token="' . $token . '" ImportType="1">
                    <ShippingId>VBB</ShippingId>
                    <ItemId>' . $product[0] . '</ItemId>'
                            . get_shipping_address_for_order( $order, $quantity, $shipping_type ) .
                '</Order>';
            }
        }
        $orders_xml .= '</Orders>';

        track_log( $orders_xml );

        $params = array(
            "OrdersXML" => $orders_xml
        );

        $validation_params = array(
            'xml' => $orders_xml
        );
        $res = $client->ValidateXML( $validation_params )->ValidateXMLResult;

        $res_duplicate = $client->OrderExist(array('OrderId' => $order_number, 'Token' => $token))->OrderExistResult;

        if ( $res === 'True' && $res_duplicate == 'no' ) {
            $result = $client->SaveOrder_XML($params)->SaveOrder_XMLResult->any;

            if ( $result == '<success xmlns=""><message>Your Order has been imported successfully.</message></success>' ) {
                update_post_meta( $order->get_id(), SNT_KEY, 'yes' );
                track_log( $result );
            }
        } else {
            if ($res_duplicate == 'yes') {
                track_log( 'Order #' . $order_number . 'not exported to SupplynTrack: ' . $res_duplicate );
            } else {
                track_log( 'Order #' . $order_number . 'not exported to SupplynTrack: ' . $res );
            }
        }
    }
}

function order_exist_cli( $args ) {
    $token = "YZbZ2ZbKDpA0DmITg59c";
    $client = new SoapClient("http://webservice.supplyntrack.net/SNTWebservice.asmx?WSDL", array('trace' => 1));
    $res = $client->OrderExist(array('OrderId' => $args[0], 'Token' => $token))->OrderExistResult;
    WP_CLI::log($res);
}

function determine_shipping_type( $siteurl, $order, $supplyntrack, $default_products, $default_product_qty ) {
    $global_quantity = 0;
    $default_included_products = array();

    $default_items_ids = array_map( function ( $item ) {
        return $item[0];
    }, $default_products );

    $default_products_ids = array_map( function ( $item ) {
        return $item[1];
    }, $default_products );

    foreach ( $order->get_items() as $item ) {
        $product_id = $item->get_product_id();
        // Skip default products
        if ( in_array( $product_id, $default_products_ids ) ) {
            continue;
        }

        $item_info = $supplyntrack[$product_id];
        // Temporary fix
        if ($siteurl == 'https://modafinilxl.com' && $item_info[2] == 24) {
            $item_id = 3;
        } else {
            $item_id = $item_info[2];
        }

        if ( $item_info[2] == 4 ) {
            track_log("Detected Modafinil in #{$order->get_id()} on {$siteurl}");
            return 'Singapore-Express';
        }

        $quantity = $item_info[1] * (int)$item->get_quantity();
        // Check for item_id in default products and add ten pills to
        if ( in_array( $item_id, $default_items_ids ) ) {
            if ( !in_array( $item_id, array_keys( $default_included_products ) ) ) {
                $quantity = $quantity + $default_product_qty;
                $default_included_products[$item_id] = $quantity;
            } else {
                $default_included_products[$item_id] += $quantity;
            }
            continue;
        }
        $global_quantity += $quantity;
    }

    if ( $order->get_shipping_country() == 'US' || $order->get_billing_country() == 'US' ) {
        if (is_no_signature($order->get_customer_note())) {
            return 'Registered';
        }
    }

    foreach ( $default_products as $product ) {
        if ( in_array( $product[0], array_keys( $default_included_products ) ) ) {
            $quantity = $default_included_products[$product[0]];
        } else {
            $quantity = 10;
        }
        $global_quantity += $quantity;
    }

    if ($siteurl == 'https://modafinilxl.com') {
        return 'Singapore-Express';
    }

    if ($global_quantity > 330) {
        return 'Registered';
    } else {
        return 'Express';
    }
}

function is_no_signature( $note ) : bool {
    if (strlen($note) <= 22) {
        return preg_match('/no signature/i', strtolower($note), $value);
    } else {
        return false;
    }
}

function mark_order_for_snt($order_id) {
	$export_mark = get_post_meta( $order_id, SNT_KEY, true );
    $processed_mark = get_post_meta( $order_id, SNT_PROCESSED, true );
	if ( $export_mark != 'yes' ) {
        update_post_meta( $order_id, SNT_KEY, 'no' );
    }
    if ($processed_mark != 'yes') {
        update_post_meta( $order_id, SNT_PROCESSED, 'no' );
    }
}

add_action( 'woocommerce_order_status_processing', 'mark_order_for_snt' );

function get_shipping_type_for_order( $args ) {
    $siteurl = get_site_url();
    $order = new WC_Order($args[0]);
    $default_products = get_products_for_site($siteurl);
    $supplyntrack = get_snt_items_for_site($siteurl);
    $default_product_qty = 10;
    $shipping_type = determine_shipping_type( $siteurl, $order, $supplyntrack, $default_products, $default_product_qty );
    WP_CLI::log($shipping_type);
}

if ( defined( 'WP_CLI' ) ) {
    WP_CLI::add_command( 'shipping-type-for', 'get_shipping_type_for_order', array('shortdesc' => 'Get shipping type for order (TEST ONLY)') );
    WP_CLI::add_command( 'send-orders-to-snt', 'send_orders_to_snt', array('shortdesc' => 'Sent orders information to SNT') );
    WP_CLI::add_command( 'send-xls-order-to-admin', 'send_xls_order_to_admin', array('shortdesc' => 'Sent order in xls format to admin'));
    WP_CLI::add_command( 'generate-xls-for-order', 'generate_xls_for_order_cli', array('shortdesc' => 'Sent order in xls format to admin'));
    WP_CLI::add_command( 'generate-and-send-xls', 'generate_and_send_xls_for_cli', array('shortdesc' => 'Generate and send XLS file for given order number'));
    WP_CLI::add_command( 'api-test', 'order_exist_cli', array('shortdesc' => ''));
}

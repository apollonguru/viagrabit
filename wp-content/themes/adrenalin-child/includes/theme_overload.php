<?php
if ( !function_exists( 'cg_content_nav' ) ) {

    /**
     * Display navigation to next/previous pages when applicable
     */
    function cg_content_nav( $nav_id ) {
        global $wp_query, $post;

        // Don't print empty markup on single pages if there's nowhere to navigate.
        if ( is_single() ) {
            $previous = ( is_attachment() ) ? get_post( $post->post_parent ) : get_adjacent_post( false, '', true );
            $next = get_adjacent_post( false, '', false );

            if ( !$next && !$previous )
                return;
        }

        // Don't print empty markup in archives if there's only one page.
        if ( $wp_query->max_num_pages < 2 && ( is_home() || is_archive() || is_search() ) )
            return;

        $nav_class = ( is_single() ) ? 'post-navigation' : 'paging-navigation';
        ?>
        <nav role="navigation" id="<?php echo esc_attr( $nav_id ); ?>" class="<?php echo $nav_class; ?>">
            <?php if ( is_single() ) : // navigation links for single posts  ?>

                <?php previous_post_link( '<div class="nav-previous">%link</div>', '<span class="meta-nav">' . _x( '&larr;', 'Previous post link', 'commercegurus' ) . '</span> %title' ); ?>
                <?php next_post_link( '<div class="nav-next">%link</div>', '%title <span class="meta-nav">' . _x( '&rarr;', 'Next post link', 'commercegurus' ) . '</span>' ); ?>

            <?php elseif ( $wp_query->max_num_pages > 1 && ( is_home() || is_archive() || is_search() ) ) : // navigation links for home, archive, and search pages  ?>

                <?php if ( get_next_posts_link() ) : ?>
                    <div class="nav-previous"><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Older posts', 'commercegurus' ) ); ?></div>
                <?php endif; ?>

                <?php if ( get_previous_posts_link() ) : ?>
                    <div class="nav-next"><?php previous_posts_link( __( 'Newer posts <span class="meta-nav">&rarr;</span>', 'commercegurus' ) ); ?></div>
                <?php endif; ?>

            <?php endif; ?>

        </nav><!-- #<?php echo esc_html( $nav_id ); ?> -->
        <?php
    }
} // cg_content_nav


// [cg_promo]

function overwrite_shortcodes() {
    function cg_overload_hp_promo($params = array(), $content = null) {
        extract(shortcode_atts(array(
            'bg' => '#f6653c',
            'height' => '',
            'title' => 'This is a promo',
            'button_text' => '',
            'button_url' => ''
        ), $params));

        $bgcolor = "";
        if (strpos($bg,'#') !== false) {
            $bgcolor = 'background-color:'.$bg.'!important; ';
        }

        $height_style = "";
        if ($height) {
            $height_style = 'height:'.$height;
        }

        $content = do_shortcode($content);

        $button_content = "";
        if (isset($button_url, $button_text)) {
            $button_content = ' <a href="'.$button_url.'" class="see-through">'.$button_text.'</a>';
        } else {
            $button_content = '';
        }

        $hppromo = '   
        <div class="cg-msg-wrap highlight-block fade-in animate" style="'.$bgcolor . $height_style .'">
          <div class="cg-msg-bg"></div>
            <div class="row">
              <div class="cg-msg-text col-lg-12 col-md-12 animate" data-animate="slideInRight">
                <div class="cg-msg-heading">'.$title . $button_content . '</div>
              </div>
            </div>
        </div>
      ';
        return $hppromo;
    }
    remove_shortcode("cg_promo");
    add_shortcode("cg_promo", "cg_overload_hp_promo");
}
add_action('wp_loaded', 'overwrite_shortcodes');

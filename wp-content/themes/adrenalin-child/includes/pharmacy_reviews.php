<?php

add_action( 'init', 'create_post_type' );
function create_post_type() {
    register_post_type( 'pharmacy_reviews',
        array(
            'labels' => array(
                'name' => __( 'Pharmacy reviews' ),
                'singular_name' => __( 'Pharmacy review' )
            ),
            'public' => false,
            'has_archive' => false,
            'rewrite' => array('slug' => 'pharmacy-reviews'),
            'supports' => array(
                'comments',
                'title',
                'editor',
                'thumbnail',
            )
        )
    );
}

function pharmacy_reviews_pre_get_posts( $query ) {
    if ( $query->is_main_query() ) {
        add_filter( 'the_content', 'pharmacy_reviews_remove_from_content', 100 );
    }
}

add_action( 'pre_get_posts', 'pharmacy_reviews_pre_get_posts' );

add_filter( 'body_class', function ( $classes ) {
    if ( in_array( 'page-template-page-right-sidebar-pharmacy-reviews', $classes ) ) {
        $classes[] = 'woocommerce-page';
        $classes[] = 'archive';
    }
    return $classes;
} );


function pharmacy_reviews_remove_from_content( $content ) {
    if ( get_post_type() == "pharmacy_reviews" ) {
        libxml_use_internal_errors( true );
        $doc = new DOMDocument();
        $doc->loadHTML( mb_convert_encoding( "<div>$content</div>", 'HTML-ENTITIES', "UTF-8" ), LIBXML_HTML_NODEFDTD | LIBXML_HTML_NOIMPLIED );
        $xpath = new DOMXPath( $doc );
        $bookmarkNode = $xpath->query( '//div[contains(@class, "addtoany_content_top")]' )->item( 0 );
        if ( !is_null( $bookmarkNode ) ) {
            $bookmarkNode->parentNode->removeChild( $bookmarkNode );
        }
        return $doc->saveHTML();
    } else {
        return $content;
    }
}


function pharmacy_reviews_list_shortcode() {
    $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
    ob_start();
    ?>
    <?php $loop = new WP_Query( array('post_type' => 'pharmacy_reviews', 'posts_per_page' => 10, 'paged' => $paged) ); ?>
    <div class="product-wrap">
        <div class="row">
            <ul class="products col-xs-product-2 col-sm-product-3 col-md-product-3 col-lg-product-4 list-layout">
                <?php while ( $loop->have_posts() ) : $loop->the_post() ?>
                    <?php get_template_part( 'pharmacy-review/content', 'pharmacy-review' ); ?>
                <?php endwhile  ?>
            </ul>
        </div>
        <nav class="woocommerce-pagination clearfix">
            <?php
            echo paginate_links( array(
                'base'         => esc_url( str_replace( 999999999, '%#%', remove_query_arg( 'add-to-cart', htmlspecialchars_decode( get_pagenum_link( 999999999 ) ) ) ) ),
                'format'       => '',
                'current'      => max( 1, get_query_var( 'paged' ) ),
                'prev_text' => '<span class="icon-arrow-left"></span>',
                'next_text' => '<span class="icon-arrow-right"></span>',
                'total' => $loop->max_num_pages,
                'type'         => 'list',
                'end_size'     => 3,
                'mid_size'     => 3
            ) );
            ?>
        </nav>
    </div>

    <?php $loop->reset_postdata() ?>

    <?php
    
    return ob_get_clean();
}

add_shortcode( 'pharmacy_reviews_list', 'pharmacy_reviews_list_shortcode' );


function pharmacy_review_set_featured_image() {
    $loop = new WP_Query( array('post_type' => 'pharmacy_reviews', 'posts_per_page' => -1) );
    while ( $loop->have_posts() ) {
        $loop->the_post();
        $attachments = get_children(
            array(
                'post_parent' => get_the_ID(),
                'post_type' => 'attachment',
                'post_mime_type' => 'image'
            )
        );
        foreach ( $attachments as $attachment_id => $attachment ) {
            set_post_thumbnail( get_the_ID(), $attachment_id );
        }
    }
    $loop->reset_postdata();
}


add_filter("wp_postratings_schema_itemtype", function() {
   return '';
});


function revisions_to_keep( $num, $post ) {

    if( 'pharmacy_reviews' == $post->post_type ) {
        $num = 5;
    }
    return $num;
}
add_filter( 'wp_revisions_to_keep', 'revisions_to_keep', 10, 2 );

if ( defined( 'WP_CLI' ) ) {
    WP_CLI::add_command( 'pharmacy-review-set-featured-image', 'pharmacy_review_set_featured_image',
        array('shortdesc' => 'Set featured image for pharmacy review from post content',)
    );
}
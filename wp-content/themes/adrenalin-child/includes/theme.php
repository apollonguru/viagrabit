<?php
add_action( 'wp_enqueue_scripts', 'child_theme_enqueue_styles' );
function child_theme_enqueue_styles() {

    $parent_style = 'adrenalin'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'adrenalin-child',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
}

function load_updated_styles()
{
    wp_dequeue_style( 'cg-font-awesome' );
    wp_dequeue_style( 'yith-wcwl-font-awesome' );
    wp_dequeue_style( 'cg-bootstrap' );

    wp_enqueue_style( 'cg-bootstrap', '//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.0/css/bootstrap.min.css' );
    wp_enqueue_style( 'cg-font-awesome', '//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css', array(), '4.5.0' );
}

add_action( 'wp_enqueue_scripts', 'load_updated_styles' );

function unload_frontpage()
{
    if ( is_page( get_option( 'page_on_front' ) ) === true ) {
//		print_r(wp_scripts());
        wp_deregister_script( 'contact-form-7' );
        wp_deregister_script( 'jquery-ui' );
        wp_deregister_script( 'wcj-datepicker' );
        wp_deregister_script( 'wcj-weekpicker' );
        wp_deregister_script( 'wcj-timepicker' );
        wp_deregister_script( 'wcj-timepicker' );

        wp_deregister_style( 'wcj-datepicker' );
        wp_deregister_style( 'wcj-weekpicker' );

        wp_deregister_style( 'jcrop' );

        wp_dequeue_script( 'jquery-lib' );
    }
}

add_action( 'wp_print_scripts', 'unload_frontpage', 100 );

function remove_head_scripts() {
    remove_action('wp_head', 'wp_print_scripts');
    remove_action('wp_head', 'wp_print_head_scripts', 9);
    remove_action('wp_head', 'wp_enqueue_scripts', 1);

    add_action('wp_footer', 'wp_print_scripts', 5);
    add_action('wp_footer', 'wp_enqueue_scripts', 5);
    add_action('wp_footer', 'wp_print_head_scripts', 5);
}
add_action( 'wp_enqueue_scripts', 'remove_head_scripts' );

add_action( 'wp_enqueue_scripts', 'theme_script_register' );
function theme_script_register()
{
    wp_enqueue_script( 'scrolling.js', get_stylesheet_directory_uri() . '/js/scrolling.js', array(), '1.0.3', true );
}

add_action( 'template_redirect', 'add_cleave_to_checkout' );

function add_cleave_to_checkout() {
    if ( is_checkout() || is_cart()) {
        wp_enqueue_script( 'cleave', get_stylesheet_directory_uri() . '/js/cleave.min.js', array(), '1.0.0', true );
        wp_enqueue_script( 'cleave-phone', get_stylesheet_directory_uri() . '/js/cleave-phone.us.js', array(), '1.0.0', true );
//        wp_enqueue_script( 'cleave-init', get_stylesheet_directory_uri() . '/js/cleave.init.js', array(), '1.0.3', true );
    }
}

function footer_js()
{
    if ( is_page( get_option( 'page_on_front' ) ) === false ) {
        echo <<<EOD
<!-- Start of vbest Zendesk Widget script -->
<script>/*<![CDATA[*/window.zEmbed||function(e,t){var n,o,d,i,s,a=[],r=document.createElement("iframe");window.zEmbed=function(){a.push(arguments)},window.zE=window.zE||window.zEmbed,r.src="javascript:false",r.title="",r.role="presentation",(r.frameElement||r).style.cssText="display: none",d=document.getElementsByTagName("script"),d=d[d.length-1],d.parentNode.insertBefore(r,d),i=r.contentWindow,s=i.document;try{o=s}catch(c){n=document.domain,r.src='javascript:var d=document.open();d.domain="'+n+'";void(0);',o=s}o.open()._l=function(){var o=this.createElement("script");n&&(this.domain=n),o.id="js-iframe-async",o.src=e,this.t=+new Date,this.zendeskHost=t,this.zEQueue=a,this.body.appendChild(o)},o.write('<body onload="document._l();">'),o.close()}("https://assets.zendesk.com/embeddable_framework/main.js","vbest.zendesk.com");
/*]]>*/</script>
<!-- End of vbest Zendesk Widget script -->
EOD;
    }
}

add_action( 'wp_footer', 'footer_js' );

function my_deregister_scripts(){
    wp_dequeue_script( 'wp-embed' );
}
add_action( 'wp_footer', 'my_deregister_scripts' );

add_filter('woocommerce_thankyou_order_received_text', function() {
    return 'This is NOT your payment receipt. A confirmation email will be sent to you once your payment is processed.';
});

<?php
add_filter( 'woocommerce_available_payment_gateways', 'filter_gateways', 1 );

function remove_gateways( $gateways, $gateways_to_remove ) {
    foreach ( $gateways_to_remove as $g ) {
        unset( $gateways[$g] );
    }
    return $gateways;
}

function filter_gateways( $gateways ) {
    if ( get_option( 'siteurl' ) != 'http://localhost:8100' ) {
        global $woocommerce;
        $redis = new Redis();
        $redis->connect( REDIS_HOST, 6379 );
        $completed_order_emails = $redis->hVals( 'completed_order_emails' );
        $args = array();
        if (isset($_POST['post_data'])) {
            parse_str( $_POST['post_data'], $args );
        }

        $gateways_to_remove = array(
            'shoptocp',
            'vigate',
            'nmi',
            'nmi2',
            'paypal_manual',
            'greengate',
        );

        $blacklist_emails = include('blacklist.php');

        if ( isset($args['billing_email']) ) {
            if ( ( $args['billing_email'] == '' || !in_array( $args['billing_email'], $completed_order_emails ) ) && !isset( $_POST['billing_email'] ) ) {
                $gateways = remove_gateways( $gateways, $gateways_to_remove );
                if ( $woocommerce->cart->cart_contents_total > 0 ) {
                    unset( $gateways['gspay'] );
                }
            }
        }

        if ( ( !empty( $_POST['billing_email'] ) && !in_array( $_POST['billing_email'], $completed_order_emails ) ) && !isset( $_POST['post_data'] ) ) {
            $gateways = remove_gateways( $gateways, $gateways_to_remove );
            if ( $woocommerce->cart->cart_contents_total > 0 ) {
                unset( $gateways['gspay'] );
            }
        }

        $billing_email = get_billing_email();
        if ( in_array( $billing_email, $blacklist_emails) ) {
            $gateways = remove_gateways( $gateways, array('payofix', 'awepay') );
        }
    }

    return $gateways;
}

function get_billing_email() {
    $args = array();
    if (isset($_POST['post_data'])) {
        parse_str( $_POST['post_data'], $args );
        if (isset($args['billing_email']) && $args['billing_email'] != '') {
            return $args['billing_email'];
        } else if (!empty( $_POST['billing_email'] )) {
            return $_POST['billing_email'];
        }
    }
}

function add_gspay_for_free_orders( $gateways ) {
    global $woocommerce;

    if ( get_option( 'siteurl' ) != 'http://localhost:8100' ) {
        if ( $woocommerce->cart->cart_contents_total == 0 ) {
            if ( $woocommerce->cart->shipping_total == 29 || $woocommerce->cart->shipping_total == 39 ) {
                $gateways['cryptowoo'];
            }
        }
    }

    return $gateways;
}

function checkout_hook() {
    if ( !is_page( wc_get_page_id( 'checkout' ) ) ) return;
    wp_enqueue_script( 'checkout-hooks', THEME_JS_DIR . '/checkout-hooks.js', $in_footer = true );
}

add_action( 'wp_footer', 'checkout_hook' );

function remove_yotpo() {
    if ( ( stripos( $_SERVER['REQUEST_URI'], '/checkout' ) !== false ) ||
         ( stripos( $_SERVER['REQUEST_URI'], '/cart' ) !== false ) )  {
        wp_dequeue_script('yquery');
    }  else {
        return;
    }
}
add_action( 'wp_enqueue_scripts', 'remove_yotpo' );

function update_completed_order_emails_cache() {
    $lockFile = '/tmp/emails_cache.lock';
    $hasFile = file_exists( $lockFile );
    $lockFp = fopen( $lockFile, 'w' );

    if ( !flock( $lockFp, LOCK_EX | LOCK_NB ) ) {
        die( 'Sorry, one more script is running.' );
    }

    if ( $hasFile ) {
        echo 'The previous running has been completed with an error.';
    }

    register_shutdown_function( function () use ( $lockFp, $lockFile ) {
        flock( $lockFp, LOCK_UN );
        unlink( $lockFile );
    } );

	$redis = new Redis();
	$connected = $redis->connect( REDIS_HOST, 6379 );

    if ( $connected === true ) {
        // Custom added client for payment gateway testing
        $redis->hSet( 'completed_order_emails', 'id_000', 'chris@awepay.com' );

        $args = array('status' => 'completed', 'limit' => -1, 'return' => 'ids');
        $orders = wc_get_orders( $args );
        $count = 0;
        foreach ( $orders as $order_id )  {
            $exists = $redis->hExists('completed_order_emails', "id_$order_id");
            if ($exists == 0) {
                $email = get_post_meta($order_id, '_billing_email', true);
                if ( $email != '') {
                    $redis->hSet( 'completed_order_emails', "id_$order_id", $email );
                    $count++;
                }
            }
        }
        WP_CLI::log( "Completed orders emails synced - $count." );
    }
}

function update_completed_order_w2payvoucher_emails_cache() {
    $args = array(
        'post_type' => 'shop_order',
        'post_status' => 'publish',
        'posts_per_page' => -1, // or -1 for all
        'fields' => 'ids',
        'tax_query' => array(
            array(
                'taxonomy' => 'shop_order_status',
                'field' => 'slug',
                'terms' => array('processing', 'completed')
            ),
        ),
        'meta_query' => array(
            array(
                'key' => '_payment_method',
                'value' => 'w2payvoucher',
            ),
        ),
    );

    $orders = get_posts( $args );

    $completed_order_emails = array_map( function ( $order ) {
        $order = new WC_Order( $order );
        if ( $order->billing_email != '' ) {
            return $order->billing_email;
        }
    }, $orders );
    $redis = new Redis();
    $redis->connect( REDIS_HOST, 6379 );

    $redis->set( 'w2payvoucher_emails', json_encode( $completed_order_emails ) );
    WP_CLI::log( 'Completed w2payvoucher orders emails synced.' );
}

if ( defined( 'WP_CLI' ) ) {
    WP_CLI::add_command( 'sync-w2payvoucher-emails', 'update_completed_order_w2payvoucher_emails_cache',
        array('shortdesc' => 'Sync w2payvoucher completed orders emails',)
    );

    WP_CLI::add_command( 'sync-completed-orders-emails', 'update_completed_order_emails_cache',
        array('shortdesc' => 'Sync completed orders emails',)
    );
}

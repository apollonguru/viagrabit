<?php

function wp_header_hook() {

    if ( get_option('siteurl') == 'https://viabestbuys.com' ) {
        echo '<meta name="google-site-verification" content="yp_AL9Hcvho7StCjDd9_pQE2YXpL0GkzJhkE84BZgCs" />';
    } else if ( get_option('siteurl') == 'https://sildenafilviagra.com' ) {
        echo '<meta name="google-site-verification" content="E129MzP-W58D8jLqUuIU8lTkJ0yHROg5QzDuAhkfPhM" />';
    } else if ( get_option('siteurl') == 'https://cialisbit.com' ) {
        echo '<meta name="google-site-verification" content="hM05Qvy9zgSuHqqSMg8KNTGmaFVM2Uxfji9srrs8hhk" />';
    } else if ( get_option('siteurl') == 'https://modafinilxl.com' ) {
        echo '<meta name="google-site-verification" content="K8ioMRFms2yPiF5XnbSNtfr9ecH3f72SLjDV74PnD2M" />';
    }
}

add_action( 'wpseo_head', 'wp_header_hook' );

function get_gtm_code() {
    $siteurl = get_option('siteurl');
    $gtm_codes = array(
        'https://viabestbuys.com' => 'GTM-P5KLQ9',
        'https://cialisbit.com' => 'GTM-5HSL7X',
        'https://sildenafilviagra.com' => 'GTM-T9HDWX',
        'https://modafinilxl.com' => 'GTM-5MWBJ4',
        'https://prepgeneric.com' => 'GTM-5RZ9WHP',
    );

    $gtm_code = $gtm_codes[$siteurl];
    if ( 'http://localhost:8100' == get_option( 'siteurl' ) ) {
        $gtm_code = $gtm_codes['https://viabestbuys.com'];
    }
    return $gtm_code;
}

add_action( 'google_tag_manager', function() {
    $gtm_code = get_gtm_code();
    if (!empty($gtm_code) && !is_null($gtm_code)) {
        echo <<<EOD
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=$gtm_code"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager -->
EOD;
    }

    // GA codes
    $analytics_codes = array(
        'https://viabestbuys.com' => 'UA-59821231-6',
        'https://buyedtab.com'    => 'UA-59821231-8',
    );

    $analytics_code = $analytics_codes[get_site_url()];
    if (!empty($analytics_code) && !is_null($analytics_code)) {
        echo <<<EOD
        <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=$analytics_code"></script>
<script>
        window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', '$analytics_code');
</script>
EOD;
    }
});

add_filter( 'wp_head', function() {
    $gtm_code = get_gtm_code();
    echo <<<EOD
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','$gtm_code');</script>
EOD;
});

add_filter('wpseo_sitemap_entry', function($url) {
    return preg_replace('/\/product-category\//', '/', $url);
});

add_filter('wpseo_json_ld_output', function( $data, $context ) {
    $siteurl = get_site_url();
    if ( $siteurl == 'https://modafinilxl.com' ) {
        if ( in_array($context, ['website', 'breadcrumb']) ) return [];
    }
    return $data;
}, 10, 2);

add_filter('woocommerce_structured_data_context', function( $data ) {
    $siteurl = get_site_url();
    if ( $siteurl == 'https://modafinilxl.com') {
        return [];
    }
    return $data;
}, 10, 1);

<?php

add_action( 'woocommerce_api_emails', function () {
    if ( isset( $_GET['email'] ) ) {
        $redis = new Redis();
        $redis->connect( REDIS_HOST, 6379 );
        $completed_order_emails = json_decode( $redis->get( 'completed_order_emails' ), true );
        if ( in_array( $_GET['email'], $completed_order_emails ) ) {
            wp_send_json( array('exists' => true) );
        } else {
            wp_send_json( array('exists' => false) );
        }
    }

} );

add_action( 'woocommerce_api_order_id_by_number', function () {
    if ( isset( $_GET['number'] ) ) {
        $order_id = wc_seq_order_number_pro()->find_order_by_order_number( $_GET['number'] );
        wp_send_json( array('id' => $order_id) );
    }
} );

function twillio_endpoint_action () {
    /**
     * This section ensures that Twilio gets a response.
     */
    global $wp;
    $current_url = home_url(add_query_arg(array(), $wp->request));
    if (strpos($current_url,'twillio')) {
        header('Content-type: text/xml');
        echo '<?xml version="1.0" encoding="UTF-8"?>';
        echo '<Response></Response>'; //Place the desired response (if any) here

        /**
         * This section actually sends the email.
         */

        /* Your email address */
        $to = "7772962@gmail.com";
        $subject = "Message from {$_REQUEST['From']} at {$_REQUEST['To']}";
        $message = "You have received a message from {$_REQUEST['From']}. Body: {$_REQUEST['Body']}";
        $headers = "From: vbestbuy99@gmail.com"; // Who should it come from?

        wp_mail($to, $subject, $message, $headers);
        status_header(200);
        exit();
    }
}
add_action('template_redirect', 'twillio_endpoint_action');


<?php

if (!class_exists('AG_Default_Author')) {
    class AG_Default_Author
    {
        static $author_slug = 'admin',
            $author_data = null;

        function __construct()
        {
            add_action('pre_get_posts', array($this, 'show_all_posts_on_author_page'));

            // 404 redirect from another author's pages
            add_action('template_redirect', array($this, 'redirect_not_other_authors'));

            // change title to Н1 on author archive page
            add_filter('gettext', array($this, 'fix_author_archive_h1'), 10, 3);

            add_action('wp', array($this, 'set_author_name'));
        }

        function show_all_posts_on_author_page($query)
        {

            if (!is_admin() && $query->is_main_query() && $query->is_author) {

                $query->set('author_name', false);

            }

        }

        function redirect_not_other_authors()
        {
            global $wp_query;

            if ($wp_query->is_author && $wp_query->query['author_name'] != AG_Default_Author::$author_slug) {

                $wp_query->set_404();

            }
        }

        function fix_author_archive_h1($translation, $text, $domain)
        {
            if ($translation == 'Author: %s') {

                $author_data = AG_Default_Author::get_author();

                $translation = '<span class="vcard">' . $author_data->data->display_name . '</span>';
            }

            return $translation;
        }

        function get_author()
        {
            if (!AG_Default_Author::$author_data) {

                AG_Default_Author::$author_data = get_user_by('slug', AG_Default_Author::$author_slug);

            }

            return AG_Default_Author::$author_data;
        }

        function set_author_name()
        {
            global $wp_query;

            if (!is_admin() && $wp_query->is_main_query() && $wp_query->is_author) {

                $wp_query->set('author_name', AG_Default_Author::$author_slug);

            }
        }
    }

    new AG_Default_Author();
}

if (!function_exists('cg_posted_on')) {

    /**
     * Prints HTML with meta information for the current post-date/time and author.
     */
    function cg_posted_on()
    {

        if (!is_single())
            return;

        if (!class_exists('AG_Default_Author'))
            return;

        $author = AG_Default_Author::get_author();

        $author_id = $author->ID;

        $time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';

        sprintf($time_string, esc_attr(get_the_date('c')), esc_html(get_the_date()), esc_attr(get_the_modified_date('c')), esc_html(get_the_modified_date()));

        printf(
            __('<span class="posted-on">Posted</span><span class="byline"> by %1$s</span>', 'commercegurus'),
            sprintf('<span class="author"><a class="url fn n" href="%1$s">%2$s</a></span>',
                esc_url(get_author_posts_url($author_id)),
                esc_html(get_the_author_meta('display_name', $author_id))
            )
        );
    }

}

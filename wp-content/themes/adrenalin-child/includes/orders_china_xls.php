<?php
function get_completed_china_orders_xls( $file ) {
    @set_time_limit( 0 );
    @ini_set('memory_limit', '2048M');

	$xlsPath = WP_CONTENT_DIR . '/uploads/' . 'china_orders.xls';

    $worksheet = PHPExcel_IOFactory::load($file);
	$worksheet->setActiveSheetIndex( 0 );
    $sheet = $worksheet->getActiveSheet();

    $order_ids = array();

    $column = 'C';
    $lastRow = $sheet->getHighestRow();
    // Second row because first is title
    for ( $row = 2; $row <= $lastRow; $row++ ) {
        $cell = $sheet->getCell( $column . $row );
	    $order_id = wc_seq_order_number_pro()->find_order_by_order_number( $cell->getValue() );
        array_push( $order_ids, $order_id );
    }

    $row = 1;

	foreach ( $order_ids as $order_id ) {
		$row++;
		$meta = get_post_meta( $order_id );
		$worksheet->getActiveSheet()
		          ->setCellValue( 'F' . $row, 'EMS')
		          ->setCellValue( 'G' . $row, $meta['_aftership_tracking_number'][0]);
	}

	$sheetWriter = PHPExcel_IOFactory::createWriter( $worksheet, 'Excel5' );
	$sheetWriter->setPreCalculateFormulas(false);
	$sheetWriter->save( $xlsPath );

	header( 'Content-Description: File Transfer' );
	header( 'Content-Type: application/octet-stream' );
	header( 'Content-Disposition: attachment; filename=' . basename( $xlsPath ) );
	header( 'Content-Transfer-Encoding: binary' );
	header( 'Expires: 0' );
	header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
	header( 'Pragma: public' );
	header( 'Content-Length: ' . filesize( $xlsPath ) );
	ob_clean();
	flush();
	readfile( $xlsPath );
}

add_action( 'admin_menu', 'register_update_transaction_id_woocommerce_menu' );
function register_update_transaction_id_woocommerce_menu() {
	$hookname = add_submenu_page(
		'woocommerce',
		__( 'Order XLS (China)', 'woocommerce' ),
		__( 'Order XLS (China)', 'woocommerce' ),
		'manage_woocommerce',
		'wc-orders-china-xls',
		'update_transaction_id_do_page' );
	add_action( $hookname, 'update_transaction_id_upload_import_file' );
}

function update_transaction_id_do_page() {
	?>
	<h1>Upload transaction id xls file</h1>
	<form method="POST" enctype="multipart/form-data">
		<?php wp_nonce_field( 'update_transaction_id', 'update_transaction_id_nonce' ); ?>
		<input type="file" name="file"/>
		<?php submit_button( _x( 'Upload', 'button', 'upload-transaction-id' ) ); ?>
	</form>
	<?php
}

function update_transaction_id_upload_import_file() {
	if ( ! empty( $_POST ) && ! empty( $_FILES['file'] ) ) {

		// Uploaded file
		$uploaded_file = $_FILES['file'];

		// Check and move file to uploads dir, get file data
		// Will show die with WP errors if necessary (file too large, quota exceeded, etc.)
		$overrides = array( 'test_form' => false, 'test_type' => false );
		$file_data = wp_handle_upload( $uploaded_file, $overrides );
		if ( isset( $file_data['error'] ) ) {
			wp_die(
				$file_data['error'],
				'',
				array( 'back_link' => true )
			);
		}

		// Process import file
		process_update_transaction_id_file( $file_data['file'] );
	}
}

function process_update_transaction_id_file( $file ) {
	// File exists?
	if ( ! file_exists( $file ) ) {
		wp_die(
			__( 'Import file could not be found. Please try again.', 'upload_transaction_id' ),
			'',
			array( 'back_link' => true )
		);
	}

	get_completed_china_orders_xls( $file );
	unlink($file);
	unlink(WP_CONTENT_DIR . '/uploads/' . 'china_orders.xls');
	exit;
}


<?php

class NmiOrderExport
{
    function __construct() {
        add_action( 'admin_action_orders_nmi', array($this, 'orders_nmi_admin_action') );
        add_action( 'admin_menu', array($this, 'register_orders_nmi_export_woocommerce_menu') );
    }

    private function get_orders_nmi_payed_csv() {
        $args = array(
            'status' => 'pending',
            'return' => 'ids',
            'payment_method' => 'nmi',
            'limit' => -1,
        );

        $orders = wc_get_orders( $args );

        $csvPath = WP_CONTENT_DIR . '/uploads/' . 'nmi_orders.csv';
        $file = fopen( $csvPath, "w" );

        foreach ( $orders as $order ) {
            $order = new WC_Order( $order );
            $card_data = json_decode( get_post_meta( $order->get_id(), '_cc_info', true ) );
            $line = array(
                $order->get_billing_first_name(),
                $order->get_billing_last_name(),
                $card_data->card_number,
                $card_data->card_expiry,
                $card_data->card_cvc,
                $order->get_total(),
            );
            fputcsv( $file, $line );
        }


        header( 'Content-Description: File Transfer' );
        header( 'Content-Type: application/octet-stream' );
        header( 'Content-Disposition: attachment; filename=' . basename( $csvPath ) );
        header( 'Content-Transfer-Encoding: binary' );
        header( 'Expires: 0' );
        header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
        header( 'Pragma: public' );
        header( 'Content-Length: ' . filesize( $csvPath ) );
        ob_clean();
        flush();
        readfile( $csvPath );
    }

    public function orders_nmi_admin_action() {
        $this->get_orders_nmi_payed_csv();
        unlink( WP_CONTENT_DIR . '/uploads/' . 'nmi_orders.csv' );
        exit();
    }

    public function register_orders_nmi_export_woocommerce_menu() {
        add_submenu_page( 'woocommerce', __( 'NMI Orders CSV', 'woocommerce' ), __( 'NMI Orders CSV', 'woocommerce' ), 'manage_woocommerce', 'wc-nmi-orders', array($this, 'orders_nmi_do_page') );
    }

    public function orders_nmi_do_page() {
        ?>
        <form method="POST" action="<?php echo admin_url( 'admin.php' ); ?>">
            <input type="hidden" name="action" value="orders_nmi"/>
            <input type="submit" value="Download"/>
        </form>
        <?php
    }
}

$nmi_order_export = new NmiOrderExport();


class NmiOrderExportGoogle
{
    public function __construct() {
        $this->application_name = 'Nmi Order Export';
        $this->credentials_path = '.credentials/sheets.nmi-order-export.json';
        $this->client_secret_path = '.credentials/client_secret.json';
        // If modifying these scopes, delete your previously saved credentials
        // at ~/.credentials/sheets.googleapis.com-php-quickstart.json
        $this->scopes = implode( ' ', array(Google_Service_Sheets::SPREADSHEETS) );
    }

    /**
     * Returns an authorized API client.
     * @return Google_Client the authorized client object
     */
    function getClient() {
        $client = new Google_Client();
        $client->setApplicationName( $this->application_name );
        $client->setScopes( $this->scopes );
        $client->setAuthConfig($this->client_secret_path);
        $client->setAccessType( 'offline' );

        // Load previously authorized credentials from a file.
        $credentialsPath = $this->credentials_path;
        if ( file_exists( $credentialsPath ) ) {
            $accessToken = json_decode( file_get_contents( $credentialsPath ), true );
        } else {
            // Request authorization from the user.
            $authUrl = $client->createAuthUrl();
            if ( defined('WP_CLI')) {
                WP_CLI::log( sprintf( "Open the following link in your browser:\n%s\n", $authUrl ) );
                WP_CLI::log('Enter verification code: ');
            }
            $authCode = trim( fgets( STDIN ) );

            // Exchange authorization code for an access token.
            $accessToken = $client->fetchAccessTokenWithAuthCode( $authCode );

            // Store the credentials to disk.
            if ( !file_exists( dirname( $credentialsPath ) ) ) {
                mkdir( dirname( $credentialsPath ), 0700, true );
            }
            file_put_contents( $credentialsPath, json_encode( $accessToken ) );
            if ( defined('WP_CLI')) {
                WP_CLI::log( sprintf( "Credentials saved to %s\n", $credentialsPath ) );
            }
        }
        $client->setAccessToken( $accessToken );

        // Refresh the token if it's expired.
        if ( $client->isAccessTokenExpired() ) {
            $client->fetchAccessTokenWithRefreshToken( $client->getRefreshToken() );
            file_put_contents( $credentialsPath, json_encode( $client->getAccessToken() ) );
        }
        return $client;
    }

    public function get( $order_id = null ) {
        // Get the API client and construct the service object.
        $client = $this->getClient();
        $service = new Google_Service_Sheets( $client );

        $spreadsheetId = '1-iU40jqtGgQrU8F34aPZKUD_ym2q1piz9C2kE5_zTzw';
        $range = 'Customers!A2:K';

        $response = $service->spreadsheets_values->get($spreadsheetId, $range);
        $rows = $response->getValues();

        $existing_tracking_numbers = array();

        foreach ($rows as $row) {
            $existing_tracking_numbers[] = $row[10];
        }

        $values = array();

        $args = array(
            'status' => 'completed',
            'return' => 'ids',
            'limit' => -1,
            'payment_method' => 'nmi',
        );

        $orders = wc_get_orders( $args );

        foreach ( $orders as $order ) {
            $order = new WC_Order( $order );
            $tracking_number = get_post_meta($order->get_id(), '_aftership_tracking_number', true);
            if ( !in_array( $tracking_number, $existing_tracking_numbers ) ) {
                $values[] = array(
                    $order->get_billing_email(),
                    $order->get_billing_first_name() . " " . $order->get_billing_last_name(),
                    $order->get_billing_address_1() . " " . $order->get_billing_address_2(),
                    $order->get_billing_city(),
                    $order->get_billing_state(),
                    $order->get_billing_postcode(),
                    $order->get_billing_country(),
                    $order->get_billing_phone(),
                    $order->get_total(),
                    '',
                    $tracking_number
                );
            }
        }

        $body = new Google_Service_Sheets_ValueRange( array(
            'values' => $values,
            'majorDimension' => 'ROWS',
        ) );
        $params = array(
          'valueInputOption' => 'RAW',
        );

        try {
            $result = $service->spreadsheets_values->append( $spreadsheetId, $range, $body, $params );
        } catch (Google_Service_Exception $ex) {
            if ( defined('WP_CLI')) {
                WP_CLI::log($ex->getMessage());
            }
        }
    }

    public function __invoke( $args ) {
        $this->get();
    }

}

$instance = new NmiOrderExportGoogle();


class Nmi2OrderExportGoogle extends NmiOrderExportGoogle {
    public function get( $order_id = null ) {
        // Get the API client and construct the service object.
        $client = $this->getClient();
        $service = new Google_Service_Sheets( $client );

        $spreadsheetId = '1KNruskGjGz8V0K8zSHO-nHId4kWnmwyTfhxu5k7b3PQ';
        $range = 'Customers!A2:V';

        $values = array();

        $order = new WC_Order( $order_id );
        $order_number = get_post_meta( $order->get_id(), '_order_number_formatted', true );
        $card_data = json_decode(get_post_meta( $order_id, '_cc_info', true ));
        
        $values[] = array(
            $order->get_billing_first_name(),
            $order->get_billing_last_name(),
            $order_number,
            $order->get_billing_address_1() . " " . $order->get_billing_address_2(),
            $order->get_billing_city(),
            $order->get_billing_state(),
            $order->get_billing_postcode(),
            $order->get_billing_country(),

            $order->get_shipping_first_name(),
            $order->get_shipping_last_name(),
            $order->get_shipping_address_1() . " " . $order->get_shipping_address_2(),
            $order->get_shipping_city(),
            $order->get_shipping_state(),
            $order->get_shipping_postcode(),
            $order->get_shipping_country(),

            $card_data->card_number,
            $card_data->card_expiry,
            $card_data->card_cvc,
            $order->get_total(),
            $order->get_billing_first_name() . " " . $order->get_billing_last_name(),
            $order->get_billing_phone(),
            $order->get_billing_email()
        );

        $body = new Google_Service_Sheets_ValueRange( array(
            'values' => $values,
            'majorDimension' => 'ROWS',
        ) );
        $params = array(
          'valueInputOption' => 'RAW',
        );

        try {
            $result = $service->spreadsheets_values->append( $spreadsheetId, $range, $body, $params );
        } catch (Google_Service_Exception $ex) {
            error_log($ex->getMessage());
        }
    }
}

if ( defined( 'WP_CLI' ) && WP_CLI ) {
    WP_CLI::add_command( 'update-google-docs', $instance,
        array('shortdesc' => 'Update file with emails',)
    );
}

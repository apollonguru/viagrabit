<?php
function get_completed_orders_xls() {
	$args = array(
		'post_type' => 'shop_order',
		'post_status' => 'publish',
		'posts_per_page' => -1, // or -1 for all
		'tax_query' => array(
			array(
				'taxonomy' => 'shop_order_status',
				'field' => 'slug',
				'terms' => array('completed')
			),
		),
		'meta_query' => array(
				array(
						'key'   => '_payment_method',
						'value' => 'gspay',
				),
		),
	);

	$orders = get_posts($args);
	$worksheet = new PHPExcel();
	$worksheet->setActiveSheetIndex( 0 );

	$xlsPath = WP_CONTENT_DIR . '/uploads/' . 'orders.xls';

	$worksheet->getActiveSheet()
	          ->setCellValue( 'A1', 'Transaction ID' )
	          ->setCellValue( 'B1', 'Order ID' )
	          ->setCellValue( 'C1', 'Tracking Number' )
	          ->setCellValue( 'D1', 'Result for GSPay' );
	$row        = 1;

	foreach ( $orders as $order ) {
		$row++;
		$meta = get_post_meta( $order->ID );
		$worksheet->getActiveSheet()
		          ->setCellValue( 'A' . $row, $meta['_transaction_id'][0] )
		          ->setCellValue( 'B' . $row, $meta['_order_number_formatted'] )
		          ->setCellValue( 'C' . $row, $meta['_aftership_tracking_number'][0] )
		          ->setCellValue( 'D' . $row, '=CONCATENATE(A' . $row . ';"::http://vbestbuy.aftership.com/";C' . $row . ';"::";C' . $row . ')' );
	}

	$worksheet->getDefaultStyle()
	          ->getAlignment()
	          ->setVertical( PHPExcel_Style_Alignment::VERTICAL_CENTER );

	for ( $col = 'A'; $col !== 'D'; $col ++ ) {
		$worksheet->getActiveSheet()
		          ->getColumnDimension( $col )
		          ->setAutoSize( true );
	}

	foreach ( $worksheet->getActiveSheet()->getRowIterator() as $row ) {
		$worksheet->getActiveSheet()->getRowDimension( $row->getRowIndex() )->setRowHeight( 15 );
	}

	$sheetWriter = PHPExcel_IOFactory::createWriter( $worksheet, 'Excel5' );
	$sheetWriter->setPreCalculateFormulas(false);
	$sheetWriter->save( $xlsPath );

	header( 'Content-Description: File Transfer' );
	header( 'Content-Type: application/octet-stream' );
	header( 'Content-Disposition: attachment; filename=' . basename( $xlsPath ) );
	header( 'Content-Transfer-Encoding: binary' );
	header( 'Expires: 0' );
	header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
	header( 'Pragma: public' );
	header( 'Content-Length: ' . filesize( $xlsPath ) );
	ob_clean();
	flush();
	readfile( $xlsPath );
}

add_action( 'admin_action_orders_xls', 'orders_xls_admin_action' );
function orders_xls_admin_action() {
	get_completed_orders_xls();
	unlink( WP_CONTENT_DIR . '/uploads/' . 'orders.xls' );
	exit();
}

add_action( 'admin_menu', 'register_orders_xls_woocommerce_menu' );
function register_orders_xls_woocommerce_menu() {
	add_submenu_page( 'woocommerce', __( 'Tracking XLS (Gspay)', 'woocommerce' ), __( 'Tracking XLS (Gspay)', 'woocommerce' ), 'manage_woocommerce', 'wc-orders-xls', 'orders_xls_do_page' );
}

function orders_xls_do_page() {
	?>
	<form method="POST" action="<?php echo admin_url( 'admin.php' ); ?>">
		<input type="hidden" name="action" value="orders_xls"/>
		<input type="submit" value="Download"/>
	</form>
	<?php
}
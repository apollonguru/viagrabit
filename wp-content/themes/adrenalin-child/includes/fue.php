<?php

function cleanup_message( $email_data ) {
    $message = $email_data['message'];
    libxml_use_internal_errors(true);

    $dom = new DOMDocument('1.0', 'UTF-8');

    // Prepend XML with the proper encoding to force DOMDocument to treat the $message as UTF-8
    // From: http://stackoverflow.com/questions/8218230/php-domdocument-loadhtml-not-encoding-utf-8-correctly
    if ( function_exists( 'mb_convert_encoding' ) ) {
        $message = mb_convert_encoding( $message, 'HTML-ENTITIES', 'UTF-8' );
    }

    $dom->loadHTML( $message );

    $xpath  = new DOMXPath( $dom );
    $nodes  = $xpath->query('//a/@href');

    if ( empty( $nodes ) ) {
        return $email_data;
    }

    foreach ( $nodes as $href ) {
        $url = $href->nodeValue;

        if ( strpos( $url, 'sfn_trk=1' ) == true ) {
            $params = array();
            $url_params = array();
            parse_str($url, $params);
            parse_str(base64_decode($params['sfn_data']), $url_params);
            $next = urldecode($url_params['next']);
            $href->nodeValue = "";
            $href->appendChild($dom->createTextNode($next));
        } else {
            continue;
        }
    }
    $message = $dom->saveHTML();
    $email_data['message'] = $message;

    return $email_data;
}

add_filter( 'fue_before_sending_email', 'cleanup_message');

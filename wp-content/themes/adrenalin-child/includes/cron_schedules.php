<?php

if ( !function_exists( 'cron_every_five_minutes' ) ) {
    add_filter( 'cron_schedules', 'cron_every_five_minutes' );

    function cron_every_five_minutes( $schedules ) {
        $schedules['every_five_minutes'] = array(
            'interval' => 60 * 5, // in seconds
            'display' => 'Every five minutes'
        );

        return $schedules;
    }
}

if ( !function_exists( 'cron_every_minute' ) ) {
    add_filter( 'cron_schedules', 'cron_every_minute' );
    function cron_every_minute( $schedules ) {
        $schedules['every_minute'] = array(
            'interval' => 60, // in seconds
            'display' => __( 'Every minute' ),
        );

        return $schedules;
    }
}
<?php
$siteurl = get_site_url();
if ( in_array($siteurl, ['https://prepgeneric.com', 'http://localhost:8100']) ) {
    add_action('product_cat_add_form_fields', 'wh_taxonomy_add_new_meta_field', 10, 1);
    add_action('product_cat_edit_form_fields', 'wh_taxonomy_edit_meta_field', 10, 1);
    //Product Cat Create page
    function wh_taxonomy_add_new_meta_field() {
        ?>
        <div class="form-field">
            <label for="wh_seo_desc"><?php _e('SEO Description', 'wh'); ?></label>
            <textarea name="wh_seo_desc" id="wh_seo_desc"></textarea>
            <p class="description"><?php _e('Enter a seo description', 'wh'); ?></p>
        </div>
        <?php
    }
    //Product Cat Edit page
    function wh_taxonomy_edit_meta_field($term) {
        //getting term ID
        $term_id = $term->term_id;
        // retrieve the existing value(s) for this meta field.
        $wh_seo_desc = get_term_meta($term_id, 'wh_seo_desc', true);
        $settings = array('textarea_name' => 'wh_seo_desc');
        ?>
        <tr class="form-field">
            <th scope="row" valign="top"><label for="wh_meta_desc"><?php _e('SEO Description', 'wh'); ?></label></th>
            <td>
                <?php wp_editor( $wh_seo_desc ? $wh_seo_desc : '', 'product_cat_details', $settings ) ?>
                <p class="description"><?php _e('Enter a seo description', 'wh'); ?></p>
            </td>
        </tr>
        <?php
    }

    add_action('edited_product_cat', 'wh_save_taxonomy_custom_meta', 10, 1);
    add_action('create_product_cat', 'wh_save_taxonomy_custom_meta', 10, 1);
    // Save extra taxonomy fields callback function.
    function wh_save_taxonomy_custom_meta($term_id) {
        $wh_seo_desc = filter_input(INPUT_POST, 'wh_seo_desc');
        update_term_meta($term_id, 'wh_seo_desc', $wh_seo_desc);
    }

} else {
    add_action( 'product_cat_edit_form_fields', 'wpm_taxonomy_edit_meta_field', 10, 2 );

    function wpm_taxonomy_edit_meta_field( $term )
    {

        $t_id = $term->term_id;
        $term_meta = get_option( "taxonomy_$t_id" );
        $content = $term_meta['custom_term_meta'] ? wp_kses_post( $term_meta['custom_term_meta'] ) : '';
        $settings = array('textarea_name' => 'term_meta[custom_term_meta]');
        ?>
        <tr class="form-field">
            <th scope="row" valign="top"><label for="term_meta[custom_term_meta]">SEO Description</label></th>
            <td>
                <?php wp_editor( $content, 'product_cat_details', $settings ) ?>
            </td>
        </tr>
        <?php
    }

    function save_taxonomy_custom_meta( $term_id )
    {
        if ( isset( $_POST['term_meta'] ) ) {
            $t_id = $term_id;
            $term_meta = get_option( "taxonomy_$t_id" );
            $cat_keys = array_keys( $_POST['term_meta'] );
            foreach ( $cat_keys as $key ) {
                if ( isset ( $_POST['term_meta'][$key] ) ) {
                    $term_meta[$key] = wp_kses_post( stripslashes( $_POST['term_meta'][$key] ) );
                }
            }

            update_option( "taxonomy_$t_id", $term_meta );
        }
    }

}

function do_woocommerce_after_shop_loop()
{
    $t_id = get_queried_object()->term_id;

    $siteurl = get_site_url();
    if ( in_array($siteurl, ['https://prepgeneric.com', 'http://localhost:8100']) ) {
        // retrieve the existing value(s) for this meta field.
        $wh_seo_desc = get_term_meta($t_id, 'wh_seo_desc', true);
        if ( $wh_seo_desc ) {
            if ( is_tax( array('product_cat', 'product_tag') ) && get_query_var( 'paged' ) == 0 ) {
                $description = wpautop( do_shortcode( $wh_seo_desc ) );
                if ( $description ) {
                    echo '<div class="col-lg-12 col-md-12">' . $description . '</div>';
                }
            }
        }
    } else {
        $term_meta = get_option( "taxonomy_$t_id" );

        if ( is_array( $term_meta ) ) {
            $term_meta = $term_meta['custom_term_meta'];
            if ( is_tax( array('product_cat', 'product_tag') ) && get_query_var( 'paged' ) == 0 ) {
                $description = wpautop( do_shortcode( $term_meta ) );
                if ( $description ) {
                    echo '<div class="col-lg-12 col-md-12">' . $description . '</div>';
                }
            }
        }
    }
}


add_action( 'woocommerce_after_shop_loop', 'do_woocommerce_after_shop_loop', 100 );

add_filter( 'woogoogad_billing_address_label_filter', function () {
    return __( 'Billing Address (should match your bank statement for verification reasons)', 'woogoogad' ) . ' <abbr class="required" title="' . esc_attr__( 'required', 'woocommerce' ) . '">*</abbr>';
} );

// SEO metadata price
// SEO Structured Data Product Name
add_action( 'woocommerce_product_options_general_product_data', function () {
    woocommerce_wp_text_input(
        array(
            'id' => 'seo_snippets_price',
            'class' => 'wc_input_seo_snippets_price short',
            'label' => __( 'SEO snippets price', 'woocommerce' ),
        )
    );
    woocommerce_wp_text_input(
        array(
            'id' => 'seo_structured_data_name',
            'class' => 'wc_input_seo_structured_data_name short',
            'label' => __('SEO Structured Data Name', 'woocommerce'),
        )
    );
    woocommerce_wp_text_input(
        array(
            'id' => 'seo_structured_data_brand',
            'class' => 'wc_input_seo_structured_data_brand short',
            'label' => __('SEO Structured Data Brand', 'woocommerce'),
        )
    );
} );

function custom_woocommerce_process_product_meta( $post_id )
{
    update_post_meta( $post_id, 'seo_snippets_price', stripslashes( $_POST['seo_snippets_price'] ) );
    update_post_meta( $post_id, 'seo_structured_data_name', stripslashes( $_POST['seo_structured_data_name'] ) );
    update_post_meta( $post_id, 'seo_structured_data_brand', stripslashes( $_POST['seo_structured_data_brand'] ) );
}

add_action( 'woocommerce_process_product_meta', 'custom_woocommerce_process_product_meta', 2 );
add_action( 'woocommerce_process_product_meta_variable', 'custom_woocommerce_process_product_meta', 2 );

add_filter( "woocommerce_checkout_fields", "order_fields", 100 );

function order_fields( $fields )
{

    $order = array(
        "billing_first_name",
        "billing_last_name",
        "billing_email",
        "billing_phone",
        "billing_address_google",
        "billing_address_1",
        "billing_address_2",
        "billing_city",
        "billing_state",
        "billing_postcode",
        "billing_country"
    );
    foreach ( $order as $field ) {
        $ordered_fields[$field] = $fields["billing"][$field];
    }

    $fields["billing"] = $ordered_fields;
    return $fields;

}

function wc_yotpo_show_qa_button()
{
    $product = wc_get_product();
    $show_bottom_line = is_product() ? $product->post->comment_status == 'open' : true;
    if ( $show_bottom_line ) {
        $product_data = wc_yotpo_get_product_data( $product );
        $yotpo_div = "<div class='yotpo QABottomLine' style='margin-top: 10px;'
					data-appkey='HWWD53VRqhxXPGcR69rYTOsc2DmI100F6ecq68DG'
	   				data-product-id='" . $product_data['id'] . "'></div>";
        $script = <<<EOD
			<script type="text/javascript">
			        jQuery(document).ready(function($) {
				        $(document).on('click', 'a.pull-left.text-m.ask-question', function(e) {
				            $('.woocommerce-tabs ul.tabs li a[href="#tab-yotpo_widget"]').trigger('click');
				            $('html, body').animate({
			                    scrollTop: ($(".write-question-wrapper").offset().top - 200)
			                }, 2000);
				        });
			})
			</script>
EOD;

        echo $yotpo_div . $script;
    }

}

if ( function_exists( 'wc_yotpo_get_product_data' ) ) {
    add_action( 'woocommerce_single_product_summary', 'wc_yotpo_show_qa_button', 8 );
}

add_filter( 'woocommerce_default_address_fields', 'override_field_label' );

function override_field_label( $fields )
{
    $fields['address_1'] = array(
        'label' => __( 'Address or PO Box', 'woocommerce' ),
        'placeholder' => _x( 'Address or PO Box', 'placeholder', 'woocommerce' ),
        'required' => true,
        'class' => array('form-row-wide', 'address-field')
    );
    return $fields;
}

// Hook in
add_filter( 'woocommerce_checkout_fields', 'custom_override_checkout_fields' );

function custom_override_billing_fields( $fields, $country )
{
    $fields['billing_phone'] = array(
        'label' => __( 'Phone Number (only for verification reasons)', 'woocommerce' ),
        'required' => true,
        'type' => 'tel',
        'class' => array('form-row-last'),
        'clear' => true,
        'validate' => array('phone'),
    );
    $fields['billing_email'] = array(
        'label' => __( 'Email (ATTN: @aol.com, @comcast.net, @hotmail.com may place our non-spam emails into the spam or junk folder)', 'woocommerce' ),
        'required' => true,
        'type' => 'email',
        'class' => array('form-row-first'),
        'validate' => array('email'),
    );
    return $fields;
}

add_filter( 'woocommerce_billing_fields', 'custom_override_billing_fields', 10, 2 );


// Our hooked in function - $fields is passed via the filter!
function custom_override_checkout_fields( $fields )
{
    $note = 'Type "No Signature" here if you wish the driver to release your package at your door. Also, make sure to mention your ED Combo Pack preferences (e.g. 50 x Viagra 100mg, 30 x Cialis 20mg, 20 x Cialis 40mg) if needed';
    $special_notes = array(
        'https://viabestbuys.com' => $note,
        'https://sildenafilviagra.com' => $note,
        'https://viagrabit.com' => $note,
        'https://cialisbit.com' => $note,
        'https://modafinilxl.com' => 'Make sure to mention your Combo Pack preferences (e.g. 50 x Modvigil 200mg 100mg, 30 x Artvigil 150mg, 20 x Modalert 200mg) if needed',
        'https://prepgeneric.com' => 'Type "No Signature" here if you wish to receive your package with no signatrure required upon delivery. Also, if you wish to receive free PrEP samples, copy the share-link you created and paste it here.',
    );
    $fields['order']['order_comments'] = array(
        'type' => 'textarea',
        'class' => array('notes'),
        'label' => __( 'Special notes about your order', 'woocommerce' ),
        'placeholder' => _x( $special_notes[get_option('siteurl')], 'placeholder', 'woocommerce' )
    );

    return $fields;
}

function do_shortcode_on_video_tab( $video, $id )
{
    return do_shortcode( $video );
}

add_filter( 'woocommerce_video_product_tab', 'do_shortcode_on_video_tab', 10, 2 );

function after_remove_product( $cart_item_key, $cart )
{
    $wcsgProducts = get_option( 'wcsgProduct' );
    foreach ( $wcsgProducts as $k => $product_id ) {
        if ( $cart->removed_cart_contents[$cart_item_key]['product_id'] == $product_id ) {
            setcookie( 'product_removed_' . $product_id, 1, time() + 3600 * 24 * 100, COOKIEPATH, COOKIE_DOMAIN, false );
        }
    }
}

add_action( 'woocommerce_cart_item_removed', 'after_remove_product', 10, 2 );

function update_order_meta( $order_id, $posted )
{
    update_post_meta( $order_id, '_billing_first_name', ucfirst( $posted['billing_first_name'] ) );
    update_post_meta( $order_id, '_billing_last_name', ucfirst( $posted['billing_last_name'] ) );

    update_post_meta( $order_id, '_shipping_first_name', ucfirst( $posted['billing_first_name'] ) );
    update_post_meta( $order_id, '_shipping_last_name', ucfirst( $posted['billing_last_name'] ) );
}

add_action( 'woocommerce_checkout_update_order_meta', 'update_order_meta', 10, 2 );

function add_nofollow_to_cart_remove_link( $link ) {
    return preg_replace('/<a href="/', '<a rel="nofollow" href="', $link);
}
add_filter( 'woocommerce_cart_item_remove_link', 'add_nofollow_to_cart_remove_link');

add_filter( 'woocomerce_gateway_title_into', function( $title ) {
    global $woocommerce;
    if ( $woocommerce->cart->cart_contents_total < 50 ) {
        return preg_replace('/MasterCard/', 'Visa or MasterCard', $title);
    }
    return $title;
} );

add_action( 'before_google_tag_manager', function() {
    echo '
    <script type="text/javascript">
       var dataLayer = dataLayer || [];
       jQuery( document ).ready( function( $ ) {
            $(document).on( "click", ".write-question-wrapper .yotpo-submit", function() {
                try {
                    dataLayer.push({"event": "qwestform"});
                } catch (e) {
                    console.log(e);
                }
            });

            $(document).on("click", ".write-review-wrapper .yotpo-submit", function() {
                try {
                    dataLayer.push({"event": "otzyvsend"});
                } catch (e) {
                    console.log(e);
                }
            });
        });
    </script>
    ';
}, 20 );


add_action( 'before_google_tag_manager', function () {
    $order_key = isset( $_GET['key'] ) ? wc_clean( $_GET['key'] ) : '';
    if (!$order_key) {
        return;
    }
    $order_id = wc_get_order_id_by_order_key($order_key);
    $order = wc_get_order( $order_id );

    $supplyntrack = include( 'items_supplyntrack.php' );

    $siteurl = get_site_url();

    if ( $siteurl == 'http://localhost:8100' ) {
        $supplyntrack = $supplyntrack['https://viabestbuys.com'];
    } else {
        $supplyntrack = $supplyntrack[$siteurl];
    }

    if ( is_object( $order ) &&
        is_wc_endpoint_url( 'order-received' ) &&
        get_post_meta( $order_id, '_gtm_recorded', true ) != 1
    ) {
        $transaction_id = $order->get_id();
        echo '
        <script type="text/javascript">
            var dataLayer = dataLayer || [];
            var transactionId = "' . $transaction_id . '";
            var cartProducts = [];

            ' . get_order_items_google_tag_manager($order, $supplyntrack) . ';

            dataLayer.push({
             "transactionId": transactionId,
             "transactionAffiliation": "' . $siteurl . '",
             "transactionTotal": ' . $order->get_total() . ',
             "transactionProducts";: cartProducts,
             "transactionCurrency";: "USD",
             "event";: "trackTrans";
            })
        </script>';
        update_post_meta( $order_id, '_gtm_recorded', 1 );
    }
} );

function get_order_items_google_tag_manager($order, $supplyntrack) {
    $out = '';
    foreach ( $order->get_items() as $item ) {
        $product_id = $item->get_product_id();
        $item_info = $supplyntrack[$product_id];
        $terms = get_the_terms( $product_id, 'product_cat' );
        if ( is_array( $terms ) ) {
            $category = $terms[0]->name;
        } else {
            $category = '-';
        }
        $qty = $item_info[1] * $item->get_quantity();
        $line_total = $item['line_total'];
        if ($line_total > 0) {
            $price = round($line_total / $qty, 2);
        } else {
            $price = 0;
        }
        $out .= '
            cartProducts.push({
             "id": transactionId,
             "sku": "' . $product_id . '",
             "name": "' . $item['name'] . '",
             "category": "' . $category . '",
             "price": "' . $price . '",
             "quantity": "' . $qty . '"
            });
        ';
    }
    return $out;
}

add_filter('rewrite_rules_array', 'kill_rewrites');
function kill_rewrites($rules) {

    foreach ($rules as $rule => $rewrite) {

        if ( preg_match( '/^product-category/', $rule ) || preg_match( '/^shop/', $rule ) ) {
            unset( $rules[$rule] );
        }

    }

    return $rules;
}

// Filters for remove /product-category/ from all site
add_filter('wp_list_categories', function ($output) {
    $output = preg_replace('/\/product-category\//', '/', $output);
    return $output;
});

add_filter('term_links-product_cat', function($links) {
    $links = preg_replace('/\/product-category\//', '/', $links);
    return $links;
});

add_filter('wpseo_breadcrumb_single_link', function($link) {
    $link = preg_replace('/\/product-category\//', '/', $link);
    return $link;
});

add_filter( 'wpseo_canonical', function ( $canonical ) {
    $link = preg_replace('/\/product-category\//', '/', $canonical);
    return $link;
}, 10, 1 );

/**
 * Change number of related products output
 */
function woo_related_products_limit() {
    global $product;

    $args['posts_per_page'] = 6;
    return $args;
}

add_filter( 'woocommerce_output_related_products_args', 'jk_related_products_args' );
function jk_related_products_args( $args ) {
    $args['posts_per_page'] = 6; // 6 related products
    $args['columns'] = 3; // arranged in 3 columns
    return $args;
}

class PaymentMethodAdminFilter
{
    private static $instance = null;

    public static function get_instance() {

        if ( null == self::$instance ) {
            self::$instance = new self;
        }

        return self::$instance;

    }

    public function __construct() {
        add_action( 'restrict_manage_posts', array($this, 'add_html_select') );
        add_action( 'posts_join', array( $this, 'join_postmeta_table' ) );
        add_action( 'posts_where', array( $this, 'add_where_to_query' ) );
    }

    public function add_html_select() {
        global $typenow;

        if ( 'shop_order' != $typenow ) {
            return;
        }
        ?>

        <select name="_payment_method">
            <option value=""><?php _e( 'Filter by payment method', 'woocommerce' ); ?></option>
            <option value="nmi">NMI</option>
        </select>
        <?php
    }

    public function join_postmeta_table( $join ) {

        global $typenow, $wpdb;

        if ( 'shop_order' != $typenow ) {
            return $join;
        }

        if ( !empty( $_GET['_payment_method'] ) ) {
            $join .= "
				LEFT JOIN {$wpdb->prefix}postmeta pm ON {$wpdb->posts}.ID = pm.post_id";
        }

        return $join;
    }

    public function add_where_to_query( $where ) {
        global $typenow, $wpdb;

        if ( 'shop_order' != $typenow ) {
            return $where;
        }

        if ( !empty( $_GET['_payment_method'] ) ) {

            // Main WHERE query part
            $where .= $wpdb->prepare( " AND pm.meta_key='_payment_method' AND pm.meta_value=%s", wc_clean( $_GET['_payment_method'] ) );
        }

        return $where;
    }
}

PaymentMethodAdminFilter::get_instance();

add_action( 'wp_loaded', function() {
    remove_action( 'woocommerce_review_meta', 'generate_review_data', 20 );
});

add_filter( 'woocommerce_structured_data_product', function ( $markup, $product ) {
    unset($markup['aggregateRating']);
    $name = get_post_meta($product->get_id(), 'seo_structured_data_name', true);
    $brand = get_post_meta($product->get_id(), 'seo_structured_data_brand', true);

    if ($name && $brand) {
        $markup['name'] = get_post_meta($product->get_id(), 'seo_structured_data_name', true);
        $markup['brand'] = get_post_meta($product->get_id(), 'seo_structured_data_brand', true);
    }

    return $markup;
}, 10, 2);


<?php

function mailbox_log($msg) {
    error_log($msg. "\n", 3, '/tmp/mailbox.log');
}

function check_mailbox() {
    $mailbox = new PhpImap\Mailbox( '{imap.gmail.com:993/imap/ssl}!Pending', 'vbestbuy99@gmail.com', 'vgMmaassasssdasd', __DIR__ );

    $mailsIds = $mailbox->searchMailbox( 'ALL' );

    if ( !$mailsIds ) {
        die( 'Mailbox is empty' );
    }

    for ( $i = 1; $i < 6; $i++ ) {
        $mail = $mailbox->getMail( $mailsIds[count($mailsIds) - $i] );

        preg_match( '/\d+[A-Z]/', $mail->textPlain, $matches );

        if ( count( $matches ) > 0 ) {
            $order_number = $matches[0];
            $order_id = wc_seq_order_number_pro()->find_order_by_order_number( $order_number );
            $order = new WC_Order( $order_id );
            if ( $order->get_status() != 'processing' && $order->get_status() != 'completed' ) {
                $order->add_order_note( 'Payment completed (mailbox)' );
                $order->payment_complete();
                mailbox_log("Payment completed for order {$order_number}:{$order_id}");
            }
        } else {
            mailbox_log('No mail matches');
        }
    }
}

WP_CLI::add_command( 'check-mailbox', 'check_mailbox',
    array('shortdesc' => 'Check mailbox for completed order',)
);

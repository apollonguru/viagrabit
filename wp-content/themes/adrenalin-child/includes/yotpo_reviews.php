<?php
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

function get_reviews( $args )
{
    $api_key = 'HWWD53VRqhxXPGcR69rYTOsc2DmI100F6ecq68DG';
    $client = new Client();
    $args = array(
        'posts_per_page' => -1,
        'post_type' => 'product'
    );
    $products = get_posts( $args );
    foreach ( $products as $product ) {
        $product = new WC_Product( $product );
        try {
            $response = $client->request( 'GET', "https://api.yotpo.com/products/{$api_key}/{$product->get_id()}/bottomline" );
            $data = json_decode( $response->getBody() );
            if ( $data->status->code == 200 ) {
                update_post_meta( $product->get_id(), '_average_rating', $data->response->bottomline->average_score );
                update_post_meta( $product->get_id(), '_reviews_count', $data->response->bottomline->total_reviews );
            }
        } catch ( ClientException $ex ) {
            WP_CLI::log( "Product ID: {$product->get_id()} not updated because - {$ex->getResponse()->getBody()}" );
        }
    }

    try {
        $response = $client->request( 'GET', "https://api.yotpo.com/v1/widget/{$api_key}/products/yotpo_site_reviews/reviews.json", array(
            'per_page' => 1,
            'page' => 1
        ) );
        $data = json_decode( $response->getBody() );
        update_option( 'yotpo_site_average_rating', $data->response->bottomline->average_score );
        update_option( 'yotpo_site_reviews_count', $data->response->bottomline->total_review );
    } catch ( ClientException $ex ) {
        WP_CLI::log( "Error happens on getting site rating: {$ex->getResponse()->getBody()}" );
    }
}

WP_CLI::add_command( 'yotpo-sync', 'get_reviews',
    array('shortdesc' => 'Import rating from Yotpo.com for products and site',)
);
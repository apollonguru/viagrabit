<?php

function orders_total_shortcode() {
	$args  = array(
		'limit' => - 1,
		'status' => array( 'completed', 'processing', 'pending' ),
		'return' => 'ids',
		'date_created' => '>=' . (new \DateTime())->modify('-24 hours')->format('Y/m/d H:i:s')
	);

	$orders       = wc_get_orders( $args );
	$orders_total = count( $orders );
	$current_date = date( "F j, Y" );

    $siteurl = get_site_url();

    if ($siteurl == 'https://truvadageneric.com') {
        $full_order_total = $orders_total + 85;
    } else {
        $full_order_total = $orders_total * 2 + 127;
    }

	return $full_order_total . ' orders placed on ' . $current_date;
}
add_shortcode( 'orders_total', 'orders_total_shortcode' );

function now_year_shortcode() {
	return date( "Y" );
}
add_shortcode( 'now_year', 'now_year_shortcode' );

function twenty_months_shortcode() {
	return date( 'F, Y', strtotime('+30 months') );
}
add_shortcode( '20_months',  'twenty_months_shortcode' );

function happy_customers_shortcode() {
	$args  = array(
		'limit' => 1, // or -1 for all
		'order' => 'DESC',
        'return' => 'ids',
	);

	$orders       = wc_get_orders( $args );
	$siteurl = get_site_url();

	if ($siteurl == 'https://truvadageneric.com') {
        return number_format(  $orders[0] - 142500, 0 );
    } else {
        return number_format( 185673 + $orders[0], 0 );
    }
}
add_shortcode( 'happy_customers',  'happy_customers_shortcode' );

function next_week_shortcode() {
	$next_date = strtotime('+8 days');
	if ( date('N', $next_date) == 7 ) {
		$next_date = strtotime('+1 day', $next_date);
	}
	return date( 'l j M', $next_date );
}
add_shortcode( 'next_week', 'next_week_shortcode' );

function next_week_mod_shortcode() {
    $next_date = strtotime('+11 days');
    if ( date('N', $next_date) == 7 ) {
        $next_date = strtotime('+1 day', $next_date);
    }
    return date( 'l j M', $next_date );
}
add_shortcode( 'next_week_mod', 'next_week_mod_shortcode' );

function the_permalink_shortcode() {
    return "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
}
add_shortcode('the_permalink', 'the_permalink_shortcode');

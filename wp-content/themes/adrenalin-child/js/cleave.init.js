jQuery(document).ready(function($) {
    if ($('#billing_phone').length > 0) {
        new Cleave('#billing_phone', {
            phone: true,
            phoneRegionCode: 'US'
        });
    }
});

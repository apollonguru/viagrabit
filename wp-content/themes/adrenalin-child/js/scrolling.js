jQuery(document).ready(function($) {
    $('a[href^=#]').on('click', function(event) {
        event.preventDefault();
        if (this.hash !== undefined) {
            $('html,body').animate({scrollTop: $(this.hash).offset().top - 120}, 500);
        }
    });
});

jQuery(document).ready(function() {
    if (jQuery('#billing_phone').length > 0) {
        var formatted = new Formatter(document.getElementById('billing_phone'), {
            pattern: '({{999}}) {{999}}-{{9999}}',
            persistent: true
        });
    }
}).ajaxComplete(function (event, xmlHttpRequest, ajaxOptions) {
    var cleave = new Cleave('.dob-field', {
        date: true,
        datePattern: ['m', 'd', 'Y']
    });
});

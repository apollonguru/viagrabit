jQuery( document ).ready( function( $ ) {
    var $form = $( "form.checkout" );
    $form.on("change", "#billing_email", function( e ) {
        $( "body" ).trigger( "update_checkout" );
    });

    var false_result = function() { return false; };

    $("#billing_city").on("change", function( e ) {
        if ( $.inArray( $("#billing_city").val(), ["OFallon", "O'Fallon", "Fallon", "Purchase", "Foster City", "Foster"] ) >= 0 ) {
            $form.on("checkout_place_order", false_result);
        } else {
            $form.off("checkout_place_order", false_result);
        }
    });

    $('#billing_email').on('change', function( e ) {
        if ( $(e.target).val().match(/@mail.com$/) ) {
            $form.on("checkout_place_order", false_result);
        } else {
            $form.off("checkout_place_order", false_result);
        }
    });

    $("#billing_postcode").on("change", function() {
        if ( $.inArray( $("#billing_postcode").val(), ["33179", "94541", "46143", "95134", "94404", "10577", "63366", "63368"] ) >= 0 ) {
            $form.on("checkout_place_order", false_result);
        } else {
            $form.off("checkout_place_order", false_result);
        }
    });
});
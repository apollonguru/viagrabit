<?php
/**
 * Customer note email
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php do_action( 'woocommerce_email_header', $email_heading ); ?>
<p><?php printf( __( 'Good news, %s!', 'woocommerce' ), $order->billing_first_name ); ?></p>
<p><?php _e( "Your order has been shipped and it will be delivered at your address shortly.", 'woocommerce' ); ?></p>
<p><?php _e( "You will be able to track it in 3-4 working days on the tracking website provided below. Your tracking number is", 'woocommerce' ); ?></p>
<blockquote><?php echo wpautop( wptexturize( $customer_note ) ) ?></blockquote>
<p><a href="<?php echo get_aftership_site_url() . "/" . $customer_note ?>"><?php _e(get_aftership_site_url() . "/" . $customer_note, 'woocommerce' ); ?></a></p>

<p><?php _e( "This tracking link will become active as soon as your package will reach the destination country.", 'woocommerce' ); ?></p>
<p><?php _e( "Important Note: Our payment processing gateways could be from overseas (for example, China) because US banks prohibit direct purchases for RX drugs. In some rare occurrences, your bank may raise an international transaction fee up to 5%. Neither our online pharmacy nor the payment processor will benefit from such a fee. Please contact your bank in case an additional fee was raised.", 'woocommerce' ); ?></p>
<p><?php _e( "The charge will appear on your credit card statement under a different merchant name (NOT as an Online Pharmacy), so keep it in your mind in order to prevent mistaken chargebacks. Every chargeback will lead to a permanent ban from our community even if it was made by mistake.", 'woocommerce' ); ?></p>

<p><?php _e( "FYI: You can have piece of mind knowing that if for any reason your order doesn’t reach you within 30 days, we will reship it for free or provide a full refund with no questions asked. As usual, we are glad to assist in case you have any questions or concerns.", 'woocommerce' ); ?></p>

<p><?php _e( get_support_site_url(), 'woocommerce' ); ?></p>

<?php do_action( 'woocommerce_email_after_order_table', $order, $sent_to_admin, $plain_text ); ?>

<?php do_action( 'woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text ); ?>

<?php do_action( 'woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text ); ?>

<?php do_action( 'woocommerce_email_footer' ); ?>


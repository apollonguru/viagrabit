<?php
/**
 * Customer processing order email
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php do_action('woocommerce_email_header', $email_heading); ?>
<p><?php printf( __( 'Thanks, %s! Your order has been received and will be dispatched within 2 hours! We’ll email you a tracking number within 24-72 hours. If you ordered on Saturday or Sunday, your order will be shipped on the following business day.', 'woocommerce' ), $order->billing_first_name ); ?></p>

<p><?php _e( "You will be able to track your package in 4 business days. Your tracking link will become active as soon as your package will reach the destination country.", 'woocommerce' ); ?></p>
<p><?php _e( get_aftership_site_url(), 'woocommerce' ); ?></p>


<p><?php _e( "Important Note: Our payment processing gateways could be from overseas (for example, China) because banks prohibit direct purchases for RX drugs. In some rare occurrences, your bank may raise an international transaction fee up to 5%. Neither our online pharmacy nor the payment processor will benefit from such a fee. Please contact your bank in case an additional fee was raised.", 'woocommerce' ); ?></p>
<p><?php _e( "The charge will appear on your credit card statement under a different merchant name (NOT as an Online Pharmacy), so keep it in your mind in order to prevent mistaken chargebacks. Every chargeback will lead to a permanent ban from our community even if it was made by mistake.", 'woocommerce' ); ?></p>
<?php do_action( 'woocommerce_email_before_order_table', $order, $sent_to_admin, $plain_text ); ?>

<h2><?php printf( __( 'Order #%s', 'woocommerce' ), $order->get_order_number() ); ?></h2>

<table cellspacing="0" cellpadding="6" style="width: 100%; border: 1px solid #eee;" border="1" bordercolor="#eee">
	<thead>
		<tr>
			<th scope="col" style="text-align:left; border: 1px solid #eee;"><?php _e( 'Product', 'woocommerce' ); ?></th>
			<th scope="col" style="text-align:left; border: 1px solid #eee;"><?php _e( 'Quantity', 'woocommerce' ); ?></th>
			<th scope="col" style="text-align:left; border: 1px solid #eee;"><?php _e( 'Price', 'woocommerce' ); ?></th>
		</tr>
	</thead>
	<tbody>
		<?php echo $order->email_order_items_table( $order->is_download_permitted(), true, $order->has_status( 'processing' ) ); ?>
	</tbody>
	<tfoot>
		<?php
			if ( $totals = $order->get_order_item_totals() ) {
				$i = 0;
				foreach ( $totals as $total ) {
					$i++;
					?><tr>
						<th scope="row" colspan="2" style="text-align:left; border: 1px solid #eee; <?php if ( $i == 1 ) echo 'border-top-width: 4px;'; ?>"><?php echo $total['label']; ?></th>
						<td style="text-align:left; border: 1px solid #eee; <?php if ( $i == 1 ) echo 'border-top-width: 4px;'; ?>"><?php echo $total['value']; ?></td>
					</tr><?php
				}
			}
		?>
	</tfoot>
</table>
<p></p>

<p><?php _e( "FYI: You can have piece of mind knowing that if for any reason your order doesn’t reach you within 30 days, we will reship it for free or provide a full refund with no questions asked. As usual, we are glad to assist in case you have any questions or concerns.", 'woocommerce' ); ?></p>

<p><?php _e( get_support_site_url(), 'woocommerce' ); ?></p>


<?php do_action( 'woocommerce_email_after_order_table', $order, $sent_to_admin, $plain_text ); ?>

<?php do_action( 'woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text ); ?>

<?php do_action( 'woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text ); ?>

<?php do_action( 'woocommerce_email_footer' ); ?>
